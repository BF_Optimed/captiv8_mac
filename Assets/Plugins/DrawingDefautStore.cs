﻿using UnityEngine;
using System.Collections;

public class DrawingDefautStore : MonoBehaviour {

    public Color DefaultColor { get; set; }
    public float DefaultWidth { get; set; }

    void Start()
    {
        DefaultColor = Color.green;
        DefaultWidth = 10.0f;
    }
}
