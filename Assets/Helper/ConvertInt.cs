﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Assets.Helper
{
    public class ConvertInt
    {
        public static int IntFetcher(string value)
        {
            var result = 0;
            value = Regex.Replace(value, "[^0-9]", "");
            UnityEngine.Debug.Log(value);
            if (int.TryParse(value, out result))
            {
                return result;
            }
            throw new InvalidCastException("Invalid input string. Not able to fetch int.");
        }
    }
}
