﻿using UnityEngine;
using System.Collections;

public class LocationButton : MonoBehaviour {
    public UIGrowthScript CloseList;
    public GameObject Location;
    public UILabel label;

    private void Start()
    {
        Location = GameObject.Find("Location");
        CloseList = Location.GetComponent<UIGrowthScript>();
        label = GameObject.Find("NameLabel").GetComponent<UILabel>();
    }

    public void OnPressed()
    {
        label.text = gameObject.name;
        Camera.main.GetComponent<BFSNRegistrationLicense>().UpdateLocation(gameObject.name);
        CloseList._showing = false;
        CloseList.Close();
    }
}
