﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityTest
{
    [TestFixture]
    internal class When_working_out_field_of_view
    {
        [Test]
        [TestCase(10.1f, 2.5f)]
        [TestCase(9.9f, 3.5f)]
        [TestCase(5.1f, 3.5f)]
        [TestCase(4.9f, 5.5f)]
        [TestCase(3.1f, 5.5f)]
        [TestCase(10f, 2.5f)]
        public void Should_calculate_correct_factor(float fieldOfView, float expected)
        {
            Camera.main.fieldOfView = fieldOfView;
            var systemUnderTest = new BFSMPan();
            var actual = systemUnderTest.GetFactor();
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
