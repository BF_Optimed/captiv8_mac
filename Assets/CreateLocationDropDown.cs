﻿using UnityEngine;
using System.Collections;

public class CreateLocationDropDown : MonoBehaviour 
{
    public string[] LocationList{get; set;}
    //the script used to fade out and move the scene to a new one
    //private BlackoutScript blackoutScript;
    //the button created by this script
    public GameObject uiObject;
    //the object the buttons are placed in, usually a table
    public GameObject parentObject;
    //the list of existing bookmarks to make
    private GameObject button;
    public UISprite background;

    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        Debug.Log("Location stuff was activated");
        LocationList = BFSNRegistrationLicense.locationArray;

        for (int i = 0; i < LocationList.Length; i++)
        {
            button = NGUITools.AddChild(parentObject, uiObject);
            UILabel newButtonLabel = button.GetComponentInChildren<UILabel>();
            button.name = LocationList[i];
            newButtonLabel.text = LocationList[i];
        }
    }
}
