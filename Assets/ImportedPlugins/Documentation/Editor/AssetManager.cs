using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

using System.IO;
using System.Text;
using System.Linq;

public class AssetManager : EditorWindow
{

    public string m_OutputFile = "";
	
	// A list of files to delete when the action button 
	// is pressed.
    string [] m_Files = new string [0];
    string m_Log = "This is the output log. The text field below can be filled with a path to save the ouput logs to a text file.";
    Vector2 m_TextScroll = new Vector2( 0.0f, 0.0f);

    [MenuItem("PixelEuphoria/Asset Manager")]
    static void Init()
    { 
		AssetManager.GetWindow(typeof(AssetManager)); 
	}
     
    void OnGUI()
    {
        //////////////////////////////////////////////////
        //      SCAN + REMOVAL
        //////////////////////////////////////////////////
        GUILayout.BeginVertical( "Box");
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label( "File Asset Scanning + Removal");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
		
		// The button to find and log all unreferenced models, and map the
		// action button to delete them.
        if (GUILayout.Button("Scan Models", GUILayout.ExpandWidth(true)))
        {
			// http://unity3d.com/unity/editor/importing
            string [] rstrModExt = {".mtl", ".obj", ".blend", ".fbm", ".fbx", ".3ds", ".mb", ".ma", ".max", ".c4d", ".collada", ".dxf"};
            ScanFiles( "Models", rstrModExt );
        }
		
		// The button to find and log all unreferenced textures, and map the 
		// action button to delete them.
        if( GUILayout.Button( "Scan Textures",  GUILayout.ExpandWidth(true) ) )
		{
			// http://unity3d.com/unity/editor/importing
        	string [] rstrTexExt = {".png", ".jpg", ".jpeg", ".rgb", ".tga", ".targa", ".gif", ".tiff", ".tif", ".bmp", ".iff", ".pict", ".psd" };
            ScanFiles( "Textures", rstrTexExt );
		}
		
		// The button to find and log all unreferenced materials, and map the
		// action button to delete them.
        if( GUILayout.Button( "Scan Materials", GUILayout.ExpandWidth(true) ) )
        {
            string [] rstrMatExt = {".mat"};
            ScanFiles( "Materials", rstrMatExt );
        }

        GUILayout.EndVertical();
        GUILayout.Space( 10 );

        //////////////////////////////////////////////////
        //      SCENE OPTIMIZATION
        //////////////////////////////////////////////////

        GUILayout.BeginVertical( "Box");
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label( "Scene Optimizations");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Consolidate Duplicate Textures", GUILayout.ExpandWidth(true)))
            FindDuplicateTextures( );

        if (GUILayout.Button("Removed Unused Animation Components"))
            RemovedUnusedAnimations();

        if( GUILayout.Button( "Remove Unused GameObject Leafs" ) )
            RemoveUnusedChildren();

        if( GUILayout.Button( "Convert White Diffuse To Mobile" ) )
            ConvertWhiteDiffuseToMobile();    

        GUILayout.EndVertical();
        GUILayout.Space( 10 );

        //////////////////////////////////////////////////
        //      INFO BOX
        //////////////////////////////////////////////////

        m_TextScroll = GUILayout.BeginScrollView( m_TextScroll, GUILayout.ExpandHeight( true), GUILayout.ExpandWidth(true) );
        GUILayout.TextArea( m_Log, GUILayout.ExpandHeight( true) );
        GUILayout.EndScrollView();

        if (m_Files != null && m_Files.Length > 0)
        { 
            Color c = GUI.color;
            GUI.color = new Color( 1.0f, 0.0f, 0.0f );

            GUILayout.Space( 10 );
            if( GUILayout.Button( "Delete Listed Unused Assets", GUILayout.ExpandWidth( true ) ) )
                DeleteAllIdentified();

            GUI.color = c;
        }

        m_OutputFile = GUILayout.TextField( m_OutputFile, GUILayout.ExpandWidth(true));
    }

    void DeleteAllIdentified()
    {
        StringBuilder sbOutput = new StringBuilder();
        sbOutput.Append( "\tDeleting Listed Files\n" );
        sbOutput.Append( "=====================================\n");

        foreach( string str in m_Files )
        {
            if( AssetDatabase.DeleteAsset( str ) )
                sbOutput.Append( string.Format("Deleted {0}\n", str) );
            else
                sbOutput.Append( string.Format("ERROR : Failed to delete asset {0}", str) );
        }
        SetLog( sbOutput.ToString() );
    }

    void ScanFiles( string type, string [] formats)
    {
        StringBuilder sbAllFiles = new StringBuilder();
        StringBuilder sbOutput = new StringBuilder();
        DirectoryInfo diRootPath = new DirectoryInfo( Application.dataPath ).Parent;
        string strRootPath = diRootPath.FullName;
        int nRootPathLen = strRootPath.Length;
        if( strRootPath[nRootPathLen -1] != '\\' && strRootPath[nRootPathLen -1] != '/' )
            ++nRootPathLen;

        sbOutput.Append( "\tScanning for unused " + type + "\n" );
        sbOutput.Append( "=====================================\n");
        sbOutput.Append( string.Format( "Using root directory {0}\n", strRootPath ) );

        // Gather all the unity files in the root folder.
		//
		// Because we're using the dependencies of the unity file, we need to make sure
		// the user saves all relevant Unity files if they want to detect dependencies
		// from the current project state.
        string [] rstrUnityFiles = 
            new DirectoryInfo( Application.dataPath).GetFiles( "*.unity").
                Select( x => x.FullName.Substring( nRootPathLen ) ).
                ToArray();        

        sbOutput.Append( string.Format( "Found {0} different unity files for dependencies\n", rstrUnityFiles.Length) );
        foreach( string str in rstrUnityFiles )
            sbOutput.Append( string.Format( "\t{0}\n", str ) );

        // All the dependencies for the unity files in the root folder
        string [] rstrDependencies  = 
            AssetDatabase.GetDependencies( rstrUnityFiles );

        foreach( string dep in rstrDependencies)
            sbAllFiles.Append( dep + '\n' );

        sbOutput.Append( string.Format( "Found {0} file depenendies for unity files\n", rstrDependencies.Length) );

        // Get a list of all the files of all extensions in our project directory.
		//
		// This makes it easier if we want to filter out other formats in the future.
		// From there, if we find a file is depended on by the Unity project, we remove it. 
		// Then ideally, all that will be left are the orphaned/unreferenced files. And hopefully
		// this should be true unless we consider Resource files, or files opened outside of 
		// inspector assignment.
        string [] rstrAllFiles = 
            diRootPath.GetFiles( "*.* ", SearchOption.AllDirectories )
            // There's a clash on what character is used to separate directories between .Net and unity
                .Select( x => x.FullName.Substring(nRootPathLen).Replace( '\\', '/' ) )
                .Where( x => !x.Contains( "Assets/Standard Assets") )
                .ToArray();
		
		// Keep only relevant files that we may consider as being orphaned.
		// We need the HashSet for quick lookups and to make sure we don't have
		// redundant entries.
        HashSet<string> hsFiltered = new HashSet<string>();
        foreach( string file in rstrAllFiles )
        {
            foreach( string format in formats )
            {
                if( file.EndsWith(format) )
                {
                    hsFiltered.Add( file );
                    break;
                }
            }
        }

        sbOutput.Append( string.Format( "Found {0} unfiltered {1} files\n", hsFiltered.Count, type ) );
		
		// For each dependant file, see if it's on our orphaned list. If it is,
		// then we need to unmark it as an orphaned asset.
        foreach (string file in rstrDependencies )
        {
            foreach( string format in formats )
            {
                if( file.EndsWith(format) )
                {
                    hsFiltered.Remove( file );
                    break;
                }
            }
        }

        sbOutput.Append( string.Format( "Found {0} {1} files not referenced\n", hsFiltered.Count, type ) );

        List<string> lststr = new List<string>();
        foreach (string file in hsFiltered)
        {
            lststr.Add( file );
            sbOutput.Append( string.Format("Identified unused asset {0}\n", file) );
        }

        
        SetLog( sbOutput.ToString() );
		m_Files = lststr.ToArray(); // Has to be called after SetLog()
    }

    class TextureNameCollection
    {
        internal Texture m_Texture;

        internal HashSet<string> m_UniqueFullPaths = 
            new HashSet<string>();

        // There's a flaw here in that is assumes each texture is only
        // used once, max, per material - which is fine, unless they're 
        // referencing duplicates.
        internal Dictionary<Material, List<string>> m_References = 
            new Dictionary<Material, List<string>>();
    }

    void RemovedUnusedAnimations()
    {
        StringBuilder sbOutput = new StringBuilder();

        Animation [] animations = (Animation[])GameObject.FindObjectsOfType( typeof(Animation));
        sbOutput.Append( string.Format( "Found {0} Animation modules\n", animations.Length) );

        int nRm = 0;
        foreach (Animation a in animations)
        {
            if( a.clip != null || a.GetClipCount() != 0 )
                continue;

            sbOutput.Append( string.Format("\tRemoving unused animation at {0}\n", a.gameObject.name));
            DestroyImmediate( a );
            ++nRm;
        }

        sbOutput.Append( string.Format("Removed {0} unused animations\n", nRm ) );
        SetLog( sbOutput.ToString() );
    }


    void GatherHeirarchyNames(List<string> names, Transform t)
    {
        names.Add( t.name );
        foreach( Transform tChild in t )
            GatherHeirarchyNames( names, tChild);
    }

    bool RemoveUnusedChildrenUtil( GameObject go, HashSet<GameObject> goDel )
    {   
        bool bEmpty = false;
        Component [] rc = go.GetComponents<Component>();
        if (rc.Length <= 1) // Should be == 1 for the transform
            bEmpty = true;

       
        foreach (Transform t in go.transform)
        {
            if (!RemoveUnusedChildrenUtil(t.gameObject, goDel))
                bEmpty = false;
            else
                goDel.Add( t.gameObject );
        }
        return bEmpty;
    }
    void RemoveChildrenFromSet( GameObject go, HashSet<GameObject> hashGo )
    {
        foreach (Transform t in go.transform)
        {
            if( hashGo.Contains( t.gameObject) )
                hashGo.Remove( t.gameObject );

            RemoveChildrenFromSet( t.gameObject, hashGo );
        }
    }

    void RemoveUnusedChildren()
    {
        StringBuilder sbOutput = new StringBuilder();

        GameObject [] AllGameObjects = (GameObject[])GameObject.FindObjectsOfType( typeof(GameObject));
        sbOutput.Append( string.Format( "There are a total of {0} game objects in the scene", AllGameObjects.Length ));

        HashSet<GameObject> sel = new HashSet<GameObject>();
        foreach (GameObject go in Selection.gameObjects)
        {
            if( !AssetDatabase.Contains(go ) )
                sel.Add( go );
        }

        foreach (GameObject go in sel)
            RemoveChildrenFromSet(go, sel);

        HashSet<GameObject> HashDel = new HashSet<GameObject>();
        foreach( GameObject go in sel )
            RemoveUnusedChildrenUtil( go, HashDel );

        if (sel.Count == 0)
        {
            m_Log = "No scene objects detected to be selected. Operation Aborted";
            return;
        }

        if (HashDel.Count > 0)
            Undo.RegisterSceneUndo( "Removed Unused Children");

        List<string> delNames = new List<string>();
        foreach( GameObject go in HashDel )
            GatherHeirarchyNames(delNames, go.transform );


        HashDel.RemoveWhere( go => { return PrefabUtility.GetPrefabObject(go) != null; } );

        sbOutput.Append( string.Format( "Removed {0} GameObjects\n", delNames.Count ));
        sbOutput.Append( string.Format( "Removed {0} Branches\n", HashDel.Count ));

        sbOutput.Append( "==============================\n");
        foreach (GameObject go in HashDel)
        {

            sbOutput.Append( string.Format( "Deleting branch {0}\n", go.name) );    
            DestroyImmediate( go );
        }

        AllGameObjects = (GameObject[])GameObject.FindObjectsOfType( typeof(GameObject));
        sbOutput.Append( string.Format( "There are a total of {0} game objects in the scene", AllGameObjects.Length ));

        sbOutput.Append( "==============================\n");
        foreach (string s in delNames)
            sbOutput.Append( string.Format( "Removed GameObject {0}\n", s));

        SetLog( sbOutput.ToString() );
    }

    void ConvertWhiteDiffuseToMobile()
    {
        StringBuilder sbOutput = new StringBuilder();
        MeshRenderer [] renderers = (MeshRenderer[])GameObject.FindObjectsOfType( typeof(MeshRenderer));
        HashSet<Material> materials = new HashSet<Material>();
        foreach (MeshRenderer r in renderers)
        {
            foreach (Material m in r.sharedMaterials)
                materials.Add( m );
        }
        sbOutput.Append( string.Format("Found {0} unique Materials\n", materials.Count));
        sbOutput.Append( "==============================\n");

        int nCt = 0;
        foreach (Material m in materials)
        {
            if (m.shader.name == "Diffuse" && m.HasProperty("_Color") )
            {
                Color c = m.GetColor( "_Color");
                if (c.r == 1.0f && c.g == 1.0f && c.b == 1.0f)
                {
                    sbOutput.Append( string.Format("Converted material {0} from diffuse to mobile diffuse\n", m.name ));
                    m.shader = Shader.Find( "Mobile/Diffuse");

                    ++nCt;
                }

            }
        }

        sbOutput.Append( "==============================\n");
        sbOutput.Append( string.Format( "Converted {0} materials", nCt ) );

        SetLog( sbOutput.ToString() );
    }

    void FindDuplicateTextures( )
    {
        StringBuilder sbOutput = new StringBuilder();

        MeshRenderer [] renderers = (MeshRenderer[])GameObject.FindObjectsOfType( typeof(MeshRenderer));
        sbOutput.Append( string.Format("Found {0} unique MeshRenderers\n", renderers.Length));

        HashSet<Material> materials = new HashSet<Material>();
        foreach (MeshRenderer r in renderers)
        {
            foreach (Material m in r.sharedMaterials)
                materials.Add( m );
        }

        sbOutput.Append( string.Format("Found {0} unique Materials\n", materials.Count));

         //http://answers.unity3d.com/questions/179255/a-way-to-iterateenumerate-shader-properties.html
        string [] KnownBuiltinTextures = 
        { 
            "_BackTex", "_BumpMap", "_BumpSpecMap", "_Control", "_DecalTex",
            "_Detail", "_DownTex", "_FrontTex", "_GlossMap", "_Illum", "_LeftTex",
            "_LightMap", "_LightTextureB0", "_MainTex", "_ParallaxMap", "_RightTex",
            "_ShadowOffset", "_Splat0", "_Splat1", "_Splat2", "_Splat3",
            "_TranslucencyMap", "_UpTex", "_Tex", "_Cube" 
        };

        Dictionary< string, TextureNameCollection> uniqueNames = 
            new Dictionary< string, TextureNameCollection>();

        HashSet<string> UniquePaths = new HashSet<string>();
        foreach (Material m in materials)
        {
            foreach (string s in KnownBuiltinTextures)
            {
                if( !m.HasProperty(s))
                    continue;

                Texture t = m.GetTexture(s);
                if( t == null )
                    continue;

                string texPath = AssetDatabase.GetAssetOrScenePath( t );
                UniquePaths.Add( texPath);

                FileInfo fi = new FileInfo( texPath );

                TextureNameCollection tnc;
                if( !uniqueNames.TryGetValue( fi.Name, out tnc ) )
                {
                    tnc = new TextureNameCollection();
                    tnc.m_Texture = t;
                    uniqueNames.Add( fi.Name, tnc );
                }
				
				if( !tnc.m_References.ContainsKey( m ) )
					tnc.m_References.Add( m, new List<string>() );
				
                tnc.m_References[m].Add( s );
                tnc.m_UniqueFullPaths.Add( texPath );
            }
        }

        foreach( string s in UniquePaths)
            sbOutput.Append( s + '\n');

        sbOutput.Append( string.Format( "Found {0} unique texture filenames from {1} unique paths\n", 
            uniqueNames.Count, 
            UniquePaths.Count ));

        foreach (KeyValuePair< string, TextureNameCollection> kvp in uniqueNames)
        {
            int nCt = kvp.Value.m_UniqueFullPaths.Count;
            if (nCt > 1)
            { 
                sbOutput.Append( string.Format( "Found duplicate {0} instanced {1} times\n", kvp.Key, nCt ));

                foreach (string path in kvp.Value.m_UniqueFullPaths)
                    sbOutput.Append( '\t' + path + '\n' );
            }
        }

        foreach (KeyValuePair<string, TextureNameCollection> kvpFilename in uniqueNames)
        {
            // This could be consolidated with the top loop, but I just feel better with
            // it being a second pass.
            int nCt = kvpFilename.Value.m_References.Count;
            if (nCt > 1)
            {
                foreach (KeyValuePair<Material, List<string>> kvpMatEntry in kvpFilename.Value.m_References)
                {
                    Material mat = kvpMatEntry.Key;
					
					foreach( string s in kvpMatEntry.Value )
                    	mat.SetTexture( s, kvpFilename.Value.m_Texture );
                }
            }
        }
		
		SetLog( sbOutput.ToString() );
    }
	
	void SetLog( string strText )
	{
		m_Files = new string [0];
		m_Log = strText;

        if (m_OutputFile != "")
        { 
            StreamWriter sw = new StreamWriter( m_OutputFile, true);
			sw.Write( "\n\n====================\n=\n=\t" + System.DateTime.Now + "\n=\r\n====================\n" );
            sw.Write( m_Log );
            sw.Close();
        }
	}
}
