﻿using UnityEngine;
using System.Collections;

public class ButtonQuitScript : MonoBehaviour 
{
	void OnClick()
	{
		Debug.Log ("Game Exited");
		Application.Quit();
	}
}
