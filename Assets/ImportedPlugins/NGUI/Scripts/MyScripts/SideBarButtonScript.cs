﻿using UnityEngine;
using System.Collections;

public class SideBarButtonScript : MonoBehaviour {
	public GameObject Open;
	private bool flag = true;
	public GameObject Button;
	Vector3 temp = new Vector3(-190,0,0); 
	Vector3 temp1 = new Vector3(-270,0,0);
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnClick()
	{
		if (flag) {
			NGUITools.SetActive (Open, true);
			Button.transform.localPosition =  temp;
			flag = false;
		} else 
		{
			NGUITools.SetActive (Open, false);
			Button.transform.localPosition =  temp1;
			flag = true;
		}
	}
}
