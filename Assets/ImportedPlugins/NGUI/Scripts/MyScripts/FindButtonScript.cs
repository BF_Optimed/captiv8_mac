﻿using UnityEngine;
using System.Collections;

public class FindButtonScript : MonoBehaviour {
	public GameObject Open;
	private bool flag = true;


	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
		if (flag) {
			NGUITools.SetActive (Open, true);
			flag = false;
		} else {
			NGUITools.SetActive (Open, false);
			flag = true;
		}
	}


}
