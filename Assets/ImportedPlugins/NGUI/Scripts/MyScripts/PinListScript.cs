﻿using UnityEngine;
using System.Collections;

public class PinListScript : MonoBehaviour {
	public GameObject uiObject;
	public GameObject parentObject;
	public int numberOfButtons;
	private GameObject newButton;
	private string[] names = new string[] {"Pin1","Pin2","Pin3","Pin4","Pin5","Pin6","Pin7", "Pin8"};
	Vector3 temp = new Vector3(0,-60,0);
	Vector3 temp1= new Vector3(1,1,0);
	private bool fClicked = false;
	private int i = 0;
	
	void Start()
	{
		createButtons ();
	}
	
	public void createButtons() 
	{
		for (i = 0; i < numberOfButtons; i++) 
		{
			newButton = NGUITools.AddChild(parentObject, uiObject);
			UILabel newButtonLabel = newButton.GetComponentInChildren<UILabel>();
			newButton.name = names[i];
			newButtonLabel.text = names[i];
			newButton.transform.localPosition =  temp1;
			temp1=temp+temp1;
		}
		if ( fClicked && Application.platform != RuntimePlatform.IPhonePlayer )
		{
			Debug.Log("Player mouse-clicked on row " + names[i]);
			
			
		}
	}
	
	void OnClick()
	{
		
	}
	
	public GameObject parentobject {
		get { return uiObject; }
		set { uiObject = value; }
	}
}
