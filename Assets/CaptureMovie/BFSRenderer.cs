﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Assets.CaptureMovie;
public  class BFSRenderer : MonoBehaviour 
{
    private static BFSReplayManager replayManager;
	public GameObject [] objPrefabs;
	public GameObject CurrentObj;
    private static List<BFSRData> RecordedData;
    string DirectoryPath = null;
    private const string BFS_MAINCAMERA_TAG = "MainCamera";
    private string _imageLocation;
	// Use this for initialization
	void Start()
	{
        //Screen.SetResolution(1366, 768, false);
        string[] args = System.Environment.GetCommandLineArgs();
		CurrentObj = GameObject.Find ("Overview");
        CurrentObj.SetActive(true);
        replayManager = GameObject.Find("BFSRecorder").GetComponent<BFSReplayManager>();
        
            
#if !UNITY_EDITOR
        try
        {
            if(args == null || args.Length<=0)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            Arguments CommandLine = new Arguments(args);
            if (CommandLine["ImageFrameId"] != null)
            {
                replayManager.ImageFrameId = Convert.ToInt32(CommandLine["ImageFrameId"]);
            }
            else
            {
                replayManager.ImageFrameId =-1;
            }
            foreach (string arg in args)
            {
                if (!arg.StartsWith("-") && !arg.EndsWith(".exe"))
                {
                    DirectoryPath = arg;
                }
            }
        }
        catch
        {
           System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
       
#else
        DirectoryPath = @"C:\Users\ashaikh\AppData\LocalLow\Optimed\Eye\Default\Test Video";   
#endif
        if (string.IsNullOrEmpty(DirectoryPath))
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
        ReadFromDisk(DirectoryPath);
        _imageLocation = DirectoryPath+"\\" + new DirectoryInfo(DirectoryPath).Name + ".png";
        GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<AVProMovieCaptureFromScene>()._outputFolderPath = DirectoryPath;
        GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<AVProMovieCaptureFromScene>()._forceFilename = new DirectoryInfo(DirectoryPath).Name + ".avi";
        GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<AVProMovieCaptureFromScene>().StartCapture();
        replayManager.IsMovieCapture = true;
        replayManager.PlayRecording(RecordedData);
	}

	/*Required for recording*/
	public GameObject GetModelObject(string ModelName)
	{
        try
        {
            for (int i = 0; i <= objPrefabs.Length - 1; i++)
            {

                if (objPrefabs[i].name == ModelName)
                {
                    return objPrefabs[i];
                }
                else
                {
                    objPrefabs[i].SetActive(false);
                }
            }
            throw new Exception();
        }
        catch (Exception)
        {
            LogError(new Exception("Model not found : " + ModelName), -12001 );
            throw new Exception();
        }
	}

    public void ReadFromDisk(string FileName)
    {
        try
        {
            string[] FilePath = Directory.GetFiles(FileName, "*.xml", SearchOption.TopDirectoryOnly);
            //DirectoryInfo info = new DirectoryInfo(FileName);
            //string location = Path.Combine(FileName, info.Name);
            var xmldata = new List<BFSRData>();
            GetObject(ref xmldata, FilePath[0]);
            RecordedData = xmldata;
        }
        catch (Exception e)
        {
            LogError(new Exception("Not able to read xml : " + e.Message), -12002);
        }
        
    }
    public  bool GetObject<T>(ref T optimizedData, string location)
    {
        try
        {
            using (FileStream stream = new FileStream(location, FileMode.Open))
            {
                XmlTextReader reader = new XmlTextReader(stream);
                var x = new XmlSerializer(optimizedData.GetType());
                optimizedData = (T)x.Deserialize(reader);
                return true;
            }
        }
        catch (Exception e)
        {
            LogError(new Exception("Not able to deserialize xml : " + e.Message), -12003);
        }
        return false;
    }

    public  void LogError(Exception e, int errorCode)
    {
        try
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(System.IO.Path.Combine(DirectoryPath, "Render.log"));
            file.Write(errorCode + " " + e.Message);
            file.Close();
            Application.Quit();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
        catch (Exception)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }


    public void StopCapture()
    {
        replayManager.enabled = false;
        GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<AVProMovieCaptureFromScene>().StopCapture();
        System.Diagnostics.Process.GetCurrentProcess().Kill();
        Application.Quit();
    }

    internal string GetImageLocation()
    {
        return _imageLocation;
    }
}
