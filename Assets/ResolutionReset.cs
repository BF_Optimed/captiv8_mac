﻿using UnityEngine;
using System.Collections;

public class ResolutionReset : MonoBehaviour
{
    public GameObject ResetLabel;

    void OnClick()
    {
        NGUITools.SetActive(ResetLabel, true);
    }
}