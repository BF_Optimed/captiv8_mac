﻿using UnityEngine;
using System.Collections;

public class BackgroundResize : MonoBehaviour {

    public GameObject grid;

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<UISprite>().bottomAnchor.absolute = ((grid.transform.childCount) * 25 + 30)*-1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
