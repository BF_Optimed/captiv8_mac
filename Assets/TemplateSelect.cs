﻿using UnityEngine;
using System.Collections;
using Assets.DBHelper;
using System.Linq;
public class TemplateSelect : MonoBehaviour
{

    public GameObject ParentObject;
    public GameObject UiObject;
    private GameObject _button;

    public Templates[] Temps;



    void Start()
    {
        CreateAllButtons();
    }

    void CreateAllButtons()
    {
        ContinuousVars con = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();

        Debug.Log("Controller selected = " + con);
        
        var video = new Video().GetVideo(con.LoadedVideoDataPath);
        Debug.Log("Loaded video is : " + video.Name);

        Temps = new Templates().GetTemplates("AnimationId In ('0','" + video.AnimationId + "')").ToArray();

        foreach (var t in Temps)
        {
            CreateNewButton(t);
        }
    }

    void CreateNewButton(Templates t)
    {
        _button = NGUITools.AddChild(ParentObject, UiObject);
        var newButtonLabel = _button.GetComponentInChildren<UILabel>();
        _button.name = t.Id.ToString();
        newButtonLabel.text = t.Name;
    }
}
