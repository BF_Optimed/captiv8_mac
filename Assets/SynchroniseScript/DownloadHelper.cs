﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.SynchroniseScript
{
    class DownloadHelper
    {
        public IEnumerator Download(string url,string path)
        {
            WWW www = new WWW(url);

            while (!www.isDone)
            {
                Debug.Log("downloaded " + (www.progress * 100).ToString() + "%...");
                yield return null;
            }

            string fullPath = path;
            File.WriteAllBytes(fullPath, www.bytes);
            yield return 0;
        }
    }
}
