﻿using Assets.DBHelper;
using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Assets.SynchroniseScript
{
    class Captiv8Videos:SynchroniseBase<Captiv8Videos>
    {
        protected Video video;
        protected Action<string,bool> CallBack;
        public static void Play(Video video)
        {
            System.Diagnostics.Process.Start(video.Location);
        }
        public IEnumerator DownloadVideo(string id, Action<string,bool> RemoveFromQueue)
        {
            yield return null;
            try
            {
                CallBack = RemoveFromQueue;
                video = new Video().GetVideo(id);
                if (video.FileCount <= 0)
                {
                    video.Status = Status.Completed;
                    video.UpdateVideo();
                    SynchroniseSingleton.Instance.IsDownloading = false;
                    CallBack(video.Id, true);
                    throw new Exception("Not valid video");
                }
                var imgUrl = GetImagePath();
                var path = GetDownloadPath(id);
                DirectoryValidator(path);
                StartCoroutine(Download(imgUrl, path, id, CompleteHandler));
            }
            catch
            {
                CallBack(video.Id, false);
            }
            //CompleteHandler(id, path);
            
        }

        private IEnumerator Download(string imgUrl, string path, string videoId,Action<string,string> complete)
        {
           
            WWW www = new WWW(imgUrl);
            while (!www.isDone)
            {
                yield return null;
            }
            File.WriteAllBytes(Path.ChangeExtension(Path.Combine(path, video.Id.Trim()), "png"), www.bytes);
            complete(videoId, path);
        }
        public void CompleteHandler(string videoId,string path)
        {
            updateDatabase(path);
            SynchroniseSingleton.Instance.IsDownloading = false;
            CallBack(this.video.Id,true);
            //Debug.Log("Done");
        }
        private string GetImagePath()
        {
            var imgPath = video.GetFile(AnimationModel.AnimationImage).CloudPath;
            if(ReferenceEquals(imgPath,null))
            {
                SynchroniseSingleton.Instance.IsDownloading = false;
                throw new FileNotFoundException("Image file not found");
            }
            return imgPath;
        }
        protected string GetDownloadPath(string videoId)
        {
            if (ReferenceEquals(video, null)) video = new Video().GetVideo(videoId);
            return Path.Combine(Path.Combine(Application.persistentDataPath, video.Category),video.Id.Trim() );
        }
        protected void DirectoryValidator(string location)
        {
            if (!Directory.Exists(location))
            {
                Directory.CreateDirectory(location);
            }
        }
        protected void updateDatabase(string path)
        {
            var localFiles = Directory.GetFiles(path, "*.png", SearchOption.TopDirectoryOnly);
            foreach (var file in localFiles)
            {
                var model = GetFiletype(file);
                video.UpdateVideo(model, file);
                break;
            }
            video.Status = Status.Completed;
            video.UpdateVideo();
        }
        protected AnimationModel GetFiletype(string file)
        {
            var type = Path.GetExtension(file);
            switch (type)
            {
                case ".xml":
                    return AnimationModel.AnimationCoordinates;
                case ".png":
                case ".jpg":
                    return AnimationModel.AnimationImage;
                case ".vtt":
                    return AnimationModel.AnimationCaptions;
                case ".wav":
                case ".mp3":
                    return AnimationModel.AnimationSound;
            }
            return AnimationModel.None;
        }
    }
}
