﻿using Assets.DBHelper;
using Ionic.Zip;
using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
namespace Assets.SynchroniseScript
{
    class DownloadManager:SynchroniseBase<DownloadManager>
    {
        protected Video video;
        protected Action<string,bool> CallBack;
        public IEnumerator DownloadVideo(string videoId,Action<string,bool> callBack)
        {
            try
            {
                CallBack = callBack;
                var url = GetZipUrl(videoId);
                var path = GetDownloadPath(videoId);
                StartCoroutine(Download(url, path, Extract));
            }
            catch
            {
                CallBack(videoId, false);
            }
            yield return null;
        }
        protected string GetDownloadPath(string videoId)
        {
            if (ReferenceEquals(this.video, null))
            {
                this.video = new Video().GetVideo(videoId);
            }
            return Path.Combine(Path.Combine(Application.persistentDataPath, this.video.Category), videoId);
        }
        protected void DirectoryValidator(string location)
        {
            if (!Directory.Exists(location))
            {
                Directory.CreateDirectory(location);
            }
        }
        protected void updateDatabase(string path)
        {
            var localFiles = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly);
            foreach (var file in localFiles)
            {
                var model = GetFiletype(file);
                this.video.UpdateVideo(model, file);
            }
            this.video.Status = Status.Completed;
            this.video.UpdateVideo();
        }
        protected AnimationModel GetFiletype(string file)
        {
            var type = Path.GetExtension(file);
            switch (type)
            {
                case ".xml":
                    return AnimationModel.AnimationCoordinates;
                case ".png":
                case ".jpg":
                    return AnimationModel.AnimationImage;
                case ".vtt":
                    return AnimationModel.AnimationCaptions;
                case ".wav":
                case ".mp3":
                    return AnimationModel.AnimationSound;
            }
            return AnimationModel.None;
        }

        private  IEnumerator Download(string url, string path,Action<string,string> extract)
        {
            DirectoryValidator(path);
            var toPath = Path.ChangeExtension(Path.Combine(Path.GetTempPath(), video.Id), "zip");
            WWW www = new WWW(url);
            //Debug.Log("URL : " + url);
            while (!www.isDone)
            {
                yield return null;
            }

            File.WriteAllBytes(toPath, www.bytes);
            Extract(toPath,path);
            yield return 0;
        }
        
        private void Extract(string fileName,string extractTo)
        {
            try
            {
                using (var zipFile = ZipFile.Read(fileName))
                {
                    zipFile.ExtractAll(extractTo, ExtractExistingFileAction.OverwriteSilently);
                }
                updateDatabase(extractTo);
                SynchroniseSingleton.Instance.IsDownloading = false;
                CallBack(video.Id, true);    
            }
            catch
            {
                CallBack(video.Id, false);
            }
            
        }
        
        private string GetZipUrl(string videoId)
        {
            video = new Video().GetVideo(videoId);
            var file = video.GetFile(AnimationModel.AnimationDocument);
            if (ReferenceEquals(file, null))
            {
                SynchroniseSingleton.Instance.IsDownloading = false;
                throw new FileNotFoundException("Zip file not found to download");
            }
            return file.CloudPath;
        }
    }
}
