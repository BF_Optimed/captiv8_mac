﻿using Assets.DBHelper;
using System;
using System.IO;
using UnityEngine;

namespace Assets.SynchroniseScript
{
    public class SynchroniseBase<T>: MonoBehaviour
        where T : Component
    {
            public static T Instance { get; private set; }

            public virtual void Awake()
            {
                if (Instance == null)
                {
                    Instance = this as T;
                    DontDestroyOnLoad(this);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }
