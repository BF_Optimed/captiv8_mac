﻿using Assets.DBHelper;
using Assets.OptimedClient;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
namespace Assets.SynchroniseScript
{
    public delegate void DownloadingComplete();
    class SynchroniseSingleton : SynchroniseBase<SynchroniseSingleton>
    {
        private static SynchroniseManager _syncManager;
        private static string _connectionString;
        public static UILabel SyncCountLabel{get;private set;}
        public event DownloadingComplete DownloadingCompleteEvent;
        public ICollection<string> DownloadAnimationId= new List<string>();
        private string cmsUrl = Runtime.Default.Captiv8Url;
        private bool _isDownloading;
        public string CmsURL
        {
            get
            {
                return cmsUrl;
            }
        }
        public bool IsDownloading 
        { 
            get
            {
                return _isDownloading   ;
            }
             set
            {
                _isDownloading = value;
                 if(!_isDownloading)
                 {
                     if(DownloadingCompleteEvent!=null)
                     {
                         DownloadingCompleteEvent();
                     }
                 }
            }
         }
        public string ConnectionString
        {
            get { return _connectionString; }
        }
        private void AddToQueue(string id)
        {
            DownloadAnimationId.Add(id);
            if (DownloadAnimationId.Count > 0)
            {
                Download(DownloadAnimationId.First());
            }
        }
        public void RemoveFromQueue(string id,bool status)
        {
            GameObject.Find("PersistentController").GetComponent<Hibernation>().CanHibernate = true;
            DownloadAnimationId.Remove(id);
            //Debug.Log("Downloaded COMPLETED : " + id + "and remaining count : " + DownloadAnimationId.Count + " and it status was : " + status);
            if(!status)
            {
                IsDownloading = false;
            }
            if (DownloadAnimationId.Count > 0)
            {
                Download(DownloadAnimationId.First());
            }
        }
        void Start()
        {
            IsDownloading = false;
            _connectionString = Application.persistentDataPath + "/OptimedDB.db";
            if(!System.IO.File.Exists( Application.persistentDataPath + "/OptimedDB.db"))
            {
                new InitDatabase().CreateTables();
            }
            
            SyncCountLabel = GameObject.Find("SyncButton").GetComponentInChildren<UILabel>();
            _syncManager = new SynchroniseManager(cmsUrl);
            _syncManager.updateSyncCount = UpdateLabel;
            StartCoroutine(startSync());
            StartCoroutine(GetQueuedVideos());
        }
        static IEnumerator startSync()
        {
            yield return null;
            _syncManager.StartSyncClient(InstallationId.GetInstallationId);
           
        }
        private void UpdateLabel(int updateCount)
        {
            Debug.Log("Sync Count : " + updateCount);
            SyncCountLabel.text = "" + updateCount;
        }
        public void AddAnimationToDownload(List<string> videoIds)
        {
            foreach (var id in videoIds)
            {
                AddToQueue(id);
            }
        }
        private IEnumerator GetQueuedVideos()
        {
            yield return null;
            var videosId = new Video().GetAllVideos(" Status = '" + Status.Downloading.ToString() + "'").Select(x=>x.Id);
            foreach(var id in videosId)
            {
                AddToQueue(id);
            }
           
        }
        public void Download(string id)
        {
            if(IsDownloading)
            {
                return;
            }
            Debug.Log("Downloaded Started : " + id + "and remaining count : " + DownloadAnimationId.Count );
            var video = new Video().GetVideo(id);
            if (video.Owner == Owner.captiv8plus && !IsDownloading)
            {
                IsDownloading = true;
                GameObject.Find("PersistentController").GetComponent<Hibernation>().CanHibernate = false;
                StartCoroutine(DownloadManager.Instance.DownloadVideo(id, RemoveFromQueue));
                return;
            }
            if (video.Owner == Owner.captiv8 && !IsDownloading)
            {
                IsDownloading = true;
                StartCoroutine(Captiv8Videos.Instance.DownloadVideo(id, RemoveFromQueue));
                return;
            }
        }
    }
}
