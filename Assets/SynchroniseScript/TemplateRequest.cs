﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Captiv8OnlineClient.Contracts;
using Newtonsoft.Json;
namespace Assets.SynchroniseScript
{
    public class TemplateRequest
    {
        public static RestClient GetOptimedClient(string url)
        {
            return new RestClient(url) { Timeout = Timeout.Infinite };
        }
        public TemplateCollection GetTemplates(string url, string registrationId)
        {
            var response = GetOptimedClient(url).Execute(MakeRequest(registrationId)).Content;
            return JsonConvert.DeserializeObject<TemplateCollection>(response);
        }
        private RestRequest MakeRequest(string registrationId)
        {
            var request = new RestRequest(string.Format("plusapi/installations/{0}", registrationId), Method.GET)
            {
                //AlwaysMultipartFormData = true
            };
            return request;
        }
        public class TemplateCollection
        {
            [JsonConstructor]
            public TemplateCollection()
            {

            }
            public ICollection<Template> templates { get; set; }
        }
    }
}
