﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SynchroniseClient;
using Captiv8OnlineClient.Contracts;
using Assets.DBHelper;
namespace Assets.SynchroniseScript
{
    public class SynchroniseManager
    {
        SynchroniseClient.SynchroniseClient _client;
        static ICollection<Templates> templates;
        static string registrationId;
        public Action<int> updateSyncCount { set; private get; }
        static string _url;
        public SynchroniseManager(string url)
        {
            _url = url;
            _client = new SynchroniseClient.SynchroniseClient(url);
        }
        public void StartSyncClient(string installationId)
        {
            registrationId = installationId;
            _client.StartSync(installationId);
            _client.Synchronised += _client_Synchronised;
        }
        void _client_Synchronised(object sender, SynchronisedEventArgs e)
        {
            try
            {
                UnityEngine.Debug.Log("Sync Process started.");
                FindNewAnimations(e.Animations);
                HandleTemplates(e.Templates);
                //HandleDeletedVideo(e.Animations);
                var count = new Video().GetAllVideos(" Status = '" + Status.Display.ToString() + "'").Count;
                updateSyncCount(count);
            }
            catch
            {

            }
             
        }
        void HandleDeletedVideo(IEnumerable<Animation> animations)
        {
            var deletedAnimation = DeletedAnimationList(animations);
            foreach (string animationId in deletedAnimation)
            {
                try
                {
                    var video = new Video().GetAllVideos(" AnimationId =  '" + animationId + "'").First();
                    video.Status = Status.Deleted;
                    video.UpdateVideo();
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.Log(ex.Message);
                }
                
            }

        }
        void  FindNewAnimations(IEnumerable<Animation> animations)
        {
            var newAnimations = NewAnimationList(animations);
            foreach (string animationId in newAnimations)
            {

                var animation = animations.Where(x => x.Id == animationId ).ToList().First();
                if (animation!= null)
                {
                    try
                    {
                        CreateNewVideo(animation);
                    }
                    catch (Exception ex)
                    {
                        UnityEngine.Debug.Log(ex.Message);
                    }
                }
            }
            
        }

        private void HandleTemplates(IEnumerable<Captiv8OnlineClient.Contracts.Template> cmsTemplates)
        {
            var templatesRequest = new TemplateRequest().GetTemplates(_url,registrationId).templates;
            templates = new Templates().GetTemplates("");

            foreach (var cmsTemplate in templatesRequest)
            {
                    if(templates.Count>0)
                    {
                        var template = templates.Where(x => x.CmsId == cmsTemplate.ID.ToString()).Select(x => x.CmsId).ToList().First();
                        if (string.IsNullOrEmpty(template))
                        {
                            CreateNewTemplate(cmsTemplate);
                        }
                    }
                    else
                    {
                        CreateNewTemplate(cmsTemplate);
                    }  
            }
        }

        private void CreateNewTemplate(Template cmsTemplate)
        {
            var template = new Templates()
            {
                CmsId=cmsTemplate.ID.ToString(),
                AnimationId=cmsTemplate.AnimationID,
                Name=cmsTemplate.Name,
                Content=cmsTemplate.Content
            };
            template.Save();
            //throw new NotImplementedException();
        }
        private Video CreateNewVideo(Animation animation)
        {
            var video = new Video()
            {
                AnimationId = animation.Id,
                Category = animation.Category,
                Duration = animation.Duration,
                Name = animation.Name,
                Description = animation.Description,
                Status = Status.Display,
                Location=animation.Location,
                Public=true,
                DateCreated=DateTime.Now,
                Locked=animation.Locked==1?true:false,
                Owner = (Owner)Enum.Parse(typeof(Owner), animation.Owner)
            };
            video.AddAnimationFile(GetFiles(animation.Files));
            video.SaveNewVideo();
            return video;
        }
        private List<FileDetail> GetFiles(ICollection<File> files)
        {
            var fileDetails= new List<FileDetail>();
            foreach (var file in files)
            {
                fileDetails.Add(CreateFile(file));
            }
            return fileDetails;
        }
        private FileDetail CreateFile(File file)
        {
            return new FileDetail
            {
                CloudPath=file.Location,
                md5=file.Md5,
                Model = (AnimationModel)Enum.Parse(typeof(AnimationModel), file.Model),
                Size=file.Size
            };
        }
        private List<string> GetServerAnimations(IEnumerable<Animation> animations)
        {
            return animations.Select(x => x.Id).ToList<string>();
        }
        private IEnumerable<string> GetLocalAnimations()
        {
            return new Video().GetAllVideos("").Select(x => x.AnimationId);
        }
        private IEnumerable<string> GetLastDisplayAnimation()
        {
            return new Video().GetAllVideos(" Status in ('" + Status.Display +"','" + Status.Viewed +"')" ).Select(x => x.AnimationId);
        }
        private IEnumerable<string> NewAnimationList(IEnumerable<Animation> animations)
        {
            var serverAnimation = GetServerAnimations(animations);
            var localAnimation = GetLocalAnimations();
            return serverAnimation.Except(localAnimation);
        }
        private IEnumerable<string> DeletedAnimationList(IEnumerable<Animation> animations)
        {
            var serverAnimation = GetServerAnimations(animations);
            var localAnimation = GetLastDisplayAnimation();
            return localAnimation.Except(serverAnimation);
        }
    }
    
}
