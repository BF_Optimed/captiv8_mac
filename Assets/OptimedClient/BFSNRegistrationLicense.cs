﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Captiv8OnlineClient;
using Captiv8OnlineClient.Contracts;
using UnityEngine;
using RestSharp;
using System.Threading;
using System.Net;
using Assets.Helper;
public class BFSNRegistrationLicense : MonoBehaviour
{
    private static bool isLocation=true;
    public static string ErrorMessage { get; private set; }
    public static string ErrorTitle { get; private set; }
    public static string location;
    public static string[] locationArray;
    public Action<bool, string, string> locationCallback;
    public  GameObject locationGO;
    string lusername; string lpassword; string ldescription; int ldevice;
    public  RestClient GetOptimedClient()
    {
        ErrorMessage = "Unknown";
        return new RestClient(Runtime.Default.Captiv8Url) { Timeout = Timeout.Infinite };
        //return new RestClient("www.captiv8online.com") { Timeout = Timeout.Infinite };
    }
    public void OptimiedRegistration(string username, string password, string description, int device,Action<bool,string,string > callback)
    {
        if (string.IsNullOrEmpty(username))
        {
            throw new ArgumentNullException("username");
        }
        if (string.IsNullOrEmpty(password))
        {
            throw new ArgumentNullException("password");
        }
        if (string.IsNullOrEmpty(description))
        {
            throw new ArgumentNullException("description");
        }
        if (device < 0)
        {
            throw new ArgumentOutOfRangeException("device");
        }
        Client client = new Client(GetOptimedClient());
        RegistrationRequest regRequest = new RegistrationRequest(username, password, description, device);
        //Debug.Log(SystemInfo.deviceUniqueIdentifier);
        if(!string.IsNullOrEmpty(location))
        {
            regRequest.Installations.Location = location;
            location = string.Empty;
        }
#if UNITY_IPHONE
    regRequest.Installations.HardwareId = SystemInfo.deviceUniqueIdentifier;
#endif
        InstallationResponse regResponse = client.Register(regRequest);
        if (!regResponse.Error)
        {
            try
            {
                if(regResponse.GetContent().Locations!=null)
                {
                    if(regResponse.GetContent().Locations.Count<string>()>0 && isLocation)
                    {
                        locationArray = regResponse.GetContent().Locations;
                        isLocation = false;
                        LocationRegistration(username, password, description, device, callback);
                        return;
                    }
                }
                OptimiedLicenseKeyRegistration(client, regResponse.GetContent().Installations.Id,callback);
                return;
            }
            catch (Exception e)
            {
                Debug.Log("Error my Exception");
                callback(false, ErrorTitle, e.Message);
            }
        }
        else
        {
            if (regResponse.GetErrors().Errors.Length > 0)
            {
                ErrorTitle = regResponse.GetErrors().Errors[0].Title;
                ErrorMessage = regResponse.GetErrors().Errors[0].Detail;
                if (string.IsNullOrEmpty(ErrorMessage) && ConvertInt.IntFetcher(regResponse.GetErrors().Errors[0].Code) == 2)
                {
                    ErrorMessage = "All installation keys are used.  Release a key from another installation or purchase additional installation bundles.";
                }
                Debug.Log(string.Format("Error title {0} , Message {1}", ErrorTitle, ErrorMessage));
            }
            callback(false, ErrorTitle, ErrorMessage);
            return;
        }

        //callback(false, true, ErrorTitle, ErrorMessage);
    }
    private void LocationRegistration(string username, string password, string description, int device, Action<bool, string, string> callback)
    {
        lusername = username;
        lpassword = password;
        ldescription = description;
        ldevice = device;
        locationCallback = callback;
        NGUITools.SetActive(locationGO, true);
    }
    public void UpdateLocation(string s)
    {
        location = s;
    }

    public void OnLocationSelection()
    {
        Client client = new Client(GetOptimedClient());
        RegistrationRequest regRequest = new RegistrationRequest(lusername, lpassword, ldescription, ldevice);
        if (!string.IsNullOrEmpty(location))
        {
            regRequest.Installations.Location = location;
            location = string.Empty;
        }
#if UNITY_IPHONE
    regRequest.Installations.HardwareId = SystemInfo.deviceUniqueIdentifier;
#endif
        InstallationResponse regResponse = client.Register(regRequest);
        if (!regResponse.Error)
        {
            try
            {
                OptimiedLicenseKeyRegistration(client, regResponse.GetContent().Installations.Id, locationCallback);
                return;
            }
            catch (Exception e)
            {
                locationCallback(false, ErrorTitle, e.Message);
            }
        }
        else
        {
            if (regResponse.GetErrors().Errors.Length > 0)
            {
                ErrorTitle = regResponse.GetErrors().Errors[0].Title;
                ErrorMessage = regResponse.GetErrors().Errors[0].Detail;
            }
            locationCallback(false, ErrorTitle, ErrorMessage);
            return;
        }
    }
    private void OptimiedLicenseKeyRegistration(Client client, string id, Action<bool, string, string> callback)
    {
        RegisterLicenseRequest licenseRequest = new RegisterLicenseRequest(id);
        InstallationResponse LicenseResponse = client.RegisterLicense(licenseRequest);
        if (!LicenseResponse.Error)
        {
            var encryption = new BFSEncryption();
            PlayerPrefs.SetString("InstallationId", LicenseResponse.GetContent().Installations.Id);
            Runtime.Default.RunManager=encryption.Encrypt(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),SystemInfo.deviceUniqueIdentifier);
            Runtime.Default.RuntimeId = encryption.Encrypt(LicenseResponse.GetContent().Installations.AnimationsCreationLimit.ToString(), SystemInfo.deviceUniqueIdentifier);
            Runtime.Default.SystemInterrupts = encryption.Encrypt(LicenseResponse.GetContent().Installations.ExpirationDate.ToString("dd/MM/yyyy HH:mm:ss"), SystemInfo.deviceUniqueIdentifier);
            Runtime.Default.Save();
            callback(true, null, null);
        }
        else
        {
            if (LicenseResponse.GetErrors().Errors.Length > 0)
            {
                ErrorTitle = LicenseResponse.GetErrors().Errors[0].Title;
                ErrorMessage = LicenseResponse.GetErrors().Errors[0].Detail;
            }
            callback(false, ErrorTitle, ErrorMessage);
        }
    }

}
