﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.OptimedClient
{
    public class InstallationId
    {
        private static string installationId;
        public static string GetInstallationId
        {
            get
            {
                if(string.IsNullOrEmpty(installationId))
                {
                    installationId= PlayerPrefs.GetString("InstallationId");
                }
                if(string.IsNullOrEmpty(installationId))
                {
                    throw new Exception("There is no Installation id.");
                }
                return installationId;
            }
        }
        public static string SetInstallationId
        {
            set
            {
                installationId = value;
            }
        }

        
    }
}
