﻿using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;
using System.Net;
using Captiv8OnlineClient.Contracts;
using Newtonsoft.Json;
namespace Assets.OptimedClient
{
    public class ReleaseInstallationId: MonoBehaviour
    {
        private RestClient GetOptimedClient()
        {
            return new RestClient(Runtime.Default.Captiv8Url) { Timeout = Timeout.Infinite };
        }
        private RestRequest RequestRelease()
        {
            var request = new RestRequest("plusapi/installations/register", Method.POST);
            string releaseJson = "{\"installations\": {\"id\": \"" + InstallationId.GetInstallationId + "\",\"action\": \"deregister\"}}";
            request.AddParameter("application/json", releaseJson, ParameterType.RequestBody);
            return request;
        }
        public IEnumerator ReleaseId(Action<bool,string,string> callback)
        {
            yield return null;
            var client = GetOptimedClient();
            var request = RequestRelease();
            var response = client.Execute(request);
            if(response.StatusCode==HttpStatusCode.NoContent)
            {
                PlayerPrefs.DeleteKey("InstallationId");
                InstallationId.SetInstallationId = null;
                callback(true, null, null);
            }
            else
            {
                var error = JsonConvert.DeserializeObject<ErrorContent>(response.Content);
                if(error.Errors.Count<Error>()>0)
                {
                    callback(false, error.Errors[0].Title, error.Errors[0].Detail);
                }
                else
                {
                    callback(false, "unknown", "unknown");
                }
                
            }
        }
    }
}
