﻿using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public BFSUTransition BlackoutScript;
    public BFSMCamZoom CamZoom;
    public GameObject Container;
    public BtnController Controller;
    public UIGrowthScript GrowthScript;
    public GameObject Part;
    

    private void Start()
    {
        
        Controller = GameObject.Find("OverviewBtnController").GetComponent<BtnController>();
        CamZoom = Camera.main.GetComponent<BFSMCamZoom>();
        BlackoutScript = GameObject.Find("Blackout").GetComponent<BFSUTransition>();
        GrowthScript = GameObject.Find("SubGroupButton").GetComponent<UIGrowthScript>();
        Part = GameObject.Find(gameObject.name);
    }

    private void OnClick()
    {
        var partAuto = Part.GetComponent<BFSUAutopilot>();
        if (Part != null && partAuto != null)
        {
            AutopilotTransition(partAuto);
        }
        else
        {
            NonAutopilotTransition();
        }
    }

    private void AutopilotTransition(BFSUAutopilot autopilot)
    {
        if (!Controller.Autopiloting)
        {
            GrowthScript.InputRecieved();
            if (Part.name == "Overview")
            {
                RevertToOverview();
            }
            else
            {
                ContinueAutopiloting(autopilot);
            }
        }
    }

    private void RevertToOverview()
    {
        BlackoutScript.GetComponent<BFSUTransition>().Begin(gameObject.name, true);
    }

    private void ContinueAutopiloting(BFSUAutopilot autopilot)
    {
        autopilot.MoveTo();
        Controller.Autopiloting = true;
        CamZoom.Target = Part;
    }

    private void NonAutopilotTransition()
    {
        GrowthScript.InputRecieved();
        BlackoutScript.Begin(gameObject.name, true);
    }
}