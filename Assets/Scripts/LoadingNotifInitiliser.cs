﻿using UnityEngine;
using System.Collections;
using Assets.SynchroniseScript;
public  class LoadingNotifInitiliser : MonoBehaviour
{
    public GameObject Title;
    public UILabel TitleText { get; set; }
    public static bool IsBusy { get; set; }
	void Awake () 
    {
        TitleText = Title.GetComponentInChildren<UILabel>();
	}

    public void DeleteSelf()
    {
        Destroy(gameObject);
    }

    private void SetUpLoadText(string titleText)
    {
        TitleText.text = titleText;
    }

    public void CreateLoadingNotif(string titleText)
    {
        SetUpLoadText(titleText);
    }

    void Update()
    {
        if(!IsBusy)
        {
            Destroy(gameObject);
        }
    }

   
}
