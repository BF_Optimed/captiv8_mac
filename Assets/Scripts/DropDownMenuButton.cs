﻿using UnityEngine;
using System.Collections;

public class DropDownMenuButton : MonoBehaviour {

	public GameObject part;
	public BFSMCamZoom camZoom;
	public BtnController controller;
	public GameObject container;
	public BFSUTransition blackoutScript;
	public UIGrowthScript growthScript;
    private BFSASlider _slider;

	// Use this for initialization
	void Start () {
        if (GameObject.Find("SliderController") != null)
            _slider = GameObject.Find("SliderController").GetComponent<BFSASlider>();

		controller = GameObject.Find ("Controller").GetComponent<BtnController> ();
		camZoom = Camera.main.GetComponent<BFSMCamZoom> ();
		blackoutScript = GameObject.Find ("Blackout").GetComponent<BFSUTransition> ();
		growthScript = GameObject.Find ("SubGroupButton").GetComponent<UIGrowthScript> ();
		if (Application.loadedLevelName == "Overview")
			part = GameObject.Find (gameObject.name);
	}
	// Update is called once per frame
	void Update () {
	}
	void OnClick()
	{
        if (_slider != null)
            _slider.ResetAnims();

				//check part exists
				if (part != null && part.GetComponent<BFSUAutopilot> () != null) {    
						//check not already doing the action on another object
						if (!controller.Autopiloting) {
								//reset all UI and rotate view to object
								growthScript.InputRecieved ();
								if (part.name == "Overview") {
										blackoutScript.GetComponent<BFSUTransition> ().Begin (gameObject.name, true);
								} else {
                                        controller.GetComponent<BtnController>().Reset();
										part.GetComponent<BFSUAutopilot> ().MoveTo ();
										controller.Autopiloting = true;
										//controller.PartsButtonContainer.SetActive (false);
										camZoom.Target = part;
								}
						}
				} else {
						//fade and change scene
						growthScript.InputRecieved ();
						blackoutScript.Begin (gameObject.name, true);
				}
		}
}
