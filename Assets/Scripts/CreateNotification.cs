﻿using UnityEngine;

public class CreateNotification : MonoBehaviour
{
    public GameObject Notif;
    public GameObject OkNotif;
    public GameObject LoadingNotif;
    public GameObject UIroot;

    private void Start()
    {
        UIroot = GameObject.FindWithTag("UIroot");
    }

    public void CreateOneButton(string title, string desc, string btn1, string btn2, EventDelegate btn1Action)
    {
        NGUITools.AddChild(UIroot, Notif);
        UIroot.GetComponentInChildren<NotificationInitiliser>()
            .CreateOneButtonNotif(btn1, btn2, title, desc, btn1Action);
    }

    public void CreateTwoButton(string title, string desc, string btn1, string btn2, EventDelegate btn1Action,
        EventDelegate btn2Action)
    {
        NGUITools.AddChild(UIroot, Notif);
        UIroot.GetComponentInChildren<NotificationInitiliser>()
            .CreateTwoButtonNotif(btn1, btn2, title, desc, btn1Action, btn2Action);
    }

    public void CreateOkayButton(string title, string desc, string btn1)
    {
        NGUITools.AddChild(UIroot, OkNotif);
        UIroot.GetComponentInChildren<OkInitiliser>().CreateOkayButton(btn1, title, desc);
    }

    public void CreateOkayKillButton(string title, string desc, string btn1)
    {
        NGUITools.AddChild(UIroot, OkNotif);
        UIroot.GetComponentInChildren<OkKillInitiliser>().CreateOkayButton(btn1, title, desc);
    }

    public void CreateLoadingNotification(string title)
    {
        NGUITools.AddChild(UIroot, LoadingNotif);
    }
}