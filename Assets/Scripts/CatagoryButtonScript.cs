﻿using UnityEngine;

public class CatagoryButtonScript : MonoBehaviour
{
    public UIGrowthScript CloseList;
    public UILabel Label;

    private void Start()
    {
        Label = GameObject.Find("Category").GetComponentInChildren<UILabel>();
        CloseList = GameObject.Find("Category").GetComponent<UIGrowthScript>();
    }

    public void OnPressed()
    {
        Label.text = gameObject.GetComponentInChildren<UILabel>().text;
        GameObject.Find("Controller").GetComponent<RecordScript>().SetSaveCategory(Label.text);
        CloseList.Close();
    }
}