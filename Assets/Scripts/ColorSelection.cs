﻿using Assets.Scripts;
using UnityEngine;

public class ColorSelection : MonoBehaviour
{
    public Color CurrentColor;
	public GameObject ColorPicker;
    public GameObject ColorPickerBtn;
    private UISprite _colorpicker;

    private void Start()
    {
        CurrentColor = DefaultColor();
        _colorpicker = ColorPickerBtn.GetComponent<UISprite>();

        switch (PlayerPrefs.GetString(MagicStrings.DefaultColourKey))
        {
            case "green":
                ChangeColorGreen();
                break;
            case "red":
                ChangeColorRed();
                break;
            case "blue":
                ChangeColorBlue();
                break;
            case "yellow":
                ChangeColorYellow();
                break;
            case "black":
                ChangeColorBlack();
                break;
        }
    }

    public void ChangeColorRed()
    {
        CurrentColor = Color.red;
		NGUITools.SetActive (ColorPicker,false);
        _colorpicker.spriteName = MagicStrings.ColorpickerRed;
    }

    public void ChangeColorGreen()
    {
        CurrentColor = Color.green;
		NGUITools.SetActive (ColorPicker,false);
        _colorpicker.spriteName = MagicStrings.ColorpickerGreen;
    }

    public void ChangeColorYellow()
    {
        CurrentColor = Color.yellow;
		NGUITools.SetActive (ColorPicker,false);
        _colorpicker.spriteName = MagicStrings.ColorpickerYellow;
    }

    public void ChangeColorBlue()
    {
        CurrentColor = Color.blue;
		NGUITools.SetActive (ColorPicker,false);
        _colorpicker.spriteName = MagicStrings.ColorpickerBlue;
    }

    public void ChangeColorBlack()
    {
        CurrentColor = Color.black;
		NGUITools.SetActive (ColorPicker,false);
        _colorpicker.spriteName = MagicStrings.ColorpickerBlack;
    }

    public Color GetCurrentColor()
    {
        return CurrentColor;
    }

    public Color DefaultColor()
    {
        return GameObject.Find("PersistentController").GetComponent<DrawingDefautStore>().DefaultColor;
    }

	public void ActivateColorPick()
	{
		NGUITools.SetActive (ColorPicker,true);
	    if (_colorpicker.spriteName == MagicStrings.ColorpickerRed)
	    {
            _colorpicker.spriteName = MagicStrings.ColorpickerRedHit;
	    }
        if (_colorpicker.spriteName == MagicStrings.ColorpickerGreen)
        {
            _colorpicker.spriteName = MagicStrings.ColorpickerGreenHit;
        }
        if (_colorpicker.spriteName == MagicStrings.ColorpickerBlue)
        {
            _colorpicker.spriteName = MagicStrings.ColorpickerBlueHit;
        }
        if (_colorpicker.spriteName == MagicStrings.ColorpickerBlack)
        {
            _colorpicker.spriteName = MagicStrings.ColorpickerBlackHit;
        }
        if (_colorpicker.spriteName == MagicStrings.ColorpickerYellow)
        {
            _colorpicker.spriteName = MagicStrings.ColorpickerYellowHit;
        }
	}
}
