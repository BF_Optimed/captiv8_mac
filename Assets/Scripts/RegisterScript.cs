﻿using Assets.DBHelper;
using System;
using System.Globalization;
using UnityEngine;
using System.Collections.Generic;
using Assets.OptimedClient;
public class RegisterScript : MonoBehaviour
{
    //private UILabel DeviceLbl;
    public static GameObject Username;
    public GameObject BadDetails;
    public GameObject Description;
    public UIInput DescriptionLbl;
    public GameObject Device;
    public GameObject Password;
    public UIInput PasswordLbl;
    public GameObject Register;
    public GameObject SuccessNotif;
    public UIInput UsernameLbl;
    private void Start()
    {
        
        if (CheckLicenseExits())
        {
            var encption = new BFSEncryption();
            string ExpirationDate;
            string LastDateCheck;
            encption.TryDecrypt( Runtime.Default.SystemInterrupts,SystemInfo.deviceUniqueIdentifier,out ExpirationDate);
            encption.TryDecrypt(Runtime.Default.RunManager, SystemInfo.deviceUniqueIdentifier, out LastDateCheck);
            if (!CheckLicenseExpire(ExpirationDate, LastDateCheck))
            {
                //BFSSecurePlayerData.SetString(LastCheckDate, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),SystemInfo.deviceUniqueIdentifier);
                Runtime.Default.RunManager = encption.Encrypt(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), SystemInfo.deviceUniqueIdentifier);
                Runtime.Default.Save();
                Debug.Log("License is valid or not experied");
                LoadMainScene();
            }
            else
            {
                Debug.Log("License is invalid or expired");
                gameObject.GetComponent<CreateNotification>().CreateOkayButton("Licence expired.", "Your licence has expired. Please renew your licence and then log in again.", "Ok");
            }
        }
        else
        {
            Debug.Log("NO License available ");
        }
    }
    //4fqhl1qaes85 
    public void Submit()
    {
        UsernameLbl = Register.GetComponent<UIInput>();
        PasswordLbl = Password.GetComponent<UIInput>();
        DescriptionLbl = Description.GetComponent<UIInput>();

        try
        {
            Camera.main.GetComponent<BFSNRegistrationLicense>().OptimiedRegistration(UsernameLbl.value, PasswordLbl.value, DescriptionLbl.value, 1, Notification);
               
        }
        catch(ArgumentNullException e)
        {
            Debug.Log(e.Message);
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "All fields are required.", "Ok");
        }
       
    }
    public void Notification(bool success, string title, string message)
    { 
        if(success)
        {
            Debug.Log("Registration Success");
            NGUITools.SetActive(SuccessNotif, true);
        }
        else
        {
            NGUITools.SetActive(BadDetails, true);
            GameObject.Find("LabelToChange").GetComponent<UILabel>().text = message;
        }
    }
    public void TryAgain()
    {
        NGUITools.SetActive(BadDetails, false);
    }

    public void LoadMainScene()
    {
        Application.LoadLevel("Overview");
    }

    private static bool CheckLicenseExpire(string strExpireDate, string strLastCheckDate)
    {
        try
        {
            var expireDate = DateTime.ParseExact(strExpireDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var lastCheckDate = DateTime.ParseExact(strLastCheckDate, "dd/MM/yyyy HH:mm:ss",
                CultureInfo.InvariantCulture);
            Debug.Log(expireDate > DateTime.Now &&
                      ((expireDate - lastCheckDate) < TimeSpan.Zero
                          ? -(expireDate - lastCheckDate)
                          : expireDate - lastCheckDate) >
                      ((expireDate - DateTime.Now) < TimeSpan.Zero
                          ? -(expireDate - DateTime.Now)
                          : (expireDate - DateTime.Now)));
            if (expireDate > DateTime.Now &&
                ((expireDate - lastCheckDate) < TimeSpan.Zero
                    ? -(expireDate - lastCheckDate)
                    : expireDate - lastCheckDate) >
                ((expireDate - DateTime.Now) < TimeSpan.Zero
                    ? -(expireDate - DateTime.Now)
                    : (expireDate - DateTime.Now)))
                return false;
        }
        catch (FormatException e)
        {
            Debug.Log(e.Message);
            //throw;
        }
        return true;
    }
    public void testUnregister()
    {
       StartCoroutine(Camera.main.GetComponent<ReleaseInstallationId>().ReleaseId(Notification));
    }
    private static bool CheckLicenseExits()
    {
        try
        {
            var temp=InstallationId.GetInstallationId;
        }
        catch(Exception ex)
        {
            Debug.Log(ex.Message);
            return false;
        }
        
        return true;
    }
}