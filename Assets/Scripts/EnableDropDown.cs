﻿using UnityEngine;

public class EnableDropDown : MonoBehaviour
{
    private bool _isEnabled;
    public GameObject Menu;

    private void OnClick()
    {
        if (_isEnabled)
        {
            CloseMenu();
        }
        else
        {
            OpenMenu();
        }

        _isEnabled = !_isEnabled;
    }

    private void OpenMenu()
    {
        Menu.SetActive(true);
        NGUITools.SetActiveChildren(Menu, true);
        Menu.GetComponentInChildren<GrowListTween>().Open();
    }

    private void CloseMenu()
    {
        Menu.GetComponentInChildren<GrowListTween>().Close();
    }
}