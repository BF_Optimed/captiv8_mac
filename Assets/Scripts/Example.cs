﻿using AudioRecorder;
using UnityEngine;

public class Example : MonoBehaviour
{
    private AudioSource audioSource;
    public bool autoPlay;
    private Recorder recorder;

    private void OnEnable()
    {
        recorder = new Recorder();
        recorder.onInit += OnInit;
        recorder.onFinish += OnRecordingFinish;

        SavWav.onSaved += OnRecordingSaved;
    }

    private void OnDisable()
    {
        recorder.onInit -= OnInit;
        recorder.onFinish -= OnRecordingFinish;

        SavWav.onSaved -= OnRecordingSaved;
    }

    //Use this for initialization  
    private void Start()
    {
        audioSource = FindObjectOfType<AudioSource>();
        recorder.Init();
    }

    private void OnGUI()
    {
        if (recorder.IsReady)
        {
            if (!recorder.IsRecording)
            {
                if (GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 - 100, 300, 60), "Start"))
                {
                    recorder.StartRecording(false, 60);
                }
            }
            else
            {
                if (GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 - 100, 300, 60), "Stop"))
                {
                    recorder.StopRecording();
                }

                GUI.Label(new Rect(Screen.width/2 - 150, 50, 300, 60), "Recording...");
            }

            if (recorder.hasRecorded)
            {
                if (GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 - 30, 300, 60), "Play"))
                {
                    Debug.Log(audioSource.timeSamples);
                    Debug.Log(audioSource.clip.length);
                    recorder.PlayRecorded(audioSource);
                }

                if (GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 + 80, 300, 60), "Play From 4 Second"))
                {
                    //recorder.PlayRecorded(audioSource);
                    audioSource.Play();
                }

                if (GUI.Button(new Rect(Screen.width/2 - 150, Screen.height/2 + 40, 300, 60), "Save"))
                {
                    SavWav.Save("", recorder.Clip);
                }
            }
        }
    }

    private void OnInit(bool success, string error)
    {
        Debug.Log("Success : " + success);
        Debug.Log("Error : " + error);
    }

    private void OnRecordingFinish(AudioClip clip)
    {
        if (autoPlay)
        {
            audioSource.clip = clip;
            audioSource.Play();

            // or you can use
            //recorder.PlayRecorded(audioSource);
        }
    }

    private void OnRecordingSaved(bool succ, string path)
    {
        if (succ)
        {
            Debug.Log("File Saved at : " + path);
        }
    }
}