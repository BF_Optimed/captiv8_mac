﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenDrawing : MonoBehaviour
{
    private readonly List<Vector3> thePath = new List<Vector3>();
    public float AllocateWidth;
    public Color colorChosen;
    private string dtxt;
    public getColor enableColorScript;
    public GameObject GuiControllerObject;
    private RaycastHit hit;
    public ArrayList LineArray;
    public GameObject LineObject;
    public LineRenderer lineRender;
    public bool mouseIsDown;
    public int numberOfPoints = 0;
    private Ray ray;
    public float widthSet = 0.0003f;

    private void Start()
    {
        GuiControllerObject = GameObject.Find("GUIController");
        enableColorScript = GuiControllerObject.GetComponent<getColor>();
        colorChosen = enableColorScript.GetColorPicked();
    }

    private void Update()
    {
        /*

 If colorPicker functionality is enabled chose the color anyways 
*/
        colorChosen = enableColorScript.GetColorPicked();
        dtxt = "";


        //Draw a ray from camera on the screen to get the point to draw and check if there is a collider is attached
        ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (mouseIsDown)
            {
                thePath.Add(hit.point);
                lineRender.SetVertexCount(thePath.Count);
                lineRender.SetPosition(thePath.Count - 1, hit.point);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            mouseIsDown = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            LineObject = new GameObject();
            lineRender = LineObject.AddComponent("LineRenderer") as LineRenderer;
            //lineRender.material= new Material(Shader.Find("lineRendererParticles"));
            lineRender.material = new Material(Shader.Find("textShader"));

            lineRender.SetVertexCount(0);
            AllocateWidth = GetLineWidth();
            lineRender.SetWidth(AllocateWidth, AllocateWidth);
            //lineRender.useWorldSpace = true;	 
            //	lineRender.SetColors(Color.green, Color.green);
            if (colorChosen == Color.clear)
            {
                lineRender.SetColors(Color.green, Color.green);
            }
            else
            {
                lineRender.SetColors(colorChosen, colorChosen);
            }
            thePath.Clear();
            LineArray.Add(LineObject);

            mouseIsDown = false;
        }

        //transform.parent.gameObject.transform.eulerAngles.y += 0.2;
    }

    private void OnDisable()
    {
        foreach (GameObject obj in LineArray)
        {
            Destroy(obj);
        }
        if (enableColorScript != null)
        {
            enableColorScript.enabled = false;
        }
        mouseIsDown = false;
    }

    private void OnEnable()
    {
        LineArray = new ArrayList();
        LineObject = new GameObject();

        lineRender = LineObject.AddComponent("LineRenderer") as LineRenderer;
        //lineRender.material= new Material(Shader.Find("lineRendererParticles"));
        lineRender.material = new Material(Shader.Find("textShader"));

        lineRender.SetVertexCount(0);
        AllocateWidth = GetLineWidth();
        lineRender.SetWidth(AllocateWidth, AllocateWidth);
        if (colorChosen == Color.clear)
        {
            lineRender.SetColors(Color.green, Color.green);
        }
        else
        {
            lineRender.SetColors(colorChosen, colorChosen);
        }


        //lineRender.SetColors(Color.green, Color.green);
        thePath.Clear();
        LineArray.Add(LineObject);

        //Enable the color Picker Here

        GuiControllerObject = GameObject.Find("GUIController");
        enableColorScript = GuiControllerObject.GetComponent<getColor>();
        enableColorScript.enabled = true;
        mouseIsDown = false;
    }

    public float GetLineWidth()
    {
        return widthSet*Camera.main.fieldOfView;
    }
}