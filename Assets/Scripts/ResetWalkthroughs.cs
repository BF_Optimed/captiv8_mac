﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class ResetWalkthroughs : MonoBehaviour
{

    public GameObject controller;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnClick()
    {
        PlayerPrefs.SetFloat(MagicStrings.ModelsAndInteractionsKey,0);
        PlayerPrefs.SetFloat(MagicStrings.RecordKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.CutKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.ThumbnailKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.SubtitlesKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.VoiceoverKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.DrawKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.ControllingTheModelKey, 0);
        PlayerPrefs.SetFloat(MagicStrings.ShareKey, 0);
        controller.GetComponent<CreateNotification>().CreateOkayButton("Walkthoughs Reset","All walkthroughs have been reset.","Ok");
    }
}
