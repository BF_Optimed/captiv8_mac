﻿using UnityEngine;

public class BFSUAutopilot : MonoBehaviour
{
    private GameObject _blackout;
    private BtnController _btnController;
    private Transform _cam;
    private Transform _controller;
    private BFSMRotation _movementScript;
    private Transform _parentObject;
    private Transform _pivot;
    private bool _selected;
    public float AutoZoomSpeed = 2;
    public int AxisRotationAmount;
    public float FinalZoom = 10;
    public string MovementType;
    public GameObject NextObj;
    public bool Outside = true;
    public float CurrentZoom { get; set; }
    public string NextObjName { get; set; }

    private void OnEnable()
    {
        _cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        _controller = GameObject.FindGameObjectWithTag("GeoController").transform;
        _pivot = GameObject.FindGameObjectWithTag("Pivot").transform;
        _parentObject = GameObject.FindGameObjectWithTag("Main").transform;
        _btnController = GameObject.Find("Controller").GetComponent<BtnController>();
        _movementScript = Camera.main.GetComponent<BFSMRotation>();
        _blackout = GameObject.FindGameObjectWithTag("Blackout");
    }

    private void Update()
    {
        if (_selected)
        {
            switch (MovementType)
            {
                case "Camera":
                    CameraMovement();
                    break;
                case "Object":
                    ObjectMovement();
                    break;
                case "Pin":
                    PinMovement();
                    break;
            }
        }
    }

    public void MoveTo()
    {
        StartAutopiloting();
    }

    private void StartAutopiloting()
    {
        _btnController.Reset();
        if (MovementType == "Object")
        {
            if (Outside)
            {
                _pivot.transform.LookAt(gameObject.transform);
            }
            else
            {
                _pivot.transform.LookAt(2 * _pivot.transform.position - transform.position);
            }

            _parentObject.transform.parent = _pivot.transform;
            CurrentZoom = Camera.main.fieldOfView;
        }

        
        _selected = true;
        //_btnController.DisableAllFunctions();
        _btnController.RotationScript.enabled = true;
        _btnController.RotationScript.MovementAllowed = false;
    }

    private void PinMovement()
    {
        ChangeObject();
    }

    private void ObjectMovement()
    {
        LerpPivot();

        //RotateAxisAmount();

        LerpTargetOffset();

        LerpFov();

        if (GetPivotAngle() <= 1 && GetZoomAmount() <= 0.01)
        {
            ChangeObject();
        }
    }

    private float GetPivotAngle()
    {
        return Quaternion.Angle(_pivot.transform.rotation,
            Quaternion.LookRotation(_cam.transform.position - _pivot.transform.position));
    }

    private float GetZoomAmount()
    {
        if(FinalZoom >= CurrentZoom)
        {
            return FinalZoom - CurrentZoom;
        }
        else
        {
            return CurrentZoom - FinalZoom;
        }
    }

    private void LerpPivot()
    {
        _pivot.transform.rotation = Quaternion.Lerp(_pivot.transform.rotation,
            Quaternion.LookRotation(_cam.transform.position - _pivot.transform.position), Time.deltaTime);
    }

    private void LerpTargetOffset()
    {
        _movementScript._panScript.DesiredOffset = Vector3.zero;

        _movementScript.TargetOffset = Vector3.Lerp(_movementScript.TargetOffset,
            new Vector3(0, 0, _movementScript.TargetOffset.z), Time.deltaTime * 2);
        
    }

    private void RotateAxisAmount()
    {
        if (_parentObject.transform.rotation.eulerAngles.z < AxisRotationAmount)
        {
            _parentObject.transform.Rotate(0, 0, 2);
        }
    }

    private void CameraMovement()
    {
        LerpCameraToObject();

        if (GetCameraAngle() <= 1)
        {
            ChangeObject();
        }
    }

    private float GetCameraAngle()
    {
        return Quaternion.Angle(_cam.rotation, Quaternion.LookRotation(gameObject.transform.position - _cam.position));
    }

    private void LerpFov()
    {
        _cam.GetComponent<Camera>().fieldOfView = Mathf.Lerp(CurrentZoom, FinalZoom, Time.deltaTime * AutoZoomSpeed);
        CurrentZoom = _cam.GetComponent<Camera>().fieldOfView;
    }

    private void LerpCameraToObject()
    {
        _cam.rotation = Quaternion.Lerp(_cam.rotation,
            Quaternion.LookRotation(gameObject.transform.position - _cam.position), Time.deltaTime*2);
    }

    private void ChangeObject()
    {
        _selected = false;
        if (NextObj != null)
        {
            ContinueChain();
        }
        else
        {
            EndChain();
        }
    }

    private void EndChain()
    {
        _blackout.GetComponent<BFSUTransition>().Begin(gameObject.name, true);
        _parentObject.transform.parent = _controller.transform;
    }

    private void ContinueChain()
    {
        NextObj.GetComponent<BFSUAutopilot>().MoveTo();
    }
}