/// REMEMBER: When importing assets for use with this script, be sure to set the normal angle to 180 degrees. 
/// When importing a mesh Unity automatically splits vertices based on normal creases. 
/// This script requires that all meshes have the same number of vertices and that 
/// those vertices are laid out in exactly the same way. It won't work if Unity autosplits vertices based on normals.

//Custom definitions to store specialized blend shape data for vertices
class BlendShapeVertex {
	var originalIndex : int;
	var position : Vector3;
	var normal : Vector3;
}

class BlendShape {
	//var vertices : BlendShapeVertex[];
	var vertices = new Array();
}

var attributes : String[]; //Names for the attributes to be morphed
var sourceMesh : Mesh; //The original mesh
var attributeMeshes : Mesh[]; //The destination meshes for each attribute.
var attributeProgress : float[]; //Current weight for each attribute.

private var blendShapes : BlendShape[]; //Array of BlendShape objects. This will be populated in Awake().
private var workingMesh : Mesh; //Stores mesh data for the working copy of the mesh (so the original mesh doesn't get changed).

function Awake () {
	//First, make sure all attributes are assigned. 
	for (i=0; i<attributeMeshes.length; i++) { 
		if (attributeMeshes[i] == null) {    
			Debug.Log("Attribute " + i + " has not been assigned."); 
			return; 
		} 
   } 
	
	//Populate the working mesh
	var filter : MeshFilter = GetComponent(MeshFilter); 
	filter.sharedMesh = sourceMesh; 
	workingMesh = filter.mesh; 	

	//Check attribute meshes to be sure vertex count is the same.
	var vertexCount = sourceMesh.vertexCount; 
	for (i=0; i<attributeMeshes.Length; i++) { 
		if (attributeMeshes[i].vertexCount != vertexCount) {    
			Debug.Log("Mesh " + i + " doesn't have the same number of vertices as the first mesh"); 
			return; 
		} 
	} 
	
	//Build blend shapes
	BuildBlendShapes();
}

//This function populates the various arrays that are used by the script later on.
function BuildBlendShapes () {

	blendShapes = new BlendShape[attributes.length];
	
	//For each attribute figure out which vertices are affected, then store their info in the blend shape object.
	for (var i=0; i < attributes.length; i++) {
		//Populate blendShapes array with new blend shapes
		blendShapes[i] = new BlendShape();
		
		for (var j=0; j < workingMesh.vertexCount; j++) {
			//If the vertex is affected, populate a blend shape vertex with that info
			if (workingMesh.vertices[j] != attributeMeshes[i].vertices[j]) {
				//Create a blend shape vertex and populate its data.
				var blendShapeVertex = new BlendShapeVertex();
				blendShapeVertex.originalIndex = j;
				blendShapeVertex.position = attributeMeshes[i].vertices[j] - workingMesh.vertices[j];
				blendShapeVertex.normal = attributeMeshes[i].normals[j] - workingMesh.normals[j];
								
				//Add new blend shape vertex to blendShape object.
				blendShapes[i].vertices.Push(blendShapeVertex);
			}
		}
		
		//Convert blendShapes.vertices to builtin array
		blendShapes[i].vertices = blendShapes[i].vertices.ToBuiltin(BlendShapeVertex);
	}
}

//This is the primary function that controls morphs. It is called from the GUI script every time one of the sliders is updated.
function SetMorph () {

	//Set up working data to store mesh offset information.
	var morphedVertices : Vector3[] = sourceMesh.vertices;
	var morphedNormals : Vector3[] = sourceMesh.normals;
	
	//For each attribute...
	for (var j=0; j<attributes.length; j++) {
		//If the weight of this attribute isn't 0	
		if (!Mathf.Approximately(attributeProgress[j], 0)) {
			//For each vertex in this attribute's blend shape...
			for (var i=0; i<blendShapes[j].vertices.length; i++) {		
				//...adjust the mesh according to the offset value and weight
				morphedVertices[blendShapes[j].vertices[i].originalIndex] += blendShapes[j].vertices[i].position * attributeProgress[j];
				//Adjust normals as well
				morphedNormals[blendShapes[j].vertices[i].originalIndex] += blendShapes[j].vertices[i].normal * attributeProgress[j];
			}	
		}	
	}
	
	//Update the actual mesh with new vertex and normal information, then recalculate the mesh bounds.		
	workingMesh.vertices = morphedVertices;
	workingMesh.normals = morphedNormals;
	workingMesh.RecalculateBounds();
}

//Require that we have a mesh filter component and a mesh renderer component when assigning this script to an object.
@script RequireComponent (MeshFilter)
@script RequireComponent (MeshRenderer)