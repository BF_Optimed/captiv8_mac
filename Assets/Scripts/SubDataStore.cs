﻿using UnityEngine;

public class SubDataStore : MonoBehaviour
{
    public string Content;
    public string ETime;
    public string STime;

    public void UpdateData(string s, string e, string c)
    {
        STime = s;
        ETime = e;
        Content = c;
    }
}