﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.DBHelper;
using UnityEngine;
using Assets.Scripts;

public class VideoEditController : MonoBehaviour
{

    private VideoPlayController _videoPlayController;
    private CreateNotification _createNotif;
    private readonly GameObject _sliderRoot;
    private UISprite _audioBtnSprite;
    private UILabel _audioLabel;
    private UISprite _cutBtnSprite;
    private UILabel _cutLabel;
    private bool _cuttingFramesOpen;
    private string _fnameStored = "";
    private UISprite _playBtn;
    private bool _playSpriteActive;
    private BFSERecordAudio _recAud;
    public bool _recordingAud;
    private RecordScript _recScr;
    private UISlider _slider1;
    private UISlider _slider2;
    private UILabel _label2;
    private UILabel _label1;
    private bool _subsActive;
    private UISprite _subsBtnSprite;
    private SubtitleCreator _subScript;
    private UILabel _subsLabel;
    private UISprite _thumbBtnSprite;
    private UILabel _thumbLabel;
    private UISprite _audioStartSprite;
    private UISprite _editBtnSprite;
    private UILabel _audioStartLabel;
    private bool _thumbnailOpen;
    private TweenScale _twAudioBtn;
    private TweenScale _twCutBtn;
    private TweenScale _twSubsBtn;
    private TweenScale _twThumbBtn;
    private bool _voiceOverOpen;
    private bool Active = true;
    private ContinuousVars _continuous;
    private float one;
    private float two;
    public GameObject GoAudNotif;
    public GameObject GoSlider1;
    public GameObject GoSlider2;
    public GameObject GoSubtitles;
    public GameObject LbAudioBtn;
    public GameObject LbCutBtn;
    public GameObject LbSubsBtn;
    public GameObject LbThumbBtn;
    public GameObject LbStartAudioBtn;
    public GameObject SpAudioBtn;
    public GameObject SpCutBtn;
    public GameObject SpSubsBtn;
    public GameObject SpThumbBtn;
    public GameObject SpEditBtn;
    public GameObject SpStartAudioBtn;
    public GameObject SubBox;
    public GameObject Controller;
    public GameObject Label1;
    public GameObject Label2;
    public GameObject TextureGameObject;

    public GameObject AudioTweenGameObject;
    public GameObject SubtitlesTweeGameObject;
    public GameObject ThumbnailTweenGameObject;
    public GameObject CutTweenGameObject;
    public GameObject EditTweenGameObject;

    private UIPlayTween _audioPlayTween;
    private UIPlayTween _subtitlesPlayTween;
    private UIPlayTween _thumbPlayTween;
    private UIPlayTween _cutPlayTween;
    private UIPlayTween _editDetailsTween;

    public GameObject TitleInput;
    public GameObject DescInput;
    private UIInput _titleInput;
    private UIInput _descInput;

    private bool _editDetailsOpen;

    public GameObject CategoryLabel;
    private UILabel _categoryLabel;

    // Use this for initialization
    void Start()
    {
        _categoryLabel = CategoryLabel.GetComponent<UILabel>();
        _recAud = Camera.main.GetComponent<BFSERecordAudio>();
        _videoPlayController = Controller.GetComponent<VideoPlayController>();

        _slider1 = GoSlider1.GetComponent<UISlider>();
        _slider2 = GoSlider2.GetComponent<UISlider>();

        _label1 = Label1.GetComponent<UILabel>();
        _label2 = Label2.GetComponent<UILabel>();

        _audioBtnSprite = SpAudioBtn.GetComponent<UISprite>();
        _thumbBtnSprite = SpThumbBtn.GetComponent<UISprite>();
        _subsBtnSprite = SpSubsBtn.GetComponent<UISprite>();
        _cutBtnSprite = SpCutBtn.GetComponent<UISprite>();
        _audioStartSprite = SpStartAudioBtn.GetComponent<UISprite>();
        _editBtnSprite = SpEditBtn.GetComponent<UISprite>();

        _audioLabel = LbAudioBtn.GetComponent<UILabel>();
        _thumbLabel = LbThumbBtn.GetComponent<UILabel>();
        _subsLabel = LbSubsBtn.GetComponent<UILabel>();
        _cutLabel = LbCutBtn.GetComponent<UILabel>();
        _audioStartLabel = LbStartAudioBtn.GetComponent<UILabel>();


        _subScript = GoSubtitles.GetComponent<SubtitleCreator>();
        _createNotif = gameObject.GetComponent<CreateNotification>();

        _continuous = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();

        _audioPlayTween = AudioTweenGameObject.GetComponent<UIPlayTween>();
        _subtitlesPlayTween = SubtitlesTweeGameObject.GetComponent<UIPlayTween>();
        _thumbPlayTween = ThumbnailTweenGameObject.GetComponent<UIPlayTween>();
        _cutPlayTween = CutTweenGameObject.GetComponent<UIPlayTween>();
        _cutPlayTween = CutTweenGameObject.GetComponent<UIPlayTween>();
        _editDetailsTween = EditTweenGameObject.GetComponent<UIPlayTween>();

        _titleInput = TitleInput.GetComponent<UIInput>();
        _descInput = DescInput.GetComponent<UIInput>();

        LoadDetails();

        //_videoPlayController.PlayLoadedVideo();
       // _videoPlayController.PauseLoadedVideo();
    }

  

    //Subtitle Click Event
    public void SubtitlesClick()
    {
        if (_subsActive)
        {
            DisableSubtitlesGui();
        }
        else
        {
            DisableAllGui();
            EnableSubtitlesGui();
        }
        _subsActive = !_subsActive;
    }

    public void OpenEditDetails()
    {
        if (_editDetailsOpen)
        {
            DisableEditDetails();
        }
        else
        {
            DisableAllGui();
            EnableEditDetails();
        }
        _editDetailsOpen = !_editDetailsOpen;
    }

    private void EnableEditDetails()
    {
        _editBtnSprite.spriteName = MagicStrings.EditDetailsBtnSpriteHit;
       // _subsLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    private void DisableEditDetails()
    {
        _editBtnSprite.spriteName = MagicStrings.EditDetailsBtnSprite;
        //_subsLabel.color = Color.black;
    }

    private void DisableAllGui()
    {
        if (_cuttingFramesOpen)
        {
            OpenCutFrames();
            _cutPlayTween.Play(false);
        }

        if (_voiceOverOpen)
        {
            VoiceOverButtons();
            _audioPlayTween.Play(false);
        }

        if (_subsActive)
        {
            SubtitlesClick();
            _subtitlesPlayTween.Play(false);
        }

        if (_thumbnailOpen)
        {
            CreateThumbnailOpen();
            _thumbPlayTween.Play(false);
        }
        if(_editDetailsOpen)
        {
            OpenEditDetails();
            _editDetailsTween.Play(false);
        }
    }

    private void EnableSubtitlesGui()
    {
        NGUITools.SetActive(GoSlider2, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider2);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label2);
        _slider2.value = 0;

        NGUITools.SetActive(GoSlider1, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label1);
        _slider1.value = 0.1f;
        
        

        _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSpriteHit;
        _subsLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);

        _subScript.LoadSubFile(_continuous.LoadedVideo.VttPath);
        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.SubtitlesTitle, MagicStrings.SubtitlesContent, MagicStrings.SubtitlesKey, MagicStrings.SubtitleIcon);
    }

    private void DisableSubtitlesGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
        _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSprite;
        _subsLabel.color = Color.black;
    }

    public void VoiceOverButtons()
    {
        if (_voiceOverOpen)
        {
            DisableVoiceOverGui();
        }
        else
        {
            DisableAllGui();
            EnableVoiceOverGui();
        }
        _voiceOverOpen = !_voiceOverOpen;
    }

    private void EnableVoiceOverGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label1);
        NGUITools.SetActive(GoSlider2, false);
        NGUITools.SetActive(GoAudNotif, true);
        _audioBtnSprite.spriteName = MagicStrings.AudioBtnSpriteHit;
        _audioLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);

        _slider2.value = 0;
        _slider1.value = 0;
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.VoiceoverTitle, MagicStrings.VoiceoverContent, MagicStrings.VoiceoverKey, MagicStrings.VoiceoverIcon);
    }

    private void DisableVoiceOverGui()
    {
        NGUITools.SetActive(GoAudNotif, false);
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
        _audioBtnSprite.spriteName = MagicStrings.AudioBtnSprite;
        _audioLabel.color = Color.black;
    }

    public void VoiceOverClick()
    {
        if (_recordingAud)
        {
            _slider1.value = 0;
            Camera.main.GetComponent<BFSERecordAudio>().StopRecording();
            _recordingAud = !_recordingAud;
            _audioStartSprite.spriteName = "green (2)";
            _audioStartLabel.text = "Start Audio Recording";
        }
        else
        {
            _slider1.value = 0;
            Debug.Log(_createNotif);
            _createNotif.CreateOneButton("Voice Over?", "Are you sure you wish to start a voice over it will overide the previous one you have done.", "Yes", "No", new EventDelegate(VoiceOverCheck));
        }
    }

    private void VoiceOverCheck()
    {
        _slider1.value = 0;
        _audioStartSprite.spriteName = "red (1)";
        _audioStartLabel.text = "Stop Audio Recording";
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        Camera.main.GetComponent<BFSERecordAudio>().StartRecording(BFSRSwitchController.Instance.VideoLength());
        _videoPlayController.PlayLoadedVideo();
        _recordingAud = !_recordingAud;
    }

    public void OpenCutFrames()
    {
        if (_cuttingFramesOpen)
        {
            
            DisableCutFramesGui();
        }
        else
        {
            DisableAllGui();
            EnableCutFramesGui();
        }
        _cuttingFramesOpen = !_cuttingFramesOpen;
    }

    private void EnableCutFramesGui()
    {
        NGUITools.SetActive(GoSlider2, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider2);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label2);
        _slider2.value = 0;

        NGUITools.SetActive(GoSlider1, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label1);
        _slider1.value = 1;
        
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSpriteHit;
        _cutLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);

        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.CutTitle, MagicStrings.CutContent, MagicStrings.CutKey, MagicStrings.CutIcon);
    }

    private void DisableCutFramesGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSprite;
        _cutLabel.color = Color.black;
    }

    public void CutFrames()
    {
        _createNotif.CreateOneButton("Delete Frames", "If you delete these frames it will also delete ALL subtitles and audio associated with this movie.", "Yes", "No", new EventDelegate(CutFrameCheck)); 
    }

    public void CutFrameCheck()
    {
        //var frames = BFSRSwitchController.NumberOfFrames2Play();
        //BFSRSwitchController.Instance.RemoveFrame((int)Math.Round(one * frames), (int)Math.Round(two * frames));
        _createNotif.CreateLoadingNotification("Cutting Frames...");
        BFSRSwitchController.Instance.RemoveFrame(_label2.text, _label1.text, AfterCut);
    }

    public void AfterCut()
    {
        _slider2.value = 0;
        _slider1.value = 1;

        //label1. text = UpdateLabels(_slider1);
        _label1.text = BFSRSwitchController.Instance.LabelUpdate(_slider1.value);
        _label2.text = BFSRSwitchController.Instance.LabelUpdate(_slider2.value);


        DeleteAudioAndVtt();
        Destroy(GameObject.FindGameObjectWithTag("LoadingNotif"));
    }

    private void DeleteAudioAndVtt()
    {
        if (File.Exists(_continuous.LoadedVideo.WavPath))
        {
            File.Delete(_continuous.LoadedVideo.WavPath);
            _continuous.LoadedVideo.UpdateVideo(AnimationModel.AnimationSound, "");
        }

        if (File.Exists(_continuous.LoadedVideo.VttPath))
        {
            File.Delete(_continuous.LoadedVideo.VttPath);
            _continuous.LoadedVideo.UpdateVideo(AnimationModel.AnimationCaptions, "");
        }
    }

    public void CreateThumbnailOpen()
    {
        if (_thumbnailOpen)
        {
            DisableThumbGui();
        }
        else
        {
            DisableAllGui();
            EnableThumbGui();
        }
        _thumbnailOpen = !_thumbnailOpen;
    }

    private void EnableThumbGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(_label1);
        _slider1.value = 0;

        NGUITools.SetActive(GoSlider2, false);
        _slider2.value = 0;
        _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSpriteHit;
        _thumbLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);

        _slider2.value = 0;
        _slider1.value = 0.0f;

        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.ThumbnailTitle, MagicStrings.ThumbnailContent, MagicStrings.ThumbnailKey, MagicStrings.ThumbnailIcon);
    }

    private void DisableThumbGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
        _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSprite;
        _thumbLabel.color = Color.black;
    }

    public void CaptureThumbnail()
    {
        _createNotif.CreateOneButton("Change Thumbnail?", "Do you wish to overide your previous thumbnail.", "Yes", "No", new EventDelegate(CaptureThumbnailCheck));
    }

    private void CaptureThumbnailCheck()
    {
        var savePath =
        Path.ChangeExtension(_continuous.LoadedVideo.XmlPath, "png");
       BFSRSwitchController.Instance.CaptureImage(savePath, Camera.main, Screen.width,
            Screen.height,ApplyTextureAction);

       DisableAllGui();

    }
    public void ApplyTextureAction()
    {
        Debug.Log("Created thumbnail!");
        TextureGameObject.GetComponent<ThumbTextureLoad>().LoadNewTex();
        _continuous.LoadedVideo.UpdateVideo(AnimationModel.AnimationImage, Path.ChangeExtension(_continuous.LoadedVideo.XmlPath, "png"));
        _continuous.LoadedVideo.AnimationId = "";
        _continuous.LoadedVideo.Public = false;
        _continuous.LoadedVideo.Private = false;
        _continuous.LoadedVideo.UpdateVideo();
    }
    public void AudioBtnHover()
    {
        _audioBtnSprite.spriteName = MagicStrings.AudioBtnSpriteHit;
        _audioLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void AudioBtnHoverOut()
    {
        if (!_voiceOverOpen)
        {
            _audioBtnSprite.spriteName = MagicStrings.AudioBtnSprite;
            _audioLabel.color = Color.black;
        }
    }

    public void SubsBtnHover()
    {
        _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSpriteHit;
        _subsLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void SubsBtnHoverOut()
    {
        if (!_subsActive)
        {
            _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSprite;
            _subsLabel.color = Color.black;
        }
    }

    public void ThumbBtnHover()
    {
        _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSpriteHit;
        _thumbLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void ThumbBtnHoverOut()
    {
        if (!_thumbnailOpen)
        {
            _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSprite;
            _thumbLabel.color = Color.black;
        }
    }

    public void CutBtnHover()
    {
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSpriteHit;
        _cutLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void CutBtnHoverOut()
    {
        if (_cuttingFramesOpen) return;
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSprite;
        _cutLabel.color = Color.black;
    }

    public void LoadDetails()
    {
        _titleInput.value = _continuous.LoadedVideo.Name;
        _descInput.value = _continuous.LoadedVideo.Description;
    }

    public void ConfirmSaveDetails()
    {
        _createNotif.CreateOneButton("Edit Details", "Are you sure you wish to change these details? It will overwrite the previous data.", "Okay", "Cancel", new EventDelegate(SaveDetails));
    }

    public void SaveDetails()
    {
        Debug.Log("SAVING THE NEW DETAILS!");
        if(!string.IsNullOrEmpty(_titleInput.value))
        {
            _continuous.LoadedVideo.Name = _titleInput.value.Trim();
        }
        if (!string.IsNullOrEmpty(_descInput.value))
        {
            _continuous.LoadedVideo.Description = _descInput.value.Trim();
        }
        GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo.Category = _categoryLabel.text;
        _continuous.LoadedVideo.UpdateVideo();
    }


}