﻿using System.Collections;
using UnityEngine;

public class TransformGet : MonoBehaviour
{
    public ArrayList myArrayList = new ArrayList();
    public Transform[] transform_list;
    // Use this for initialization
    private void Start()
    {
        GetTransformList();
    }

    //Getting the trasform list here ... setting the public variable  transform_list so I can accesss it from other class 
    public void GetTransformList()
    {
        transform_list = GetComponentsInChildren<Transform>();
        print(transform_list.Length);
    }

    public void AddTag(string newtag)
    {
        myArrayList.Add(newtag);
    }

    public string getTag(int ind)
    {
        return (string) myArrayList[ind];
    }

    public int getCountOfArrayList()
    {
        return myArrayList.Count;
    }
}