﻿using UnityEngine;

public class ButtonResizer : MonoBehaviour
{
    private UIPanel _panel;
    private UISprite _sprite;

    private void Start()
    {
        _panel = gameObject.transform.parent.transform.parent.GetComponent<UIPanel>();
        _sprite = gameObject.GetComponent<UISprite>();
    }

    private void Update()
    {
        _sprite.width = (int) _panel.width;
    }
}