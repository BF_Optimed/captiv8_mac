﻿using UnityEngine;

public class BFSDLineWidth : MonoBehaviour
{
    public float lineWidth;
    // Use this for initialization
    private void Start()
    {
        lineWidth = GameObject.Find("PersistentController").GetComponent<DrawingDefautStore>().DefaultWidth;
    }

    public void SetLineWidth(float lWidth)
    {
        if (lWidth >= 0.0f)
        {
        }
        lineWidth = lWidth;
    }

    public float GetCurrentLineWidth()
    {
        if (lineWidth >= 0)
            return lineWidth;
        return 0.0f;
    }

    public void SmallWidth()
    {
        SetLineWidth(6.0f);
    }

    public void MediumWidth()
    {
        SetLineWidth(10.0f);
    }

    public void HighWidth()
    {
        SetLineWidth(14.0f);
    }
}