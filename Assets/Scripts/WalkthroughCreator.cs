﻿using UnityEngine;

public class WalkthroughCreator : MonoBehaviour
{
    public GameObject grid;
    public GameObject WalkButton;
    public string[] WalkEntries;
    public GameObject CurrentLabel;
    public GameObject TitleLabel;
    public GameObject Icon;
    private UISprite _icon;
    public string TitleText;
    public string IconName;
   
    // Use this for initialization
    private void Start()
    {
        TitleLabel.GetComponent<UILabel>().text = TitleText;
        _icon = Icon.GetComponent<UISprite>();
        _icon.spriteName = IconName;

        foreach (var s in WalkEntries)
        {
            NGUITools.AddChild(grid, WalkButton);
        }

        WalkButtonScript [] temp = grid.GetComponentsInChildren<WalkButtonScript>();

        for(int i = 0; i <= temp.Length-1;i++)
        {
            temp[i].Info = WalkEntries[i];
        }

        CurrentLabel.GetComponent<UILabel>().text = WalkEntries[0];
    }

    void OnEnable()
    {
        if (Application.loadedLevelName == "Overview")
        {
            var temp = GameObject.Find("Controller").GetComponent<BtnController>();
            temp.DisableAllFunctions();
            temp.Reset();
        }
            
        Debug.Log("Disabled all functions.");

        grid.GetComponent<UIGrid>().Reposition();
    }

    public void DestroyOnClick()
    {
        if (Application.loadedLevelName == "Overview")
        {
            GameObject.Find("Controller").GetComponent<BtnController>().Reset();
        }

        Destroy(gameObject);
    }

    public void Update()
    {
        grid.GetComponent<UIGrid>().enabled = false;
        grid.GetComponent<UIGrid>().enabled = true;
    }
}