﻿using UnityEngine;

public class PinchZoom : MonoBehaviour
{
    public float orthoZoomSpeed = 0.5f; // The rate of change of the orthographic size in orthographic mode.
    public float perspectiveZoomSpeed = 0.01f; // The rate of change of the field of view in perspective mode.
    public float zoomMax = 100.0f;
    public float zoomMin = 10.0f;

    private void Update()
    {
        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            var prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            var touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            var deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            if (camera.isOrthoGraphic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                camera.orthographicSize += deltaMagnitudeDiff*orthoZoomSpeed;

                // Make sure the orthographic size never drops below zero.
                camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                camera.fieldOfView += deltaMagnitudeDiff*perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between Min and Max Zoom.
                camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, zoomMin, zoomMax);
            }
        }
    }
}