﻿using Assets.DBHelper;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BFSGTVVideoList : MonoBehaviour
{
    public static string StoredCatagory = "";
    private GameObject _button;
    private UILabel[] _labelArray;
    private string _text;
    public GameObject NoVideos;
    public GameObject ParentObject;
    public string[] TvLabels;
    public GameObject UiObject;
    private UIInput _seachInput;
    public GameObject SearchInput;
    public GameObject VideoPrivStatus;
    public GameObject VideoPubStatus;
    private bool _isSearchActive;
    public GameObject CategoryLabel;
    private UILabel _categoryLabel;


    private void OnEnable()
    {

        //var tempstring = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().CatagoryString;
        GetListFromArray("", 2);

        if (Application.loadedLevelName == "TVLibrary")
        {
            _categoryLabel = CategoryLabel.GetComponent<UILabel>();
        }
    }

    public void EnableSearch()
    {
        _isSearchActive = !_isSearchActive;
        if (_isSearchActive)
        {
            NGUITools.SetActive(SearchInput, true);
            _seachInput = SearchInput.GetComponent<UIInput>();
            _seachInput.isSelected = true;
        }
        else
        {
            NGUITools.SetActive(SearchInput, false);
        }
    }

    public void GetListFromCategory(string category)

    {
        _categoryLabel.text = category;
        StoredCatagory = category;

        foreach (Transform childTransform in gameObject.transform)
        {
            Destroy(childTransform.gameObject);
        }

        NGUITools.SetActive(NoVideos, true);

        foreach (var video in BFSRSwitchController.Instance.RecordingList(category))
        {
            CreateVideoButton((Video) video);
        }
    }

    public void GetListFromArray(string str, int status)
    {
        Debug.Log("GETTING LIST FROM ARRAY : string = " + str + " ... and int = " + status);
        foreach (Transform childTransform in gameObject.transform)
        {
            Destroy(childTransform.gameObject);
        }

        NGUITools.SetActive(NoVideos, true);

        var videos = new Video().GetAllVideos("Name like '%" + str + "%' AND Status IN ('" + Status.Local.ToString() + "','" + Status.Completed.ToString() + "')");


        foreach (var video in videos)
        {
            switch(status)
            {
                case 0:
                    //public
                    if(video.Public && video.Owner==Owner.captiv8plus)
                        CreateVideoButton((Video)video);
                    break;
                case 1:
                    //private
                    if (video.Private && video.Owner == Owner.captiv8plus)
                        CreateVideoButton((Video)video);
                    break;
                case 2:
                    //all minus captiv8
                    if(video.Owner != Owner.captiv8)
                        CreateVideoButton((Video)video);
                    break;
            }
            
        }
    }

    private void CreateVideoButton(Video video)
    {
        NGUITools.SetActive(NoVideos, false);

        NewButtonInit(video);

        GetImagePathAndFillTex(video);

        ReadText(video);

        LabelArrayFileNameCreation(video);

        if(video.Public)
            _button.transform.FindChild("PubStatus").GetComponent<UISprite>().spriteName = "public_share_status_256";

        if (video.Private)
            _button.transform.FindChild("PrivStatus").GetComponent<UISprite>().spriteName = "private_share_status_256_flip";
    }

    private void NewButtonInit(Video video)
    {
        _button = NGUITools.AddChild(ParentObject, UiObject);
        _button.name = video.Name;
        _button.GetComponentInChildren<FileNameStore>().Data = video.Id.ToString();
    }

    private void LabelArrayFileNameCreation(Video video)
    {
        _labelArray = _button.GetComponentsInChildren<UILabel>();

        _labelArray[0].text = video.Name;

        _labelArray[1].text =Convert.ToString(video.Id);

        SetTimeLabel(_labelArray[2]);

        _labelArray[3].text = SetDateLabel(video);
    }

    private static string SetDateLabel(Video video)
    {
        return video.DateCreated.ToString("dd/MM/yyyy");
    }

    private void GetImagePathAndFillTex(Video video)
    {
       if(!string.IsNullOrEmpty(video.PngPath))
       {
           PopulateTexture(video.PngPath);
        }
    }

    private void SetTimeLabel(UILabel timelable)
    {
        int timeint;
        int.TryParse(_text, out timeint);
        var minutes = timeint/60;
        var seconds = timeint%60;

        ConvertTextToTimeLabel(timelable, minutes, seconds);
    }

    private void ConvertTextToTimeLabel(UILabel timelable, int minutes, int seconds)
    {
        _text = minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
        timelable.text = _text;
    }

    private void ReadText(Video video)
    {
        _text = video.Duration.ToString();
    }

    private void PopulateTexture(string imagepath)
    {
        if (string.IsNullOrEmpty(imagepath)) return;
        var url = "file:///" + imagepath;
        var www = new WWW(url);
        while (!www.isDone)
        {
        }
        _button.GetComponentInChildren<UITexture>().mainTexture = www.texture;
    }
}