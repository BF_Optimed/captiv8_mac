﻿using System;
using UnityEngine;
using Assets.Scripts;

public class BtnController : MonoBehaviour
{
    private bool[] _actionActives = new bool [11];
    private float[] _actionAmounts;
    private bool _actionClickable = true;
    private lineDrawMouse _drawingScript;
    private BFSDDrawingGUI _guiDrawingController;
    private UISprite _menuBtnSprite;
    private bool _menuClickable = true;
    private bool _panActive;
    private UISprite _panBtnSprite;
    private bool _panClickable = true;
    private BFSMPan _panScript;
    private bool _partsActive;
    private bool _penActive;
    private UISprite _penBtnSprite;
    private bool _penClickable = true;
    private bool _recordActive;
    private UISprite _recordBtnSprite;
    private bool _recordClickable = true;
    private RecordScript _recordController;
    private UISprite _resetBtnSprite;
    private BFSASlider _scrAnimSlider;
    private BFSUTransition _scrTransition;
    private bool _tvActive;
    private UISprite _tvBtnSprite;
    private bool _tvClickable = true;
    private TVGUIController _tvController;
    public GameObject[] ActionButtons;
    public GameObject[] AdnexaArray;
    public GameObject BlackOut;
    public GameObject CatagoryBtn;
    public GameObject[] CiliaryArray;
    public GameObject CursorController;
    public GameObject Eye;
    public GameObject[] FrontSectionArray;
    public GameObject GreyOut;
    public GameObject InputController;
    public GameObject[] MaculaArray;
    public GameObject MenuBtnContainer;
    public GameObject MenuButton;
    public GameObject[] OpticNerveArray;
    public GameObject[] OverviewArray;
    public GameObject[] RoomViewArray;
    public GameObject PanButton;
    public GameObject PenButton;
    public GameObject RecordButton;
    public GameObject ResetButton;
    public GameObject TvButton;
    public GameObject ButtonIcon;
    private UILabel _panLabel;
    private UILabel _penLabel;
    private UILabel _recordLabel;
    private UILabel _resetLabel;
    private UILabel _tvLabel;
    public UISprite ActionBtnSprite { get; set; }
    public int ActionSelected { get; set; }
    public bool Autopiloting = false;
    public BFSMCamZoom CamZoomScript { get; set; }
    public bool MenuActive { get; set; }
    public BFSMRotation RotationScript { get; set; }
    public RotateAxis SpinScript;
    public GameObject ActionSlider;
    private bool _ifHoverAllowed = true;
    public GameObject SaveBox;

    private int _oldActionSelected;

    public int DepthCount { get; set; }

    private void Start()
    {
        Debug.Log("Unity Editor");
        InitiliseScripts();

        InitiliseSprites();

        InitiliseActionButtons();

        _panLabel = PanButton.GetComponentInChildren<UILabel>();
        _penLabel = PenButton.GetComponentInChildren<UILabel>();
        _recordLabel = RecordButton.GetComponentInChildren<UILabel>();
        _resetLabel = ResetButton.GetComponentInChildren<UILabel>();
        _tvLabel = TvButton.GetComponentInChildren<UILabel>();

        DepthCount = 0;

        Reset();
        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.ControllingTheModelTitle, MagicStrings.ControllingTheModelContent, MagicStrings.ControllingTheModelKey, MagicStrings.ControllingTheModelIcon);
        
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            if (!SaveBox.activeSelf)
                PenButtonClick();
        }
    }

    private void InitiliseActionButtons()
    {
        _actionActives = new bool[11];
        _actionAmounts = new float[11];

        CreateActionActiveVars();

        CreateActionAmountVars();
    }

    private void CreateActionAmountVars()
    {
        for (var i = 0; i <= _actionAmounts.Length - 1; i++)
        {
            _actionAmounts[i] = 0;
        }
    }

    private void CreateActionActiveVars()
    {
        for (var i = 0; i <= ActionButtons.Length - 1; i++)
        {
            _actionActives[i] = false;
        }
    }

    private void InitiliseSprites()
    {
        _penBtnSprite = PenButton.GetComponent<UISprite>();
        _resetBtnSprite = ResetButton.GetComponent<UISprite>();
        _recordBtnSprite = RecordButton.GetComponent<UISprite>();
        _tvBtnSprite = TvButton.GetComponent<UISprite>();
        _panBtnSprite = PanButton.GetComponent<UISprite>();
        _menuBtnSprite = MenuButton.GetComponent<UISprite>();
    }

    private void InitiliseScripts()
    {
        Autopiloting = false;
        SpinScript = Eye.GetComponent<RotateAxis>();
        Debug.Log("Spin script should be set.");
        _guiDrawingController = gameObject.GetComponent<BFSDDrawingGUI>();
        _recordController = gameObject.GetComponent<RecordScript>();
        RotationScript = Camera.main.GetComponent<BFSMRotation>();
        CamZoomScript = Camera.main.GetComponent<BFSMCamZoom>();
        _panScript = Camera.main.GetComponent<BFSMPan>();
        _drawingScript = Camera.main.GetComponent<lineDrawMouse>();
        _scrTransition = BlackOut.GetComponent<BFSUTransition>();
        _scrAnimSlider = ActionSlider.GetComponent<BFSASlider>();
    }

    public void Reset()
    {
        DeactivateAnyActiveButtons();

        ResetScriptAndFunctions();

        ResetSprites();

        EnableAllBtns();

        DeactivateAllActions();

        ResetAllActionSprites();
        _panLabel.color = Color.white;
        _penLabel.color = Color.white;
        _ifHoverAllowed = true;
    }

    private void ResetAllActionSprites()
    {
        for (var i = 0; i <= ActionButtons.Length - 1; i++)
        {
            ActionBtnSprite = ActionButtons[i].GetComponent<UISprite>();
            ActionBtnSprite.color = new Color(0, 0, 0, 1f);
        }
    }

    private void DeactivateAllActions()
    {
        Debug.Log("action button length : " + ActionButtons.Length);
        for (var i = 0; i <= ActionButtons.Length - 1; i++)
        {
            _actionActives[i] = false;
        }
    }

    private void ResetSprites()
    {
        _penBtnSprite.spriteName = MagicStrings.PenBtnSprite;
        _panBtnSprite.spriteName = MagicStrings.PanBtnSprite;
        _tvBtnSprite.spriteName = MagicStrings.TvBtnSprite;
       // _recordBtnSprite.spriteName = MagicStrings.RecordBtnSprite;
    }

    private void ResetScriptAndFunctions()
    {
        BFSRSwitchController.Instance.Component = BFSRComponent.Transformation;
        SpinScript.enabled = true;
        RotationScript.MovementAllowed = true;
        RotationScript.enabled = true;
        _panScript.enabled = true;
        CamZoomScript.enabled = true;
        _panScript.MouseSelection = 2;
        _drawingScript.enabled = false;
       // _recordController.DeactivateRecord();
        _guiDrawingController.TurnOffDrawing();
        //_scrAnimSlider.ResetAnims();
        ActionSlider.SetActive(false);
        NGUITools.SetActive(GreyOut, false);
    }

    private void DeactivateAnyActiveButtons()
    {
        _penActive = false;
        _panActive = false;
        _tvActive = false;
        _recordActive = false;
    }

    public void DisableAllFunctions()
    {
        SpinScript.enabled = false;
        RotationScript.MovementAllowed = false;
        RotationScript.enabled = false;
        CamZoomScript.enabled = false;
        _drawingScript.enabled = false;
        _panScript.enabled = false;
    }

    private void EnableAllBtns()
    {
        _penClickable = true;
        _panClickable = true;
        _recordClickable = true;
        _actionClickable = true;
        _tvClickable = true;
        _menuClickable = true;
    }

    private void DisableBtnsExcept(string button)
    {
        DisableAllButtons();
        EnableButton(button);
    }

    private void EnableButton(string button)
    {
        switch (button)
        {
            case MagicStrings.Pen:
                _penClickable = true;
                break;
            case MagicStrings.Pan:
                _panClickable = true;
                break;
            case MagicStrings.TV:
                _tvClickable = true;
                break;
            case MagicStrings.Action:
                _actionClickable = true;
                break;
            case MagicStrings.Menu:
                _menuClickable = true;
                break;
            default:
                Debug.Log("There was no matching button, check the code!");
                break;
        }
    }

    private void DisableAllButtons()
    {
        _penClickable = false;
        _panClickable = false;
        //_recordClickable = false;
        _tvClickable = false;
        _actionClickable = false;
        //_menuClickable = false;
    }

    public void PenButtonClick()
    {
        if (_penClickable)
        {
            _penActive = !_penActive;
            if (_penActive)
            {
                EnableDrawing();
            }
            else
            {
                DisableDrawing();
            }
        }
    }

    public void DisableDrawing()
    {
        _penActive = false;
        BFSRSwitchController.Instance.Component = BFSRComponent.Transformation;
        _penBtnSprite.spriteName = MagicStrings.PenBtnSprite;
        _penLabel.color = Color.white;
        _ifHoverAllowed = true;
        Reset();
    }

    private void EnableDrawing()
    {
        Reset();
        ActionSlider.SetActive(false);
        _penActive = true;
        
        DepthCount = 0;
        BFSRSwitchController.Instance.Component = BFSRComponent.Drawing;
        _guiDrawingController.ActivateDrawing();
        DisableBtnsExcept(MagicStrings.Pen);
        DisableAllFunctions();
        _drawingScript.enabled = true;
        _penBtnSprite.spriteName = MagicStrings.PenBtnSpriteHit;
        _penLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
        _ifHoverAllowed = false;
        _actionClickable = true;
    }

    public void PanButtonClick()
    {
        if (_panClickable)
        {
            _panActive = !_panActive;
            if (_panActive)
            {
                EnablePan();
            }
            else
            {
                Reset();
            }
        }
    }

    private void EnablePan()
    {
        BFSRSwitchController.Instance.Component = BFSRComponent.Transformation;
        DisableBtnsExcept(MagicStrings.Pan);
        DisableAllFunctions();
        _panScript.MouseSelection = 0;
        _panScript.enabled = true;
        _panBtnSprite.spriteName = MagicStrings.PanBtnSpriteHit;
        _panLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
        _ifHoverAllowed = false;
    }

    public void RecordButtonClick()
    {
        if (_recordClickable)
        {
            _recordActive = !_recordActive;
            if (_recordActive)
            {
                EnableRecording();
            }
            else
            {
                DisableRecording();
            }
        }
    }

    private void DisableRecording()
    {
        _recordBtnSprite.spriteName = MagicStrings.RecordBtnSprite;
        _recordController.DeactivateRecord();
        _recordLabel.color = Color.white;
    }

    private void EnableRecording()
    {
        _recordController.ActivateRecordingToolbar();
        _recordBtnSprite.spriteName = MagicStrings.RecordBtnSpriteHit;
        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.RecordTitle, MagicStrings.RecordContent, MagicStrings.RecordKey, MagicStrings.RecordIcon);
        _recordLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void TvButtonClick()
    {
        Application.LoadLevel("TVLibrary");
    }

    private void ActivateTvView()
    {
        _recordController.DeactivateRecord();
        Reset();
        _tvActive = !_tvActive;
        DisableBtnsExcept(MagicStrings.TV);
        DisableAllFunctions();
        RotationScript.enabled = false;
        _panScript.enabled = false;
        _recordBtnSprite.spriteName = MagicStrings.RecordBtnSprite;
        _tvController.TvLibraryActivate();
        _tvBtnSprite.spriteName = MagicStrings.TvBtnSpriteHit;
    }

    public void ActionButtonClick(GameObject btn)
    {
        //_scrAnimSlider.ResetAnims();
        ActionSelected = Array.IndexOf(ActionButtons, btn);
        Debug.Log("ACTION BUTTON CLICK -- ACTIVE IS :" + _actionClickable);
        if (_actionClickable)
        {
            _actionActives[ActionSelected] = !_actionActives[ActionSelected];

            if (_actionActives[ActionSelected])
            {
                EnableChosenAction();
            }
            else
            {
                DisableChosenAction();
            }
        }
    }

    private void DisableChosenAction()
    {
        Reset();
        ActionSlider.SetActive(false);
    }

    private void EnableChosenAction()
    {
        Reset();
        {
            _actionActives[ActionSelected] = true;
        }
        if(ActionSelected != _oldActionSelected)
        {
            _scrAnimSlider.ResetAnims();
            _oldActionSelected = ActionSelected;
        }

        DisableAllFunctions();

        DisableBtnsExcept(MagicStrings.Action);

        _penClickable = true;

        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.ModelsAndInteractionsTitle, MagicStrings.ModelsAndInteractionsContent, MagicStrings.ModelsAndInteractionsKey, MagicStrings.ModelsAndInteractionsIcon);

        ActionSlider.SetActive(true);

        _scrAnimSlider.SelectedAction = ActionSelected;

        ResetAllActionSprites();

        ActionButtons[ActionSelected].GetComponent<UISprite>().color = new Color(0.5f, 0.5f, 0.5f, 1f);

        DeactivateAllActions();

        _actionActives[ActionSelected] = true;
    }

    public void MenuButtonClick()
    {
        if (!Autopiloting)
        {
            if (_menuClickable)
            {
                MenuActive = !MenuActive;
                if (MenuActive)
                {
                    EnableMenu();
                }
                else
                {
                    DisableMenu();
                }
                MenuButton.GetComponent<UIGrowthScript>().InputRecieved();
            }
        }
    }

    private void DisableMenu()
    {
        ButtonIcon.GetComponent<TweenRotation>().PlayForward();
        Reset();
        _menuBtnSprite.alpha = 1.0f;
    }

    private void EnableMenu()
    {
        DisableAllFunctions();
        ButtonIcon.GetComponent<TweenRotation>().PlayForward();
        DisableBtnsExcept(MagicStrings.Menu);
        NGUITools.SetActive(GreyOut, true);
        _menuBtnSprite.alpha = 0.2f;
        _scrAnimSlider.ResetAnims();
    }

    public void ResetButtonClick()
    {
        _scrAnimSlider.ResetAnims();
        _scrTransition.Begin(_scrTransition.CurrentObj.name, true);
    }

    public void HomeButtonClick()
    {
        if(BFSRSwitchController.Instance.Mode == BFSRMode.Record)
        {
            gameObject.GetComponent<CreateNotification>().CreateOneButton("Warning", "Returning home will stop the recording, are you sure you wish to do this?", "Yes", "No", new EventDelegate(LoadSameLevel));
        }
        else
        {
            LoadSameLevel();
        }
    }

    public void LoadSameLevel()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void CatagoryBtnClick()
    {
        DisableAllFunctions();
        CatagoryBtn.GetComponent<UIGrowthScript>().InputRecieved();
    }



    public void ResetHoverOver()
    {
        _resetBtnSprite.spriteName = MagicStrings.ResetBtnSpriteHit;
        _resetLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void ResetHoverOut()
    {
        _resetBtnSprite.spriteName = MagicStrings.ResetBtnSprite;
        _resetLabel.color = Color.white;
    }

    public void PenHoverOver()
    {
        if (_ifHoverAllowed)
        {
            _penBtnSprite.spriteName = MagicStrings.PenBtnSpriteHit;
            _penLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
        }
    }

    public void PenHoverOut()
    {
        if (!_penActive)
        {
            _penBtnSprite.spriteName = MagicStrings.PenBtnSprite;
            _penLabel.color = Color.white;
        }
    }

    public void PanHoverOver()
    {
        if (_ifHoverAllowed)
        {
            _panBtnSprite.spriteName = MagicStrings.PanBtnSpriteHit;
            _panLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
        }
    }

    public void PanHoverOut()
    {
        if (!_panActive)
        {
            _panBtnSprite.spriteName = MagicStrings.PanBtnSprite;
            _panLabel.color = Color.white;
        }
    }

    public void TvHoverOver()
    {
        _tvBtnSprite.spriteName = MagicStrings.TvBtnSpriteHit;
    }

    public void TvHoverOut()
    {
        if (!_tvActive)
        {
            _tvBtnSprite.spriteName = MagicStrings.TvBtnSprite;
        }
    }

    public void RecordHoverOver()
    {
        _recordBtnSprite.spriteName = MagicStrings.RecordBtnSpriteHit;
        _recordLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void RecordHoverOut()
    {
        if (!_recordActive)
        {
            _recordBtnSprite.spriteName = MagicStrings.RecordBtnSprite;
            _recordLabel.color = Color.white;
        }
    }
}