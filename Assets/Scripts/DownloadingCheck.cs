﻿using UnityEngine;
using System.Collections;
using Assets.SynchroniseScript;

public class DownloadingCheck : MonoBehaviour {

    private UISprite spr;

	// Use this for initialization
	void Start () {
        spr = gameObject.GetComponent<UISprite>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(SynchroniseSingleton.Instance.IsDownloading)
        {
            spr.enabled = true;
        }
        else
        {
            spr.enabled = false;
        }
	}
}
