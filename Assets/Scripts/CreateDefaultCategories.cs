﻿using UnityEngine;

public class CreateDefaultCategories : MonoBehaviour
{
    private void Start()
    {
        var categories = new[]
        {"Adnexa", "Overview", "Cilliary Body", "Optic Nerve", "Default", "Macula", "Front Section"};
        BFSRSwitchController.Instance.CreateCategories(categories);
    }
}