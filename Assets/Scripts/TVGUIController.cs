﻿using System.IO;
using UnityEngine;
using Assets.Scripts;

public class TVGUIController : MonoBehaviour
{
    private readonly GameObject _sliderRoot;
    private UISprite _audioBtnSprite;
    private UILabel _audioLabel;
    private UISprite _cutBtnSprite;
    private UILabel _cutLabel;
    private bool _cuttingFramesOpen;
    private string _fnameStored = "";
    private UISprite _playBtn;
    private bool _playSpriteActive;
    private BFSERecordAudio _recAud;
    private bool _recordingAud;
    private RecordScript _recScr;
    private UISlider _slider1;
    private UISlider _slider2;
    private bool _subsActive;
    private UISprite _subsBtnSprite;
    private SubtitleCreator _subScript;
    private UILabel _subsLabel;
    private UISprite _thumbBtnSprite;
    private UILabel _thumbLabel;
    private bool _thumbnailOpen;
    private TweenScale _twAudioBtn;
    private TweenScale _twCutBtn;
    private TweenScale _twSubsBtn;
    private TweenScale _twThumbBtn;
    private bool _voiceOverOpen;
    public GameObject ActionDeactivate;
    private bool Active = true;
    public GameObject EditView;
    public GameObject EditViewHigh;
    public GameObject GoAudioBtn;
    public GameObject GoAudNotif;
    public GameObject GoCutBtn;
    public GameObject GoShareDesc;
    public GameObject GoShareEmail;
    public GameObject GoShareName;
    public GameObject GoSlider1;
    public GameObject GoSlider2;
    public GameObject GoSubsBtn;
    public GameObject GoSubtitles;
    public GameObject GoThumbBtn;
    public GameObject LbAudioBtn;
    public GameObject LbCutBtn;
    public GameObject LbSubsBtn;
    public GameObject LbThumbBtn;
    public GameObject Play;
    public GameObject RecordController;
    public GameObject RelatedVideos;
    public GameObject SecondPlayGui;
    public GameObject SpAudioBtn;
    public GameObject SpCutBtn;
    public GameObject SpSubsBtn;
    public GameObject SpThumbBtn;
    public GameObject SubBox;
    public UISprite TvBtnSprite;
    public GameObject TvButton;
    public GameObject TvOverlay;
    public GameObject TvVideo;
    public GameObject UnderbarChange;
    public GameObject UnderbarChangeTo;

    private void Start()
    {
        TvBtnSprite = TvButton.GetComponent<UISprite>();
        _playBtn = Play.GetComponent<UISprite>();

        _recScr = RecordController.GetComponent<RecordScript>();
        _recAud = Camera.main.GetComponent<BFSERecordAudio>();

        _slider1 = GoSlider1.GetComponent<UISlider>();
        _slider2 = GoSlider2.GetComponent<UISlider>();

        _twAudioBtn = GoAudioBtn.GetComponent<TweenScale>();
        _twThumbBtn = GoThumbBtn.GetComponent<TweenScale>();
        _twSubsBtn = GoSubsBtn.GetComponent<TweenScale>();
        _twCutBtn = GoCutBtn.GetComponent<TweenScale>();

        _audioBtnSprite = SpAudioBtn.GetComponent<UISprite>();
        _thumbBtnSprite = SpThumbBtn.GetComponent<UISprite>();
        _subsBtnSprite = SpSubsBtn.GetComponent<UISprite>();
        _cutBtnSprite = SpCutBtn.GetComponent<UISprite>();

        _audioLabel = LbAudioBtn.GetComponent<UILabel>();
        _thumbLabel = LbThumbBtn.GetComponent<UILabel>();
        _subsLabel = LbSubsBtn.GetComponent<UILabel>();
        _cutLabel = LbCutBtn.GetComponent<UILabel>();

        _subScript = GoSubtitles.GetComponent<SubtitleCreator>();
    }

    public void TvLibraryActivate()
    {
        NGUITools.SetActive(TvOverlay, true);
        NGUITools.SetActive(ActionDeactivate, false);
    }

    public void TvLibraryDeactivate()
    {
        NGUITools.SetActive(TvOverlay, false);
        NGUITools.SetActive(TvVideo, false);
        NGUITools.SetActive(EditView, false);
        NGUITools.SetActive(EditViewHigh, false);
        NGUITools.SetActive(ActionDeactivate, true);
        NGUITools.SetActive(_sliderRoot, true);
        NGUITools.SetActive(SecondPlayGui, false);
    }

    public void VideoButtonClick(string filename)
    {
        Debug.Log("Video Id : " + filename);
        _fnameStored = filename;

        NGUITools.SetActive(TvOverlay, false);
        NGUITools.SetActive(_sliderRoot, false);
        NGUITools.SetActive(UnderbarChange, true);
        NGUITools.SetActive(TvVideo, true);
        NGUITools.SetActive(SecondPlayGui, true);
        Camera.main.rect = new Rect(0.07f, 0.15f, 0.63f, 0.67f);
        Camera.main.depth = 1.2f;
        var info = new DirectoryInfo(filename);
        RelatedVideos.GetComponent<BFSGTVVideoList>().GetListFromCategory(info.Parent.Name);
        _recScr.LoadVideoFiles(filename);
        BFSRSwitchController.Instance.AddSliderToReplay(GameObject.Find("Slider1").GetComponent<UISlider>());
    }

    public void BackButton()
    {
        NGUITools.SetActive(TvOverlay, true);
        NGUITools.SetActive(TvVideo, false);
        NGUITools.SetActive(EditView, false);
        NGUITools.SetActive(EditViewHigh, false);
        NGUITools.SetActive(SecondPlayGui, false);
        Camera.main.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
        Camera.main.depth = 0;
        //Camera.main.GetComponent<BFSERecordAudio>().StopRecording();
       // Camera.main.GetComponent<BFSERecordAudio>().StopPlayingAudio();
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
    }

    public void SaveButton()
    {
        _recScr.SaveRecording = true;
        //Camera.main.GetComponent<BFSERecordAudio>().SaveClick();
        VideoButtonClick(_fnameStored);
        if (BFSRSwitchController.Instance.Mode != BFSRMode.Pause)
        {
            BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
        }
        //Camera.main.GetComponent<BFSERecordAudio>().PlayingAudio = false;
        EditBackButton();
    }

    public void EditButton()
    {
        NGUITools.SetActive(TvVideo, false);
        NGUITools.SetActive(UnderbarChange, false);
        NGUITools.SetActive(EditView, true);
        EditViewHigh.SetActive(true);
        NGUITools.SetActive(SecondPlayGui, false);

        _twAudioBtn.ResetToBeginning();
        _twThumbBtn.ResetToBeginning();
        _twSubsBtn.ResetToBeginning();
        _twCutBtn.ResetToBeginning();

        NGUITools.SetActive(GoThumbBtn, false);
        NGUITools.SetActive(GoAudioBtn, false);
        NGUITools.SetActive(GoSubsBtn, false);
        NGUITools.SetActive(GoCutBtn, false);

        //Camera.main.GetComponent<BFSERecordAudio>().StopRecording();
        //Camera.main.GetComponent<BFSERecordAudio>().StopPlayingAudio();
        NGUITools.SetActive(UnderbarChangeTo, true);
        BFSRSwitchController.Instance.Mode = BFSRMode.Play;
        BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
    }

    public void EditBackButton()
    {
        NGUITools.SetActive(TvVideo, true);
        NGUITools.SetActive(UnderbarChange, true);
        NGUITools.SetActive(EditView, false);
        NGUITools.SetActive(EditViewHigh, false);
        NGUITools.SetActive(SecondPlayGui, true);
        NGUITools.SetActive(UnderbarChangeTo, false);
        BFSRSwitchController.Instance.AddSliderToReplay(GameObject.Find("Slider1").GetComponent<UISlider>());
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
        _audioBtnSprite.spriteName = MagicStrings.AudioBtnSprite;
        _audioLabel.color = Color.black;
        _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSprite;
        _subsLabel.color = Color.black;
        _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSprite;
        _thumbLabel.color = Color.black;
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSprite;
        _cutLabel.color = Color.black;
        _subsActive = false;
        _voiceOverOpen = false;
        _thumbnailOpen = false;
        _cuttingFramesOpen = false;
    }

    public void EditCancelNotif()
    {
        gameObject.GetComponent<CreateNotification>()
            .CreateOneButton("Cancel Edit?", "Are you sure you wish to cancel the changes?", "Yes", "No",
                new EventDelegate(EditBackButton));
    }

    //Subtitle Click Event
    public void SubtitlesClick()
    {
        if (_subsActive)
        {
            DisableSubtitlesGui();
        }
        else
        {
            EnableSubtitlesGui();
        }
        _subsActive = !_subsActive;
    }

    private void EnableSubtitlesGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        NGUITools.SetActive(GoSlider2, true);

		_subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSpriteHit;
		_subsLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    private void DisableSubtitlesGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
		_subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSprite;
        _subsLabel.color = Color.black;
    }

    public void VoiceOverButtons()
    {
        if (_voiceOverOpen)
        {
            DisableVoiceOverGui();
        }
        else
        {
            EnableVoiceOverGui();
        }
        _voiceOverOpen = !_voiceOverOpen;
    }

    private void EnableVoiceOverGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        NGUITools.SetActive(GoSlider2, false);
        NGUITools.SetActive(GoAudNotif, true);
		_audioBtnSprite.spriteName = MagicStrings.AudioBtnSpriteHit;
		_audioLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);

        BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
    }

    private void DisableVoiceOverGui()
    {
        NGUITools.SetActive(GoAudNotif, false);
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
		_audioBtnSprite.spriteName = MagicStrings.AudioBtnSprite;
        _audioLabel.color = Color.black;
    }

    public void VoiceOverClick()
    {
        if (_recordingAud)
        {
            _recScr.StopRecordVoiceoOver();
        }
        else
        {
            BFSRSwitchController.Instance.AddSliderToReplay(_slider1);
            _recScr.RecordVoiceOver();
        }

        _recScr.PlayLoadedVideo();
        _recordingAud = !_recordingAud;
    }

    public void OpenCutFrames()
    {
        if (_cuttingFramesOpen)
        {
            DisableCutFramesGui();
        }
        else
        {
            EnableCutFramesGui();
        }
        _cuttingFramesOpen = !_cuttingFramesOpen;
    }

    private void EnableCutFramesGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        NGUITools.SetActive(GoSlider2, true);
		_cutBtnSprite.spriteName = MagicStrings.CutBtnSpriteHit;
		_cutLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    private void DisableCutFramesGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
		_cutBtnSprite.spriteName = MagicStrings.CutBtnSprite;
        _cutLabel.color = Color.black;
    }

    public void CutFrames()
    {
        //var one = _slider1.value;
        //var two = _slider2.value;
        //if (two >= one)
        //{
        //    _recScr.CutFrames(one, two);
        //}
        //else
        //{
        //    _recScr.CutFrames(two, one);
        //}
    }

    public void CreateThumbnailOpen()
    {
        if (_thumbnailOpen)
        {
            DisableThumbGui();
        }
        else
        {
            EnableThumbGui();
        }
        _thumbnailOpen = !_thumbnailOpen;
    }

    private void EnableThumbGui()
    {
        NGUITools.SetActive(GoSlider1, true);
        NGUITools.SetActive(GoSlider2, false);
		_thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSpriteHit;
		_thumbLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    private void DisableThumbGui()
    {
        NGUITools.SetActive(GoSlider1, false);
        NGUITools.SetActive(GoSlider2, false);
		_thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSprite;
        _thumbLabel.color = Color.black;
    }

    public void MainPlayBtnClick()
    {
        if (_playSpriteActive)
        {
            _playBtn.spriteName = MagicStrings.Play;
        }
        else
        {
            _playBtn.spriteName = MagicStrings.Pause;
        }
        _playSpriteActive = !_playSpriteActive;
    }

    public void SecondaryPlayBtnClick()
    {
        if (!_playSpriteActive) return;
		_playBtn.spriteName = MagicStrings.Play;
        _playSpriteActive = false;
    }

    public void AudioBtnHover()
    {
		_audioBtnSprite.spriteName = MagicStrings.AudioBtnSpriteHit;
		_audioLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void AudioBtnHoverOut()
    {
        if (!_voiceOverOpen)
        {
			_audioBtnSprite.spriteName = MagicStrings.AudioBtnSprite;
            _audioLabel.color = Color.black;
        }
    }

    public void SubsBtnHover()
    {
        _subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSpriteHit;
		_subsLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void SubsBtnHoverOut()
    {
        if (!_subsActive)
        {
			_subsBtnSprite.spriteName = MagicStrings.SubtitleBtnSprite;
            _subsLabel.color = Color.black;
        }
    }

    public void ThumbBtnHover()
    {
        _thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSpriteHit;
		_thumbLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void ThumbBtnHoverOut()
    {
        if (!_thumbnailOpen)
        {
			_thumbBtnSprite.spriteName = MagicStrings.ThumbnailBtnSprite;
            _thumbLabel.color = Color.black;
        }
    }

    public void CutBtnHover()
    {
        _cutBtnSprite.spriteName = MagicStrings.CutBtnSpriteHit;
		_cutLabel.color = new Color (0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void CutBtnHoverOut()
    {
        if (_cuttingFramesOpen) return;
		_cutBtnSprite.spriteName = MagicStrings.CutBtnSprite;
        _cutLabel.color = Color.black;
    }
}