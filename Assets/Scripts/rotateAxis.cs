using UnityEngine;

public class RotateAxis : MonoBehaviour
{
    public Quaternion _fromRotation;
    private bool _isRotating;
    public Quaternion _toRotation;
    private float _yDeg;
    public float Friction = 2.0f;
    public float LerpSpeed = 2.0f;
    public int Speed = 1;
    public GameObject Blackout;

    private void Start()
    {
        _toRotation = _fromRotation = transform.rotation;
    }

    private void Update()
    {
        if (CheckForNonRotationObjects())
        {
            if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if (!_isRotating)
                {
                    _yDeg = transform.rotation.eulerAngles.z;
                }

                _isRotating = true;
            }

            if (_isRotating)
            {
                RotateTransform(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            }
        }  
    }

    bool CheckForNonRotationObjects()
    {
        if (Blackout != null)
        {
            switch (Blackout.GetComponent<BFSUTransition>().CurrentObj.name)
            {
                case "Vision Simulator":
                    return false;
                case "Adnexa":
                    return false;
                default:
                    return true;
            }
        }
        return true;
    }

    private void RotateTransform(float xNum, float yNum)
    {
        _yDeg += xNum*Speed*Friction;
        _fromRotation = transform.rotation;
        var xPos = transform.rotation.eulerAngles.x;
        var yPos = transform.rotation.eulerAngles.y;

        if (Input.GetMouseButton(0) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            
            _toRotation = Quaternion.Euler(xPos, yPos, _yDeg);
        }

        transform.rotation = Quaternion.Lerp(_fromRotation, _toRotation, Time.deltaTime*LerpSpeed);

        if (CheckRotationFinished())
        {
            _isRotating = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            _isRotating = false;
        }
    }

    private bool CheckRotationFinished()
    {
        return (CompareAngles(transform.rotation.eulerAngles.x, _toRotation.eulerAngles.x) < 1) &&
               (CompareAngles(transform.rotation.eulerAngles.y, _toRotation.eulerAngles.y) < 1) &&
               (CompareAngles(transform.rotation.eulerAngles.z, _toRotation.eulerAngles.z) < 1);
    }

    private static float CompareAngles(float x, float y)
    {
        return ((Mathf.Abs((x) - Mathf.Abs(y))));
    }
}