var morphObject : MorphTargets;

//Position variables
var xPos : int = 25;
var yPos : int = 25;
var wide : int = 100;
//var high : int = 30;

var scrollPosition : Vector2; //Position of the scrollview

var labelStyle : GUIStyle;

private var sliderValues : float[];

function Awake () {
	sliderValues = new float[morphObject.attributes.length];
}

function OnGUI () {
	GUILayout.BeginArea(Rect(xPos, yPos, wide, Screen.height - 2*yPos));
	
	scrollPosition = GUILayout.BeginScrollView(scrollPosition);
	
	for (var i=0; i<morphObject.attributes.length; i++) {
		sliderValues[i] = TargetSlider (sliderValues[i], 1, morphObject.attributes[i]);
		
		//Set morph attribute progress to equal slider values
		//morphObject.attributeProgress[i] = sliderValues[i];

	}
	
	GUILayout.EndScrollView();
	
	GUILayout.EndArea();
	
	if (GUI.changed) {
		var changedAttribute : int = -1;
		
		//First, detect which slider value has changed, if any
		for (var j = 0; j < morphObject.attributes.length; j++) {
			if (Mathf.Approximately(sliderValues[j], morphObject.attributeProgress[j])) {
				continue;
			} else {
				changedAttribute = j;
			}
		}
		
		if (changedAttribute != -1) {
			//Set morph attribute progress to equal slider value for value that has changed
			morphObject.attributeProgress[changedAttribute] = sliderValues[changedAttribute];
			//Apply the actual morph for this attribute
			//morphObject.SetMorph(changedAttribute);
			morphObject.SetMorph();
		}
	}

}

function TargetSlider (sliderValue : float, sliderMaxValue : float, labelText : String) {
	GUILayout.Label (labelText, labelStyle);
	//screenRect.y += screenRect.height; // <- Push the Slider below the Label
	sliderValue = GUILayout.HorizontalSlider (sliderValue, 0.0, sliderMaxValue);
	return sliderValue;
}