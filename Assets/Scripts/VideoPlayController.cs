﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.DBHelper;
using UnityEngine;
using Assets.Scripts;

public class VideoPlayController : MonoBehaviour {

    private List<BFSRData> _recordData;
    private Video _video;
    public GameObject VideoTimeLabel;
    private BFSERecordAudio scrAudio;
    public GameObject GoSubtitle;
    private SubtitleCreator _subtitleCreator;
    public GameObject[] ObjPrefabs;
    public GameObject VideoSlider;
    public GameObject PlayPause;
    private UISprite _playpause;
    private bool _playActive;
    private VideoEditController _videoEditController;
    private Hibernation _hibernation;

  /*  void OnEnable()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        var temp = GameObject.Find("PersistentController");
        if(temp !=null)
            _video = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo;
        _subtitleCreator = GoSubtitle.GetComponent<SubtitleCreator>();
        scrAudio = Camera.main.GetComponent<BFSERecordAudio>();

        BFSRSwitchController.Instance.Start += Instance_Start;
        BFSRSwitchController.Instance.Stop += Instance_Stop;
        BFSRSwitchController.Instance.finish+=Instance_finish;
        PlayLoadedVideo();
        PauseLoadedVideo();
        Debug.Log("loading video file name called for video  "+ _video.Name);
        LoadVideoFiles(_video);

        if (gameObject.GetComponent<VideoEditController>() != null)
        {
            _videoEditController = gameObject.GetComponent<VideoEditController>();
        }
#endif
    }*/

    void Start()
    {

        var temp = GameObject.Find("PersistentController");
        if(temp !=null)
            _video = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo;
        _hibernation = temp.GetComponent<Hibernation>();
        _subtitleCreator = GoSubtitle.GetComponent<SubtitleCreator>();
        scrAudio = Camera.main.GetComponent<BFSERecordAudio>();

        BFSRSwitchController.Instance.StartEvent += Instance_Start;
        BFSRSwitchController.Instance.Stop += Instance_Stop;
        BFSRSwitchController.Instance.finish+=Instance_finish;
        PlayLoadedVideo();
        PauseLoadedVideo();
        Debug.Log("loading video file name called for video  "+ _video.Name);
        LoadVideoFiles(_video);

        if (gameObject.GetComponent<VideoEditController>() != null)
        {
            _videoEditController = gameObject.GetComponent<VideoEditController>();
        }
    
    }

    private void Instance_finish()
    {
        Debug.Log("Just finished playing.");
        _hibernation.CanHibernate = true;
        if (BFSRSwitchController.Instance.Mode != BFSRMode.Stop)
        {
            if (_videoEditController != null)
            {
                if(_videoEditController._recordingAud)
                    _videoEditController.VoiceOverClick();
            }
        }
    }

    void OnDisable()
    {
        try
        {
            BFSRSwitchController.Instance.StartEvent -= Instance_Start;
            BFSRSwitchController.Instance.Stop -= Instance_Stop;
            BFSRSwitchController.Instance.finish -= Instance_finish;
            Debug.Log("Removing Recording events handler");
        }
        catch
        {
            // ignored
        }
    }

    private void Instance_Stop(BFSREventArgs e)
    {
        if (e.Mode == BFSRMode.Play)
        {
            if (_videoEditController != null)
            {
                //_videoEditController.VoiceOverClick();
            }
        }
    }

    private void Instance_Start(BFSREventArgs e)
    {
        if (e.Mode == BFSRMode.Play)
        {
            Debug.Log("Video Started.");
            _recordData = BFSRSwitchController.Instance.ReadFromFile(_video.XmlPath);

            BFSRSwitchController.Instance.PlayData(new List<BFSRData>(_recordData));
            BFSRSwitchController.Instance.AddSliderToReplay((VideoSlider.GetComponent<UISlider>()));
            BFSRSwitchController.Instance.AddTimerLabelToReplay(VideoTimeLabel.GetComponent<UILabel>());
            if (Application.loadedLevelName == "VideoView")
                BFSRSwitchController.Instance.AddSprite(PlayPause.GetComponent<UISprite>());
        }
    }

    public void PlayLoadedVideo()
    {
        _hibernation.CanHibernate = false ;
        BFSRSwitchController.Instance.Mode = BFSRMode.Play;
        //if (Application.loadedLevelName == "VideoView")
        //{
        //    GameObject.Find("FullScreen").GetComponent<Fullscreen>().MakeFullscreen();
        //}
        if(Application.loadedLevelName == "VideoView")
            scrAudio.PlayAudio();
    }

    public void PauseLoadedVideo()
    {
        _hibernation.CanHibernate = true;
        BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
    }

    public void StopLoadedVideo()
    {
        _hibernation.CanHibernate = true;
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
    }

    public void LoadVideoFiles(Video video)
    {
        Debug.Log("Trying to load audio and subs.");
        if (File.Exists(video.WavPath))
        {
            scrAudio.LoadAudio(video.WavPath);
            Debug.Log("Audio Loaded.");
        }
            
        if (File.Exists(video.VttPath))
        {
            _subtitleCreator.LoadSubFile(video.VttPath);
            Debug.Log("Subs Loaded.");
        }
            
    }

    public GameObject GetModelObject(string modelName)
    {
        for (var i = 0; i <= ObjPrefabs.Length - 1; i++)
        {
            if (ObjPrefabs[i].name == modelName)
            {
                return ObjPrefabs[i];
            }
        }
        return null;
    }
}
