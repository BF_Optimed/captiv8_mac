﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.DBHelper;
using UnityEngine;
using Assets.Helper;
public class SubtitleCreator : MonoBehaviour
{
    private readonly List<string> _chunks = new List<string>();
    private string _fname;
    private string _mainFile;
    private VideoPlayController _videoPlayController;
    private CreateNotification _createNotification;
    private int _subCounter;
    private CreateNotification _createNotif;
    public GameObject EndTime;
    public UILabel EndTimeScript;
    public GameObject Grid;
    public GameObject ReadTime;
    public UILabel ReadTimeScript;
    public GameObject Controller;
    public GameObject StartTime;
    public UILabel StartTimeScript;
    public GameObject SubCurrentDis;
    public UILabel SubCurrentDisScript;
    public GameObject SubDisplay;
    public UIInput SubDisplayScript;
    public GameObject SubEntryPre;
    public List<SubtitleEntry> SubList = new List<SubtitleEntry>();
    public GameObject TextBox;
    public UIInput TextBoxScript;
    private ContinuousVars _continuousVars;

    public bool isEditing = false;
    private SubtitleEntry _editingSub;

    public GameObject LowerSlider;
    public GameObject HigherSlider;

    public UISlider _lowerSlider;
    public UISlider _higherSlider;

    private SliderLimiter _sliderLimiterLow;

    private SliderLimiter _sliderLimiterHigh;

    public UISprite _editingSprite;
    public GameObject AddSubLabel;
    private UILabel _AddSubLabel;
    private bool isBusy;

    private void OnEnable()
    {
        if (Application.loadedLevelName != "Overview")
        {
            _continuousVars = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
            _createNotification = Controller.GetComponent<CreateNotification>();
            SubCurrentDisScript = SubCurrentDis.GetComponent<UILabel>();
            ReadTimeScript = ReadTime.GetComponent<UILabel>();
            _createNotif = Controller.GetComponent<CreateNotification>();
            _videoPlayController = Controller.GetComponent<VideoPlayController>();
        }
        if (Application.loadedLevelName == "VideoEdit")
        {
            TextBoxScript = TextBox.GetComponent<UIInput>();
            StartTimeScript = StartTime.GetComponent<UILabel>();
            EndTimeScript = EndTime.GetComponent<UILabel>();
            _lowerSlider = LowerSlider.GetComponent<UISlider>();
            _higherSlider = HigherSlider.GetComponent<UISlider>();
            _sliderLimiterLow = LowerSlider.GetComponent<SliderLimiter>();
            _sliderLimiterHigh = HigherSlider.GetComponent<SliderLimiter>();
            _AddSubLabel = AddSubLabel.GetComponent<UILabel>();
        }
    }

    public void OnAddClick()
    {
        if (string.IsNullOrEmpty(TextBoxScript.value))
        {
            _createNotification.CreateOkayButton("Error", "You have not entered any text.", "Ok");
        }
        else
        {
            if (isEditing)
            {
                bool passed = CheckSubTimes(CleanTime(StartTimeScript.text), CleanTime(EndTimeScript.text));
                if (passed)
                {
                    isEditing = false;
                    _editingSub.Content = TextBoxScript.value;
                    _editingSub.startTime = CleanTime(StartTimeScript.text);
                    _editingSub.endTime = CleanTime(EndTimeScript.text);
                    _editingSub = null;
                    UpdateMainFile();
                    PopulateList();
                    SetAddSubButtonToAdd();
                    TextBoxScript.value = "";

                    var tempVal = _higherSlider.value + 0.01f;

                    BFSRSwitchController.Instance.AddSliderToReplay(_higherSlider);
                    BFSRSwitchController.Instance.AddTimerLabelToReplay(EndTimeScript);
                    _higherSlider.value += 0.1f;


                    BFSRSwitchController.Instance.AddSliderToReplay(_lowerSlider);
                    BFSRSwitchController.Instance.AddTimerLabelToReplay(StartTimeScript);
                    _lowerSlider.value = tempVal;
                    
                }
                else
                {
                    _createNotif.CreateOkayButton("Cant Do That", "The subtitle you tried to create intersects an existing subtitle.", "OK");
                }
            }
            else
            {
                AddBrandNewSubtitle();
            }
        }
    }

    private void AddBrandNewSubtitle()
    {
        var newStartTime = CleanTime(StartTimeScript.text);
        var newEndTime = CleanTime(EndTimeScript.text);
        Debug.Log(EndTimeScript.text);
        bool passed = CheckSubTimes(newStartTime, newEndTime);
        if (passed)
        {
            AddNewSubtitle(newStartTime, newEndTime);
            Debug.Log("YOU PASSED ALL THE SUBTITLE TIME CHECK TESTS.");
        }
        else
        {
            _createNotif.CreateOkayButton("Cant Do That", "The subtitle you tried to create intersects an existing subtitle.", "OK");
        }
    }

    private bool CheckSubTimes(string newStartTime, string newEndTime)
    {
        bool passed = true;
        foreach (SubtitleEntry s in SubList)
        {
            if(s != _editingSub)
            {
                var ns = ConvertTime(newStartTime);
                var ne = ConvertTime(newEndTime);
                var os = ConvertTime(s.startTime);
                var oe = ConvertTime(s.endTime);

                Debug.Log(" New Start Time : " + ns + " New End Time : " + ne + " Old Start Time : " + os + " Old End Time : " + oe);

                if (ConvertTime(newStartTime) >= ConvertTime(s.startTime) &&
                    ConvertTime(newStartTime) <= ConvertTime(s.endTime))
                {
                    passed = false;
                }

                if (ConvertTime(newEndTime) >= ConvertTime(s.startTime) &&
                    ConvertTime(newEndTime) <= ConvertTime(s.endTime))
                {
                    passed = false;
                }

                if (ConvertTime(newStartTime) <= ConvertTime(s.startTime) &&
                    ConvertTime(newStartTime) <= ConvertTime(s.endTime) &&
                    ConvertTime(newEndTime) >= ConvertTime(s.startTime) &&
                    ConvertTime(newEndTime) >= ConvertTime(s.endTime))
                {
                    passed = false;
                }

                if (ConvertTime(newStartTime) >= ConvertTime(s.startTime) &&
                    ConvertTime(newStartTime) <= ConvertTime(s.endTime) &&
                    ConvertTime(newEndTime) >= ConvertTime(s.startTime) &&
                    ConvertTime(newEndTime) <= ConvertTime(s.endTime))
                {
                    passed = false;
                }
            }
        }
        return passed;
    }

    private void AddNewSubtitle(string time1, string time2)
    {
        var temp = new SubtitleEntry(time1, time2, TextBoxScript.value);
        SubList.Add(temp);
        UpdateMainFile();
        UpdateListGui();
        var tempVal = _higherSlider.value + 0.01f;

        _higherSlider.value += 0.1f;
        BFSRSwitchController.Instance.AddSliderToReplay(_higherSlider);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(EndTimeScript);

        
        _lowerSlider.value = tempVal;
        BFSRSwitchController.Instance.AddSliderToReplay(_lowerSlider);
        BFSRSwitchController.Instance.AddTimerLabelToReplay(StartTimeScript);

        TextBoxScript.value = "";
    }

    public void OnCreateClick()
    {
        UpdateMainFile();
        LoadSubFile(_continuousVars.LoadedVideo.VttPath);
    }

    public void Update()
    {
        SubCurrentDisScript.text = "";
        DisplayCurrentSubtitle(GetCurrentTime());
    }

    private int GetCurrentTime()
    {
        var tempRead = "00:" + ReadTimeScript.text;
        tempRead = ReplaceAt(tempRead, 8, ',');
        var currentTime = ConvertTime(tempRead);
        return currentTime;
    }

    private void ConvertMainFileToChunks()
    {
        var startChar = 0;

        if (_mainFile != null)
        {
            for (var i = 0; i < _mainFile.Length; i++)
            {
                i = DecypherChar(i, ref startChar);
            }
        }
    }

    private int DecypherChar(int i, ref int startChar)
    {
        if ((i + 2) < _mainFile.Length)
        {
            i = AddChunk(i, ref startChar);
        }
        else
        {
            AddFinalChunk(i, startChar);
        }
        return i;
    }

    private void AddFinalChunk(int i, int startChar)
    {
        var endChar = i;
        if (endChar - startChar > 29)
            _chunks.Add(_mainFile.Substring(startChar, endChar - startChar));
    }

    private int AddChunk(int i, ref int startChar)
    {
        if (!_mainFile[i].Equals('\n') || !_mainFile[i + 2].Equals('\n')) return i;

        var endChar = i - 1;
        if (endChar - startChar > 29)
        {
            _chunks.Add(_mainFile.Substring(startChar, endChar - startChar));
        }
        i += 2;
        startChar = i;
        startChar++;
        return i;
    }

    //take current time and display current subtitle
    private void DisplayCurrentSubtitle(int currentTime)
    {
        foreach (var s in SubList)
        {
            if (ConvertTime(s.startTime) <= currentTime)
            {
                if (ConvertTime(s.endTime) >= currentTime)
                    SubCurrentDisScript.text = s.Content;
            }
        }
    }

    //converts mainfile into subtitle entries in sublist.
    public void PopulateList()
    {
        SubList.Clear();
        _chunks.Clear();
        ConvertMainFileToChunks();
        CreateSubList();

        if (Grid != null)
            UpdateListGui();
    }

    private void CreateSubList()
    {
        foreach (var s in _chunks)
        {
            var startTime = s.Substring(3, 12);
            var endTime = s.Substring(20, 12);
            var content = s.Substring(34);
            SubList.Add(new SubtitleEntry(startTime, endTime, content));
        }
    }

    private static int ConvertTime(string badTime)
    {
        if (badTime.Length < 12)
        {
            return 0;
        }
        
        var goodTime = 0;
        goodTime += Convert.ToInt32(badTime.Substring(0, 2)) * 10000000;
        goodTime += Convert.ToInt32(badTime.Substring(3, 2)) * 100000;
        goodTime += Convert.ToInt32(badTime.Substring(6, 2)) * 1000;
        goodTime += Convert.ToInt32(badTime.Substring(9, 3));
        return goodTime;
    }
    private void UpdateMainFile()
    {
        _mainFile = "";
        _subCounter = 0;
        foreach (var s in SubList)
        {
            AddSubListSectionToMainFile(s);
        }
        Debug.Log("Trying to write vtt to file.");
        var savePath =
        Path.ChangeExtension(_continuousVars.LoadedVideo.XmlPath, "vtt");
        System.IO.File.WriteAllText(savePath, _mainFile);
        _continuousVars.LoadedVideo.UpdateVideo(AnimationModel.AnimationCaptions, savePath);
        _continuousVars.LoadedVideo.AnimationId = "";
        _continuousVars.LoadedVideo.Public = false;
        _continuousVars.LoadedVideo.Private = false;
        _continuousVars.LoadedVideo.UpdateVideo();
    }

    private void AddSubListSectionToMainFile(SubtitleEntry s)
    {
        _mainFile = string.Concat(_mainFile,
            _subCounter + Environment.NewLine + s.startTime + " --> " + s.endTime + Environment.NewLine + s.Content +
            Environment.NewLine + Environment.NewLine);
        _subCounter++;
    }

    //clears gui grid and repopulates with gui objects
    public void UpdateListGui()
    {
        foreach (Transform child in Grid.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var s in SubList)
        {
            var sTemp = s.startTime.Replace(',', ':');
            sTemp = sTemp.Remove(0, 3);

            var eTemp = s.endTime.Replace(',', ':');
            eTemp = eTemp.Remove(0, 3);

            var ctemp = s.Content;
            if (ctemp.Length > 17)
            {
                ctemp = ctemp.Substring(0, 15) + "...";
            }

            CreateNewGuiObject(s, sTemp, eTemp, ctemp);
        }
    }

    private void CreateNewGuiObject(SubtitleEntry s, string sTemp, string eTemp, string ctemp)
    {
        var newSubEntryPre = Instantiate(SubEntryPre, Grid.transform.position, Grid.transform.rotation) as GameObject;

        if (newSubEntryPre == null) return;
        newSubEntryPre.GetComponent<SubDataStore>().UpdateData(s.startTime, s.endTime, s.Content);
        int stTemp = Convert.ToInt32(s.startTime.Substring(6, 2)) + (Convert.ToInt32(s.startTime.Substring(3, 2)) * 60);
        int etTemp = Convert.ToInt32(s.endTime.Substring(6, 2)) + (Convert.ToInt32(s.endTime.Substring(3, 2)) * 60);

        
        Debug.Log("Total Time : " + (etTemp-stTemp));
        newSubEntryPre.GetComponentInChildren<UILabel>().text = sTemp.Substring(0, 5) + "-" + eTemp.Substring(0, 5) + "(" + (etTemp-stTemp) + ")" + ctemp;
        newSubEntryPre.transform.parent = Grid.transform;
        newSubEntryPre.transform.localScale = new Vector3(1, 1, 1);
        newSubEntryPre.transform.name = sTemp;
    }

    public void DeleteSubtitle(GameObject par)
    {
        var dataStore = par.GetComponent<SubDataStore>();

        foreach (var s in SubList.Where( s => s.startTime == dataStore.STime || s.endTime == dataStore.ETime || s.Content == dataStore.Content))
        {
            if(s == _editingSub)
            {
                if(isEditing)
                {
                    isEditing = false;
                    _editingSub = null;
                    SetAddSubButtonToAdd();
                }
            }
            SubList.Remove(s);
            Destroy(par);
            break;
        }

        UpdateMainFile();
    }

    //Loads a vtt file into the main file.
    public void LoadSubFile(string path)
    {
        if (File.Exists(path))
        {
            using (var reader = new StreamReader(path))
            {
                _mainFile = reader.ReadToEnd();
            }
        }
        else
        {
            _mainFile = "";
        }

        PopulateList();
    }

    public string ReplaceAt(string input, int index, char newChar)
    {
        if (input == null)
        {
            throw new ArgumentNullException("input");
        }
        var chars = input.ToCharArray();
        chars[index] = newChar;
        return new string(chars);
    }

    private string CleanTime(string time)
    {
        var temp = ReplaceAt(time, 5, ',');
        return "00:" + temp;
    }

    public void ClearList()
    {
        SubList.Clear();
        LoadSubFile(_continuousVars.LoadedVideo.VttPath);
    }

    public bool SubsExist()
    {
        return _mainFile.Length > 10;
    }

    //Subtitle entry that we use to do calculations.
    public class SubtitleEntry
    {
        public string Content;
        public string endTime;
        public string startTime;

        public SubtitleEntry(string startTimeTemp, string endTimeTemp, string contentTemp)
        {
            startTime = startTimeTemp;
            endTime = endTimeTemp;
            Content = contentTemp;
        }
    }

    public void EditSubtitle(GameObject go)
    {
        isEditing = true;

        SetAddSubButtonToChange();

        var dataStore = go.GetComponent<SubDataStore>();

        foreach (var s in SubList.Where(s => s.startTime == dataStore.STime || s.endTime == dataStore.ETime || s.Content == dataStore.Content))
        {
            _editingSub = s;
        }

        TextBoxScript.value = _editingSub.Content;

        string tempstr = _editingSub.startTime.Substring(_editingSub.startTime.IndexOf(','));

        _sliderLimiterLow.enabled = false;
        _sliderLimiterHigh.enabled = false;
        _createNotification.CreateLoadingNotification("Please wait...");
        LoadingNotifInitiliser.IsBusy = true;
        BFSRSwitchController.Instance.GetSliderValue(_editingSub.startTime.Replace(',', ':'), _editingSub.endTime.Replace(',', ':'), LabelSliderCallback);  
        
        _sliderLimiterLow.enabled = true;
        _sliderLimiterHigh.enabled = true;
    }

    void SetAddSubButtonToChange()
    {
        _AddSubLabel.text = "Commit Changes";
    }

    void SetAddSubButtonToAdd()
    {
        _AddSubLabel.text = "Add New Subtitle";
    }

    public void LabelSliderCallback(float lowerSlider, string lowerLabel,float higherSlider,string higherLabel)
    {
        Debug.Log(string.Format("LSV : {0} LBT :{1} HSV : {2} HLT : {3} ",lowerSlider,lowerLabel,higherSlider,higherLabel));
        _higherSlider.value = higherSlider;
        EndTimeScript.text=higherLabel;
        _lowerSlider.value=lowerSlider;
        StartTimeScript.text=lowerLabel;
        LoadingNotifInitiliser.IsBusy = false;
    }
}