﻿using UnityEngine;
using Assets.DBHelper;
using System.Linq;
using System.Collections;
using System;
using System.Collections.Generic;
using Assets.SynchroniseScript;
public class PopulateSyncList : MonoBehaviour
{

    public GameObject Child;
    public GameObject UIroot;
    public GameObject Controller;
    private List<Video> videos;
    private ContinuousVars _continuousVars;
    // Use this for initialization
    void OnEnable()
    {
        _continuousVars = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
        _continuousVars.SyncCount = 0;
        StartCoroutine(CreateList());
        StartCoroutine(ChangeStatus());
        StartCoroutine(GetSyncCount(UpdateLabel));
    }

    void OnDisable()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    private IEnumerator CreateList()
    {
        yield return null;
        UIroot = GameObject.Find("UI Root");
        videos = new Video().GetAllVideos(" Status IN ( '" + Status.Display.ToString() + "','" + Status.Viewed.ToString() + "')").ToList();
        foreach (var video in videos)
        {
            NGUITools.AddChild(gameObject, Child);
        } 
    }
    private IEnumerator ChangeStatus()
    {
        yield return null;
        var i = 0;
        foreach (SyncItemInit syncItem in UIroot.GetComponentsInChildren<SyncItemInit>())
        {

            syncItem.SetName(videos[i].Name);
            syncItem.Id = videos[i].Id;
            if (videos[i].Status == Status.Display)
            {
                ChangeStatus(videos[i], Status.Viewed);
            }
            i++;
        }

    }
    private IEnumerator GetSyncCount(Action<int> updateCount)
    {
        yield return null;
        var count = 0;
        updateCount(count);
        
    }
    private void UpdateLabel(int updateCount)
    {
        SynchroniseSingleton.SyncCountLabel.text = "" + updateCount;
    }
    public void DownloadClick()
    {
        _continuousVars.StartDownload(GetSelectedVideo(), videos.ToArray());
        Controller.GetComponent<BottomBarController>().DisableSync();
    }
    private void ChangeStatus(Video video, Status status)
    {
        video.Status = status;
        video.UpdateVideo();
    }
    private IEnumerable<string> GetSelectedVideo()
    {
        return UIroot.GetComponentsInChildren<SyncItemInit>().Where(x => x.IsChecked == true).Select(x => x.Id);
    }
}