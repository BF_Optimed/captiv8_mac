﻿using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class boxSelect : MonoBehaviour
{
    private bool clickFlag = false;
    private List<VectorLine> LineArray;
    public Material lineMaterial;
    private BFSDLineWidth LineWidth;
    private int oldWidth;
    private Vector2 originalPos;
    private ColorSelection SelectColor;
    private VectorLine selectionLine;
    public float textureScale;

    public GameObject BtnController;
    private BtnController _btnController;

    private void Start()
    {
        _btnController = BtnController.GetComponent<BtnController>();
        LineWidth = BtnController.GetComponent<BFSDLineWidth>();
        SelectColor = BtnController.GetComponent<ColorSelection>();
        selectionLine = new VectorLine("Selection", new Vector2[5], lineMaterial, 4.0f, LineType.Continuous);
        lineMaterial.color = SelectColor.GetCurrentColor();
        selectionLine.depth = _btnController.DepthCount;
        _btnController.DepthCount++;
        selectionLine.textureScale = textureScale;
        oldWidth = Screen.width;
        LineArray = new List<VectorLine>();
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            originalPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            if (ReferenceEquals(null, selectionLine))
            {
                
                selectionLine = new VectorLine("Selection", new Vector2[5], lineMaterial, LineWidth.GetCurrentLineWidth(),
                    LineType.Continuous);
                lineMaterial.color = SelectColor.GetCurrentColor();
                selectionLine.depth = _btnController.DepthCount;
            }
            selectionLine.MakeRect(originalPos, Input.mousePosition);
            selectionLine.lineWidth = LineWidth.GetCurrentLineWidth();
            selectionLine.material.color = SelectColor.GetCurrentColor();
            selectionLine.Draw();
        }

        if (Input.GetMouseButtonUp(0))
        {
            LineArray.Add(selectionLine);
            selectionLine = null;
           _btnController.DepthCount++;
            Debug.Log(_btnController.DepthCount);
            //Add Depthcount here and do selectionLine.Depth = (call the function with updated incremenented depth)
        }

        if (oldWidth != Screen.width)
        {
            oldWidth = Screen.width;
            VectorLine.SetCamera();
        }


        if (Input.GetMouseButtonDown(1))
        {
            var countArray = LineArray.Count;
            print(countArray);
            if (countArray != 0)
            {
                var newLineRefrence = LineArray[countArray - 1];
                if (newLineRefrence != null)
                {
                    VectorLine.Destroy(ref newLineRefrence);
                    LineArray.RemoveAt(countArray - 1);
                }
            }
        }
    }

    public void deleteArray()
    {
        if (LineArray != null)
            VectorLine.Destroy(LineArray);
    }
}