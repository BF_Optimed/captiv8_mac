﻿using Assets.DBHelper;
using Assets.Share;
using System;
using System.IO;
using UnityEngine;
using System.Linq;
using Assets.Scripts;
using Assets.SynchroniseScript;
using System.Collections;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Assets.OptimedClient;
public class TvLibraryGuiController : MonoBehaviour
{

    public GameObject GoShareName;
    public GameObject GoShareLastName;
    public GameObject GoShareTitle;
    public GameObject GoShareEmail;
    public GameObject GoShareDesc;
    public GameObject ButtonIcon;

    private UIInput _inputLastname;
    private UIInput _inputTitle;
    private UIInput _inputName;
    private UIInput _inputEmail;
    private UIInput _inputDesc;

    public GameObject PrivateShareGameObject;

    public string PathVar;
    private Video video;
    public GameObject Controller;
    private VideoPlayController _videoPlayController;
    private GameObject _uploading;
    public GameObject CurrentCatLabel;
    private UILabel _currentCateLabel;

    public GameObject TvVideoList;
    private BFSGTVVideoList _tvVideoList;


    void OnEnable()
    {
        SynchroniseSingleton.Instance.DownloadingCompleteEvent += Instance_DownloadingCompleteEvent;
        if (Application.loadedLevelName != "TVLibrary")
        {
            _videoPlayController = Controller.GetComponent<VideoPlayController>();
        }

        if(Application.loadedLevelName == "TVLibrary")
            _currentCateLabel = CurrentCatLabel.GetComponent<UILabel>();
        
        _tvVideoList = TvVideoList.GetComponent<BFSGTVVideoList>();
        _uploading = GameObject.FindGameObjectWithTag("Upload");
        _inputName = GoShareName.GetComponent<UIInput>();
        _inputEmail = GoShareEmail.GetComponent<UIInput>();
        _inputDesc = GoShareDesc.GetComponent<UIInput>();
        _inputLastname = GoShareLastName.GetComponent<UIInput>();
        _inputTitle = GoShareTitle.GetComponent<UIInput>();
        gameObject.GetComponent<WalkCreator>().CreateWalkthrough(MagicStrings.ShareTitle, MagicStrings.ShareContent, MagicStrings.ShareKey, MagicStrings.ShareIcon);
    }

    void OnDisable()
    {
        SynchroniseSingleton.Instance.DownloadingCompleteEvent -= Instance_DownloadingCompleteEvent;
    }

    void Instance_DownloadingCompleteEvent()
    {
        if(Application.loadedLevelName == "TVLibrary")
            UpdateList();
        //throw new NotImplementedException();
    }

    public void ActivatePrivateShare()
    {
        NGUITools.SetActive(PrivateShareGameObject, true);
    }

    public void PrivateShare()
    {
        GameObject.Find("PersistentController").GetComponent<Hibernation>().CanHibernate = false;
        PathVar = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideoDataPath;
        video = new Video().GetVideo(PathVar);
        if(_inputTitle.value == null || _inputName.value == null || _inputLastname.value == null || _inputEmail.value == null || _inputDesc.value == null)
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "All of the fields must be complete, please try again.", "Ok");
        }
        
        else
        {
            if (!ValidateEmail(_inputEmail.value))
            {
                gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "Please enter the valid email address.", "Ok");
                return;
            }
            string desc = _inputDesc.value.Trim();
            string title = _inputTitle.value.Trim();
            string name = _inputName.value.Trim();
            string lastName = _inputLastname.value.Trim();
            string email = _inputEmail.value.Trim();

            gameObject.GetComponent<CreateNotification>().CreateLoadingNotification("Sharing in progress...");
            if (video.Owner == Owner.captiv8plus)
            {
                StartCoroutine(Captiv8PlusShare(video, title, name, lastName));
            }
            else
            {
                StartCoroutine(Captiv8Share(video, title, name, lastName));
            }
            Debug.Log("Finishing private share.");
        }
    }
    public bool ValidateEmail(string emailaddress)
    {
        try
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(emailaddress);
            if (match.Success)
                return true;
            else
                return false;
        }
        catch (FormatException)
        {
            Debug.Log("not valid email");
            return false;
        }
    }
    private IEnumerator Captiv8Share(Video video, string toTitle, string toFirstname, string toLastName)
    {
        yield return null;
        var capti8Share = new ShareCaptiv8(video,Assets.SynchroniseScript.SynchroniseSingleton.Instance.CmsURL);
        NGUITools.SetActive(_uploading, true);
        capti8Share.PostPrivateAnimation(InstallationId.GetInstallationId, _inputEmail.value, toTitle, toFirstname, toLastName, _inputDesc.value, ShareResponse);    
    }

    private IEnumerator Captiv8PlusShare(Video video, string toTitle, string toFirstname, string toLastName)
    {
        yield return null;
        var shareRequest = new ShareRequestHelper(video);
        var restRequest = shareRequest.CreatePrivateRequest(toTitle, toFirstname, toLastName, _inputEmail.value, _inputDesc.value);
        NGUITools.SetActive(_uploading, true);
        shareRequest.PostShareRequest(restRequest, ShareResponse);
    }
    public void ShareResponse(ShareService.Contracts.ShareResponse response)
    {
        Destroy(GameObject.FindGameObjectWithTag("LoadingNotif"));
        GameObject.Find("PersistentController").GetComponent<Hibernation>().CanHibernate = true;
        if(response.ErrorMessage=="Done")
        {
            video.Private = true;
            video.UpdateVideo();
            UpdateList();
            Controller.GetComponent<CreateNotification>().CreateOkayButton("Share Successful", "You have sucessfully shared animation", "Ok");
            return;
        }
        if (response.Error)
        {
            Controller.GetComponent<CreateNotification>().CreateOkayButton("Error", response.ErrorMessage, "Ok");
            Debug.Log(response.ErrorMessage);
        }
        else
        {
            video.AnimationId = response.Animations.AnimationId;
            video.Private = true;
            video.UpdateVideo();
            //SEAN PUT TRIANGE CODE HERE
            Controller.GetComponent<CreateNotification>().CreateOkayButton("Share Successful", "You have sucessfully shared animation", "Ok");
            UpdateList();
        }
    }
    public void ArrowSpin()
    {
        ButtonIcon.GetComponent<TweenRotation>().PlayForward();
    }

    public void UpdateList()
    {
        if (Application.loadedLevelName != "TVLibrary")
        {
            NGUITools.SetActive(TvVideoList, false);
            NGUITools.SetActive(TvVideoList, true);
            return;
        }

        Debug.Log("Category text is : " + _currentCateLabel.text);
        switch(_currentCateLabel.text)
        {
            case "All":
                _tvVideoList.GetListFromArray("", 2);
                break;
            case "Category":
                _tvVideoList.GetListFromArray("", 2);
                break;
            case "Public":
                _tvVideoList.GetListFromArray("", 0);
                break;
            case "Private":
                _tvVideoList.GetListFromArray("", 1);
                break;
            default:
                _tvVideoList.GetListFromCategory(_currentCateLabel.text);
                break;

        }
    }
}