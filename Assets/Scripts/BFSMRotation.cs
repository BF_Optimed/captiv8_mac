﻿using UnityEngine;

public class BFSMRotation : MonoBehaviour
{
    private Quaternion _currentRotation;
    private Quaternion _desiredRotation;
    public BFSMPan _panScript;
    private Vector3 _savedPos;
    private Quaternion _savedQuat;
    public float _xDeg;
    public float _yDeg;
    public float MaxDistance = 20;
    public float MinDistance = .6f;
    public float PanSpeed = 0.3f;
    public Transform Target;
    public int XMaxLimit = 360;
    public int XMinLimit = -360;
    public float XSpeed = 200.0f;
    public float XTouchSpeed = 20.0f;
    public int YMaxLimit = 80;
    public int YMinLimit = -80;
    public float YSpeed = 200.0f;
    public float YTouchSpeed = 20.0f;
    public float ZoomDampening = 5.0f;
    public int ZoomRate = 40;
    public bool InputAllowed { get; set; }
    public bool MovementAllowed { get; set; }
    public Quaternion Rotation { get; set; }
    public Vector3 Position { get; set; }
    public Vector3 PositionNew { get; set; }
    public float CurrentDistance { get; set; }
    public Vector3 TargetOffset;
    public GameObject Blackout;
    public BFSUTransition _bfsuTransition;

    private void Start()
    {
        _bfsuTransition = Blackout.GetComponent<BFSUTransition>();
        _panScript = Camera.main.GetComponent<BFSMPan>();
        InputAllowed = true;
        Init();
    }

    public void Init()
    {
        _yDeg = 0.0f;
        switch(_bfsuTransition.CurrentObj.name)
        {
            case "Overview":
                _xDeg = 250.0f;
                break;
            case "Adnexa":
                _xDeg = -160.0f;
                break;
            case "Ciliary Body":
                _xDeg = -130.0f;
                _yDeg = 25.0f;
                break;
            case "Anterior Segment":
                _xDeg = -130.0f;
                break;
            case "Macula":
                _xDeg = 180.0f;
                _yDeg = 25.0f;
                break;
            case "Optic Nerve":
                _xDeg = 250.0f;
                break;
            case "Vision Simulator":
                _xDeg = -150.0f;
                _yDeg = 6.0f;
                break;
            default:
                _xDeg = 0.0f;
                break;
        }
        
        
        if (_bfsuTransition.CurrentObj.name == "Vision Simulator")
        {
            CurrentDistance = 0.0f;
        }
        else
        {
            CurrentDistance = 10.0f;
        }
        _yDeg = ClampAngle(_yDeg, YMinLimit, YMaxLimit);
        _xDeg = ClampAngle(_xDeg, XMinLimit, XMaxLimit);
        TargetOffset = Vector3.zero;
        _panScript.DesiredOffset = Vector3.zero;
    }

    private void LateUpdate()
    {
        if (MovementAllowed && InputAllowed)
        {
            if ((Input.GetMouseButton(0) && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))))
            {
                UpdateDegrees();
            }
        }
        UpdateRotation();
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        PositionNew = (new Vector3(0.0f, 0.0f, -CurrentDistance) + TargetOffset);
        Position = Rotation*PositionNew + Target.position;
        transform.position = Position;
    }

    private void UpdateRotation()
    {
        _desiredRotation = Quaternion.Euler(_yDeg, _xDeg, 0);
        _currentRotation = transform.rotation;
        Rotation = Quaternion.Lerp(_currentRotation, _desiredRotation, Time.deltaTime*ZoomDampening);
        transform.rotation = Rotation;
    }

    private void UpdateDegrees()
    {
        _xDeg += Input.GetAxis("Mouse X")*XSpeed*0.02f;
        _yDeg -= Input.GetAxis("Mouse Y")*YSpeed*0.02f;
        _yDeg = ClampAngle(_yDeg, YMinLimit, YMaxLimit);
        _xDeg = ClampAngle(_xDeg, XMinLimit, XMaxLimit);
    }

    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}