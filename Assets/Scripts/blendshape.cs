﻿using UnityEngine;

public class blendshape : MonoBehaviour
{
    public float Blink;
    public float cilliary_reduce_slider;
    public float cilliary_thin_slider;
    public float Pupil;
    private SkinnedMeshRenderer skinMeshRenderer;

    private void Start()
    {
        skinMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    private void OnGUI()
    {
        Pupil = GUI.HorizontalSlider(new Rect(25, 25, 100, 30), Pupil, 0.0f, 100.0f);
        GUI.Label(new Rect(25, 5, 100, 20), "Pupil");

        Blink = GUI.HorizontalSlider(new Rect(25, 65, 100, 30), Blink, 0.0f, 100.0f);
        GUI.Label(new Rect(25, 45, 100, 20), "Blink");

        cilliary_thin_slider = GUI.HorizontalSlider(new Rect(25, 105, 100, 30), cilliary_thin_slider, 0.0f, 100.0f);
        GUI.Label(new Rect(25, 85, 100, 20), "Blank1");

        cilliary_reduce_slider = GUI.HorizontalSlider(new Rect(25, 145, 100, 30), cilliary_reduce_slider, 0.0f, 100.0f);
        GUI.Label(new Rect(25, 125, 100, 20), "Blank2");
    }

    private void Update()
    {
        skinMeshRenderer.SetBlendShapeWeight(0, Pupil);
        skinMeshRenderer.SetBlendShapeWeight(1, Blink);
        skinMeshRenderer.SetBlendShapeWeight(2, cilliary_thin_slider);
        skinMeshRenderer.SetBlendShapeWeight(3, cilliary_reduce_slider);
    }
}