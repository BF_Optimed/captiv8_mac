﻿using UnityEngine;
using System.Collections;

public class BFSMCamZoom : MonoBehaviour
{

    public int zoomSpeed = 250;
    public float zoomMin = 0.1f;
    public float zoomMax = 100f;
    public GameObject Target;

    public float DesiredDistance = 0.0f;
    private float currentDistance = 0.0f;
    private float zoomDampening = 5.0f;
    private float distance;

    public GameObject Controller;
    private BtnController _btnController;

    // Use this for initialization
    void Start()
    {
        _btnController = Controller.GetComponent<BtnController>();
        DesiredDistance = camera.fieldOfView;
        currentDistance = camera.fieldOfView;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(!_btnController.Autopiloting)
        {
            //If Right mouse button is pressed
            if (Input.GetMouseButton(1) && !(Input.GetKey(KeyCode.LeftShift)))
            {
                //update the desired distance, set the current, and lerp between them
                DesiredDistance -= Input.GetAxis("Mouse Y") * zoomSpeed * 0.1f;
                DesiredDistance = Mathf.Clamp(DesiredDistance, zoomMin, zoomMax);
                currentDistance = Mathf.Lerp(currentDistance, DesiredDistance, Time.deltaTime * zoomDampening);
                camera.fieldOfView = currentDistance;

            }
            else
            {
                distance = camera.fieldOfView;
                // affect the desired Zoom distance if we roll the scrollwheel
                DesiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomSpeed * Mathf.Abs(distance);
                //clamp the zoom min/max
                DesiredDistance = Mathf.Clamp(DesiredDistance, zoomMin, zoomMax);
                // For smoothing of the zoom, lerp distance
                currentDistance = Mathf.Lerp(currentDistance, DesiredDistance, Time.deltaTime * zoomDampening);
                camera.fieldOfView = currentDistance;
            }
        }
    }


    void OnEnable()
    {
        currentDistance = DesiredDistance = Camera.main.fieldOfView;
    }

    public void SetDesiredDistance(float f)
    {
        DesiredDistance = f;
    }
}
