﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Assets.OptimedClient;

public class SettingsBtnController : MonoBehaviour 
{
    public GameObject PersonalInfo;
    public GameObject Drawing;
    public GameObject Display;
    public GameObject Applications;
    public GameObject Information;
    public GameObject Reset;
    public GameObject NameEditArrow;
    public GameObject DrawArrow;
    public GameObject DisplayArrow;
    public GameObject ApplicationArrow;
    public GameObject InformationArrow;
    public GameObject ResetArrow;
    public GameObject PersonalBtn;
    public GameObject DrawingBtn;
    public GameObject DisplayBtn;
    public GameObject ApplicationBtn;
    public GameObject InformationBtn;
    public GameObject ResetBtn;
    public GameObject Relese;
    private UISprite _personalInfoSprite;
    private UISprite _drawingSprite;
    private UISprite _displaySprite;
    private UISprite _applicationSprite;
    private UISprite _informationSprite;
    private UISprite _nameeditArrowSprite;
    private UISprite _displayArrowSprite;
    private UISprite _drawArrowSprite;
    private UISprite _applicationArrowSprite;
    private UISprite _informationArrowSprite;
    private UILabel _personalinfoLabel;
    private UILabel _drawingLabel;
    private UILabel _displayLabel;
    private UILabel _applicationLabel;
    private UILabel _informationLabel;

    public GameObject PinToggle;
    private UIToggle _pinToggle;
    public GameObject SliderToggle;
    private UIToggle _sliderToggle;
    public GameObject FullscreenToggle;
    private UIToggle _fullscreenToggle;

    public GameObject QualityDropDown;
    private UIPopupList _qualityDropDown;
    private string _qualitySetting = "";

    public GameObject ResolutionDropDown;
    private UIPopupList _ResolutionDropDown;
    private string _ResolutionSetting = "";

    public GameObject HibernationDropDown;
    private UIPopupList _hibernationDropDown;
    private string _hibernationSetting = "";

    public string DefaultColour;
    public string DefaultWidth;

    public GameObject Title;
    public GameObject Name;
    public GameObject Email;
    public GameObject LastName;

    private UIInput _title;
    private UIInput _name;
    private UIInput _email;
    private UIInput _lastName;
    private UIInput _relese;

    public GameObject Fullscreen;
    private UIToggle _fullscreen;

    private String _installationId;


    public void Start() 
    {
        _installationId = InstallationId.GetInstallationId;
        _personalInfoSprite = PersonalBtn.GetComponent<UISprite>();
        _drawingSprite = DrawingBtn.GetComponent<UISprite>();
        _displaySprite = DisplayBtn.GetComponent<UISprite>();
        _applicationSprite = ApplicationBtn.GetComponent<UISprite>();
        _informationSprite = InformationBtn.GetComponent<UISprite>();
        _personalinfoLabel = PersonalBtn.GetComponentInChildren<UILabel>();
        _drawingLabel = DrawingBtn.GetComponentInChildren<UILabel>();
        _displayLabel = DisplayBtn.GetComponentInChildren<UILabel>();
        _applicationLabel = ApplicationBtn.GetComponentInChildren<UILabel>();
        _informationLabel = InformationBtn.GetComponentInChildren<UILabel>();
        _relese = Relese.GetComponent<UIInput>();
        _nameeditArrowSprite = NameEditArrow.GetComponent<UISprite>();
        _displayArrowSprite = DisplayArrow.GetComponent<UISprite>();
        _drawArrowSprite = DrawArrow.GetComponent<UISprite>();
        _applicationArrowSprite = ApplicationArrow.GetComponent<UISprite>();
        _informationArrowSprite = InformationArrow.GetComponent<UISprite>();
        _pinToggle = PinToggle.GetComponent<UIToggle>();
        _sliderToggle = SliderToggle.GetComponent<UIToggle>();
        _fullscreenToggle = FullscreenToggle.GetComponent<UIToggle>();
        _qualityDropDown = QualityDropDown.GetComponent<UIPopupList>();
        _ResolutionDropDown = ResolutionDropDown.GetComponent<UIPopupList>();
        _hibernationDropDown = HibernationDropDown.GetComponent<UIPopupList>();
        _fullscreen = Fullscreen.GetComponent<UIToggle>();

        _title = Title.GetComponent<UIInput>();
        _name = Name.GetComponent<UIInput>();
        _email = Email.GetComponent<UIInput>();
        _lastName = LastName.GetComponent<UIInput>();

        PersonalInfoClick();

        if (PlayerPrefs.GetInt(MagicStrings.PinOptionKey) == 1)
        {
            _pinToggle.value = true;
        }
        else
        {
            _pinToggle.value = false;
        }
        //if (PlayerPrefs.GetInt(MagicStrings.FullscreenOptionKey) == 1)
        //{
        //    _fullscreen.value = true;
        //}
        //else
        //{
        //    _fullscreen.value = false;
        //}
        if (PlayerPrefs.GetInt(MagicStrings.SliderOptionKey) == 1)
        {
            _sliderToggle.value = true;
        }
        else
        {
            _sliderToggle.value = false;
        }
        if (PlayerPrefs.GetInt(MagicStrings.FullscreenOptionKey) == 1)
        {
            _fullscreenToggle.value = true;
        }
        else
        {
            _fullscreenToggle.value = false;
        }

        switch (QualitySettings.GetQualityLevel())
        {
            case 0:
                _qualityDropDown.value = "Low";
                break;
            case 1:
                 _qualityDropDown.value = "Medium";
                break;
            case 2:
                 _qualityDropDown.value = "High";
                break;
        }

        switch (PlayerPrefs.GetInt(MagicStrings.HibernationTimeKey))
        {
            case int.MaxValue:
                _hibernationDropDown.value = "Never";
                break;
            case 30:
                _hibernationDropDown.value = "30 Seconds";
                break;
            case 60:
                _hibernationDropDown.value = "1 Minute";
                break;
            case 300:
                _hibernationDropDown.value = "5 Minutes";
                break;
            case 600:
                _hibernationDropDown.value = "10 Minutes";
                break;
        }

        //Change
        _ResolutionDropDown.value = PlayerPrefs.GetInt(MagicStrings.ResolutionXKey).ToString() + " x " + PlayerPrefs.GetInt(MagicStrings.ResolutionYKey).ToString();

        if (BFSSecurePlayerData.HasKey(MagicStrings.ConTitle))
        {
            _title.value = BFSSecurePlayerData.GetString(MagicStrings.ConTitle, SystemInfo.deviceUniqueIdentifier);
        }

        if (BFSSecurePlayerData.HasKey(MagicStrings.ConName))
        {
            _name.value = BFSSecurePlayerData.GetString(MagicStrings.ConName, SystemInfo.deviceUniqueIdentifier);
        }

        if (BFSSecurePlayerData.HasKey(MagicStrings.ConEmail))
        {
            _email.value = BFSSecurePlayerData.GetString(MagicStrings.ConEmail, SystemInfo.deviceUniqueIdentifier);
        }

        if (BFSSecurePlayerData.HasKey(MagicStrings.ConLastName))
        {
            _lastName.value = BFSSecurePlayerData.GetString(MagicStrings.ConLastName, SystemInfo.deviceUniqueIdentifier);
        }

        Debug.Log("Quality setting number is : " + QualitySettings.GetQualityLevel());
        Debug.Log("Hibernation setting number is : " + _hibernationDropDown.value);
        //Debug.Log("Resolution has been set to");

    }

    private void Update() 
    {
        _relese.value = _installationId;
    }

    private void DisableOthers() 
    {
        NGUITools.SetActive(PersonalInfo, false);
        NGUITools.SetActive(Drawing, false);
        NGUITools.SetActive(Display, false);
        NGUITools.SetActive(Applications, false);
        NGUITools.SetActive(Information, false);
        Debug.Log(MagicStrings.SettingsDeactive);
        Debug.Log(_personalInfoSprite);
        _personalInfoSprite.spriteName = MagicStrings.SettingsDeactive;
        _drawingSprite.spriteName = MagicStrings.SettingsDeactive;
        _displaySprite.spriteName = MagicStrings.SettingsDeactive;
        _applicationSprite.spriteName = MagicStrings.SettingsDeactive;
        _informationSprite.spriteName = MagicStrings.SettingsDeactive;
        _personalinfoLabel.color = Color.black;
        _drawingLabel.color = Color.black;
        _displayLabel.color = Color.black;
        _applicationLabel.color = Color.black;
        _informationLabel.color = Color.black;
        _nameeditArrowSprite.spriteName = MagicStrings.ArrowDeactive;
        _displayArrowSprite.spriteName = MagicStrings.ArrowDeactive;
        _drawArrowSprite.spriteName = MagicStrings.ArrowDeactive;
        _applicationArrowSprite.spriteName = MagicStrings.ArrowDeactive;
        _informationArrowSprite.spriteName = MagicStrings.ArrowDeactive;
    }

    public void PersonalInfoClick() 
    {
        DisableOthers();
        NGUITools.SetActive(PersonalInfo,true);
        _personalinfoLabel.color = Color.white;
        _personalInfoSprite.spriteName = MagicStrings.SettingsActive;
        _nameeditArrowSprite.spriteName = MagicStrings.ArrowActive;
    }

    public void DrawingClick() 
    {
        DisableOthers();
        NGUITools.SetActive(Drawing, true);
        _drawingLabel.color = Color.white;
        _drawingSprite.spriteName = MagicStrings.SettingsActive;
        _drawArrowSprite.spriteName = MagicStrings.ArrowActive;
    }

    public void DisplayClick() 
    {
        DisableOthers();
        NGUITools.SetActive(Display, true);
        _displayLabel.color = Color.white;
        _displaySprite.spriteName = MagicStrings.SettingsActive;
        _displayArrowSprite.spriteName = MagicStrings.ArrowActive;
    }

    public void ApplicationClick()
    {
        DisableOthers();
        NGUITools.SetActive(Applications, true);
        _applicationLabel.color = Color.white;
        _applicationSprite.spriteName = MagicStrings.SettingsActive;
        _applicationArrowSprite.spriteName = MagicStrings.ArrowActive;
    }

    public void InformationClick()
    {
        DisableOthers();
        NGUITools.SetActive(Information, true);
        _informationLabel.color = Color.white;
        _informationSprite.spriteName = MagicStrings.SettingsActive;
        _informationArrowSprite.spriteName = MagicStrings.ArrowActive;
    }

    public void ReleseClick()
    {
        //Debug.Log("PRODUCT KEY HAS BEEN RELESED!!!!");
        StartCoroutine(gameObject.GetComponent<ReleaseInstallationId>().ReleaseId(ReleseNotification));
    }

    public void ReleseNotification(bool success, string title, string message)
    {
        if (success)
        {
            Application.LoadLevel("Login");
        }
        else
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton(title, message, "Ok");
        }
    }

    public void DontSaveClick()
    {
        Application.LoadLevel("SettingsPage");
    }

    public void DefaultClick() 
    {
        gameObject.GetComponent<CreateNotification>().CreateOneButton("Restore Default Settings ?", "Are you sure you wish to restore the settings to default? This will override all of your current settings.", "Yes", "No", new EventDelegate(DefaultAction));
    }

    private void DefaultAction()
    {
        PlayerPrefs.SetInt(MagicStrings.PinOptionKey, 0);
        PlayerPrefs.SetInt(MagicStrings.SliderOptionKey, 0);
        PlayerPrefs.SetInt(MagicStrings.FullscreenOptionKey, 0);
        QualitySettings.SetQualityLevel(2);
        PlayerPrefs.SetInt(MagicStrings.QualitySettingsKey, 2);
        PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, 30);
        _title.value = "";
        _name.value = "";
        _email.value = "";
        _lastName.value = "";
        BFSSecurePlayerData.SetString(MagicStrings.ConTitle, _title.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConName, _name.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConEmail, _email.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConLastName, _lastName.value, SystemInfo.deviceUniqueIdentifier);
        gameObject.GetComponent<DrawingOptions>().GreenSelect();
        gameObject.GetComponent<DrawingOptions>().MediumWidthSelect();
        Application.LoadLevel("SettingsPage");
    }

    public bool ValidateEmail(string emailaddress)
    {
        try
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(emailaddress);
            if (match.Success)
                return true;
            else
                return false;
        }
        catch (FormatException)
        {
            Debug.Log("not valid email");
            return false;
        }
    }

    public void SaveSettings()
    {

        if (!ValidateEmail(_email.value))
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "Please enter the valid email address.", "Ok");
            return;
        }

        if(_pinToggle.isChecked)
        {
            PlayerPrefs.SetInt(MagicStrings.PinOptionKey, 1);
        }
        else
        {
            PlayerPrefs.SetInt(MagicStrings.PinOptionKey, 0);
        }
        if (_sliderToggle.isChecked)
        {
            PlayerPrefs.SetInt(MagicStrings.SliderOptionKey, 1);
        }
        else
        {
            PlayerPrefs.SetInt(MagicStrings.SliderOptionKey, 0);
        }
        if (_fullscreenToggle.isChecked)
        {
            PlayerPrefs.SetInt(MagicStrings.FullscreenOptionKey, 1);
        }
        else
        {
            PlayerPrefs.SetInt(MagicStrings.FullscreenOptionKey, 0);
        }

        switch (_qualitySetting)
       {
           case "Low":
               QualitySettings.SetQualityLevel(0);
               PlayerPrefs.SetInt(MagicStrings.QualitySettingsKey, 0);
               break;
           case "Medium":
               QualitySettings.SetQualityLevel(1);
               PlayerPrefs.SetInt(MagicStrings.QualitySettingsKey, 1);
               break;
           case "High":
               QualitySettings.SetQualityLevel(2);
               PlayerPrefs.SetInt(MagicStrings.QualitySettingsKey, 2);
               break;
       }

        switch(_ResolutionDropDown.value)
        {
            case "1024 x 768":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1024);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 768);
                break;
            case "1280 x 720":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1280);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 720);
                break;   
            case "1280 x 800":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1280);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 800);
                break;
            case "1280 x 960":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1280);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 960);
                break;
            case "1280 x 1024":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1280);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1024);
                break;
            case "1366 x 768":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1366);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 768);
                break;
            case "1400 x 1050":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1400);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1050);
                break;
            case "1440 x 900":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1440);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 900);
                break;
            case "1600 x 1200":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1600);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1200);
                break;
            case "1680 x 900":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1680);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 900);
                break;
            case "1680 x 1050":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1680);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1050);
                break;
            case "1920 x 1080":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1920);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1080);
                break;
            case "1920 x 1200":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1920);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1200);
                break;
            case "1920 x 1440":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1920);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1440);
                break;
            case "2560 x 1600":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 2560);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 1600);
                break;
            case "3840 x 2160":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 3840);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 2160);
                break;
            case "3840 x 2400":
                PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 3840);
                PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 2400);
                break;
        }

        _ResolutionDropDown.value = PlayerPrefs.GetInt(MagicStrings.ResolutionXKey).ToString() + " x " + PlayerPrefs.GetInt(MagicStrings.ResolutionYKey).ToString();

        Debug.Log("RES SAVED AS : "+ PlayerPrefs.GetInt(MagicStrings.ResolutionXKey) + " - " + PlayerPrefs.GetInt(MagicStrings.ResolutionYKey));

        switch(_hibernationSetting)
        {
            case "Never":
                PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, int.MaxValue);
                break;
            case "30 Seconds":
                PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, 30);
                break;
            case "1 Minute":
                PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, 60);
                break;
            case "5 Minutes":
                PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, 300);
                break;
            case "10 Minutes":
                PlayerPrefs.SetInt(MagicStrings.HibernationTimeKey, 600);
                break;
        }

        BFSSecurePlayerData.SetString(MagicStrings.ConTitle, _title.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConName, _name.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConEmail, _email.value, SystemInfo.deviceUniqueIdentifier);
        BFSSecurePlayerData.SetString(MagicStrings.ConLastName, _lastName.value, SystemInfo.deviceUniqueIdentifier);
        Debug.Log("Update all settings stored in playerprefs.");

        Debug.Log("Hibernation time is now : "+PlayerPrefs.GetInt(MagicStrings.HibernationTimeKey));
        gameObject.GetComponent<CreateNotification>().CreateOkayButton("Settings Saved", "Your settings have been saved.", "Ok");
        //Application.LoadLevel(Application.loadedLevelName);
    }

    public void UpdateQuality()
    {
        if (_qualityDropDown != null)
        _qualitySetting = _qualityDropDown.value;
    }

    public void UpdateRes()
    {
        _ResolutionSetting = _ResolutionDropDown.value;
    }

    public void UpdateHibernation()
    {
        _hibernationSetting = _hibernationDropDown.value;
    }
}
