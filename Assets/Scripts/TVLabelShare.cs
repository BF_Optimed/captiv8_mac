using Assets.DBHelper;
using Assets.Share;
using System;
using System.IO;
using UnityEngine;
using System.Linq;
using Assets.Scripts;
using System.Collections;
using Assets.OptimedClient;

public class TVLabelShare : MonoBehaviour
{
    public static string Path;
    private TvLibraryGuiController _tvGuiCon;
    private Video video;
    private GameObject _controller;
    private CreateNotification _createNotification;
    private ContinuousVars _continuousVars;
    private GameObject _uploading;
    private GameObject _persistent;

    private void OnEnable()
    {
        _controller = GameObject.Find("Controller");
        _persistent = GameObject.Find("PersistentController");
        _uploading = GameObject.FindGameObjectWithTag("Upload");
        _tvGuiCon = _controller.GetComponent<TvLibraryGuiController>();
        _createNotification = _controller.GetComponent<CreateNotification>();
        
    }

    public void OnClick()
    {
        Path = gameObject.transform.parent.transform.FindChild("FileName").GetComponent<FileNameStore>().Data;
        video = new Video().GetVideo(Path);
        if (CheckSettingsForInfo())
        {
            _continuousVars = _persistent.GetComponent<ContinuousVars>();
            _continuousVars.LoadedVideoDataPath = Path;
            if(video.Public)
            {
                _tvGuiCon.ActivatePrivateShare();
            }
            else
            {
                _createNotification.CreateTwoButton("Public or Private", "Is this video going to be a Public or private video?", "Public",
                    "Private", new EventDelegate(PublicShare), new EventDelegate(_tvGuiCon.ActivatePrivateShare));
                
            }
        }
        else
        {
            _createNotification.CreateOkayButton("Error", "Please fill out your personal details in settings before sharing.", "Ok");
        }
            
    }
    public void ShareResponse(ShareService.Contracts.ShareResponse response)
    {
        //Upload stops
        LoadingNotifInitiliser.IsBusy = false;
        _persistent.GetComponent<Hibernation>().CanHibernate = true;
        Destroy(GameObject.FindGameObjectWithTag("LoadingNotif"));
        if (response.Error)
        {
            _createNotification.CreateOkayButton("Error", response.ErrorMessage, "Ok");
            Debug.Log(response.ErrorMessage);
        }
        else
        {
            video.AnimationId = response.Animations.AnimationId;
            video.Public = true;
            video.UpdateVideo();
            //SEAN ADD GREEN TRIANGLE HERE
            _tvGuiCon.UpdateList();
            _createNotification.CreateOkayButton("Share Successful", "You have sucessfully shared animation", "Ok");
        }
    }
    public void PublicShare()
    {
        if(!validateShareLimit())
        {
            _createNotification.CreateOkayButton("Share limitation reached.", "You have exceeded your public animation limit.  Please reduce the number of public animations from your online management area.", "Ok");
            return;
        }
        _createNotification.CreateLoadingNotification("Please wait...");
        LoadingNotifInitiliser.IsBusy = true;
        _persistent.GetComponent<Hibernation>().CanHibernate = false;
        StartCoroutine(PublicShareRoutine());
    }
    public bool validateShareLimit()
    {
        var encryption = new BFSEncryption();
        var shareCount = new Video().GetAllVideos("Status ='Local' And AnimationId like '" + InstallationId.GetInstallationId.Trim() + "-%'").Count;
        string creationLimit;
        encryption.TryDecrypt(Runtime.Default.RuntimeId, SystemInfo.deviceUniqueIdentifier, out creationLimit);
        if (Convert.ToInt32(creationLimit) <= shareCount) 
        {
            return false;
        }
        return true;
 
    }
    public IEnumerator PublicShareRoutine()
    {
        yield return null;
        if (video.Private && !video.Public)
        {
            video.AnimationId = "";
            video.UpdateVideo();
        }
        var shareRequest = new ShareRequestHelper(video);
        var restRequest = shareRequest.CreatePublicRequest();
        shareRequest.PostShareRequest(restRequest, ShareResponse);
    }
    bool CheckSettingsForInfo()
    {
        bool temp = true;
        if (string.IsNullOrEmpty(BFSSecurePlayerData.GetString(MagicStrings.ConEmail, SystemInfo.deviceUniqueIdentifier)))
        {
            temp = false;
        }
        if (string.IsNullOrEmpty(BFSSecurePlayerData.GetString(MagicStrings.ConLastName, SystemInfo.deviceUniqueIdentifier)))
        {
            temp = false;
        }
        if (string.IsNullOrEmpty(BFSSecurePlayerData.GetString(MagicStrings.ConName, SystemInfo.deviceUniqueIdentifier)))
        {
            temp = false;
        }
        if (string.IsNullOrEmpty(BFSSecurePlayerData.GetString(MagicStrings.ConTitle, SystemInfo.deviceUniqueIdentifier)))
        {
            temp = false;
        }
        return temp;
    }
}