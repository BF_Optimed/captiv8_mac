﻿using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class lineDrawMouse : MonoBehaviour
{
    private bool canDraw;
    public Material capMaterial;
    public Texture2D capTex;
    public Material Continuousline;
    public Material Dottedline;
    private VectorLine line;
    private List<VectorLine> LineArray;
    private int lineIndex;
    public Material lineMaterial;
    private Vector2[] linePoints;
    public float lineWidth = 4.0f;
    private BFSDLineWidth LineWidth;
    public int maxPoints = 1000;
    public int minPixelMove = 5; // Must move at least this many pixels per sample for a new segment to be recorded
    private int oldWidth;
    private Vector2 previousPosition;
    private ColorSelection SelectColor;
    private int sqrMinPixelMove;
    public bool useEndCap = false;

    public GameObject BtnController;
    private BtnController _btnController;

    public string LineMaterial
    {
        get { return lineMaterial.name; }
    }

    // Use this for initialization
    private void Start()
    {
        _btnController = BtnController.GetComponent<BtnController>();
        SelectColor = BtnController.GetComponent<ColorSelection>();
        LineWidth = BtnController.GetComponent<BFSDLineWidth>();
        
        Init();
    }

    public void Init()
    {
        lineWidth = LineWidth.GetCurrentLineWidth();
        lineMaterial = Continuousline;
        if (useEndCap)
        {
            VectorLine.SetEndCap("cap", EndCap.Mirror, capMaterial, capTex);
            lineMaterial = capMaterial;
        }
        LineArray = new List<VectorLine>();
        linePoints = new Vector2[maxPoints];

        line = new VectorLine("DrawnLine", linePoints, lineMaterial, lineWidth, LineType.Continuous, Joins.Weld);
        lineMaterial.color = SelectColor.GetCurrentColor();
        line.depth = _btnController.DepthCount;
        _btnController.DepthCount++;
        if (useEndCap)
        {
            line.endCap = "cap";
        }

        sqrMinPixelMove = minPixelMove*minPixelMove;
        oldWidth = Screen.width;
    }

    // Update is called once per frame
    private void Update()
    {
        Vector2 mousePos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            line = new VectorLine("DrawnLine", linePoints, lineMaterial, LineWidth.GetCurrentLineWidth(),
                LineType.Continuous, Joins.Weld);
            lineMaterial.color = SelectColor.GetCurrentColor();
            line.depth = _btnController.DepthCount;
            _btnController.DepthCount++;

            line.ZeroPoints();
            line.minDrawIndex = 0;
            line.Draw();

            previousPosition = linePoints[0] = mousePos;
            lineIndex = 0;
            canDraw = true;
            LineArray.Add(line);
        }
        else if (Input.GetMouseButton(0) && (mousePos - previousPosition).sqrMagnitude > sqrMinPixelMove && canDraw)
        {
            previousPosition = linePoints[++lineIndex] = mousePos;
            line.minDrawIndex = lineIndex - 1;
            line.maxDrawIndex = lineIndex;
            if (useEndCap) line.drawEnd = lineIndex;
            if (lineIndex >= maxPoints - 1) canDraw = false;
            line.Draw();
        }

        if (oldWidth != Screen.width)
        {
            oldWidth = Screen.width;
            VectorLine.SetCamera();
        }

        if (Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0))
        {
            var countArray = LineArray.Count;
            print(countArray);
            if (countArray != 0)
            {
                var newLineRefrence = LineArray[countArray - 1];
                if (newLineRefrence != null)
                {
                    VectorLine.Destroy(ref newLineRefrence);
                    LineArray.RemoveAt(countArray - 1);
                }
            }
        }
    }

    private void OnDisable()
    {
        //	deleteArray ();
    }

    public void deleteArray()
    {
        if (LineArray != null)
            VectorLine.Destroy(LineArray);
    }

    public void DottedLine()
    {
        lineMaterial = new Material(Dottedline);
    }

    public void ContinuousLine()
    {
        lineMaterial = new Material(Continuousline);
    }
}