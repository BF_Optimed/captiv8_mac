﻿using UnityEngine;

public class SliderLimiter : MonoBehaviour
{
    private UISlider _otherSlider;
    private UISlider _thisSlider;
    public string LimitType;
    public float OldNum;
    public GameObject OtherSlider;
    public float StartValue;

    private void OnEnable()
    {
        _thisSlider = gameObject.GetComponent<UISlider>();
        _otherSlider = OtherSlider.GetComponent<UISlider>();
        _thisSlider.value = StartValue;
    }

    public void OnValueChange()
    {
        if (LimitType == "Less")
        {
            KeepLess();
        }
        else
        {
            KeepMore();
        }
        OldNum = _thisSlider.value;
    }

    private void KeepMore()
    {
        if (_thisSlider.value > _otherSlider.value)
        {
            _thisSlider.value = OldNum;
        }
    }

    private void KeepLess()
    {
        if (_thisSlider.value < _otherSlider.value)
        {
            _thisSlider.value = OldNum;
        }
    }
}