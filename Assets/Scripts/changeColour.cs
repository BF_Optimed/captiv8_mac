﻿using UnityEngine;

public class changeColour : MonoBehaviour
{
    private bool clicked;
    private UILabel label;
    // Use this for initialization
    private void OnEnable()
    {
        label = gameObject.GetComponent<UILabel>();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void SetBlue()
    {
        label.color = Color.blue;
    }

    public void SetBlack()
    {
        label.color = Color.black;
    }

    private void OnClick()
    {
        if (clicked)
        {
            SetBlack();
        }
        else
        {
            SetBlue();
        }
        clicked = !clicked;
    }
}