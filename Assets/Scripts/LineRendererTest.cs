﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (LineRenderer))]
public class LineRendererTest : MonoBehaviour
{
    public float endWidth = 0.001f;
    private Vector3 lastPos = Vector3.one*float.MaxValue;
    private int lineCount;
    private List<Vector3> linePoints = new List<Vector3>();
    private LineRenderer lineRenderer;
    public float startWidth = 0.001f;
    private Camera thisCamera;
    public float threshold = 0.001f;

    private void Awake()
    {
        thisCamera = Camera.main;
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        var mousePos = Input.mousePosition;
        //mousePos.z = thisCamera.nearClipPlane;
        mousePos.z = 5.0f;
        var mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);

        var dist = Vector3.Distance(lastPos, mouseWorld);

        if (dist <= threshold)
            return;

        lastPos = mouseWorld;
        if (linePoints == null)
            linePoints = new List<Vector3>();
        linePoints.Add(mouseWorld);

        UpdateLine();
    }

    private void UpdateLine()
    {
        lineRenderer.SetWidth(startWidth, endWidth);
        lineRenderer.SetVertexCount(linePoints.Count);

        for (var i = lineCount; i < linePoints.Count; i++)
        {
            lineRenderer.SetPosition(i, linePoints[i]);
        }
        lineCount = linePoints.Count;
    }
}