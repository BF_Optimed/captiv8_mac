﻿using UnityEngine;

public class SyncGreyOut : MonoBehaviour
{
    public GameObject Close;
    public GameObject NotifBackground;

    public void CloseClick()
    {
        NGUITools.SetActive(NotifBackground, false);
        NGUITools.SetActive(Close, false);
    }
}