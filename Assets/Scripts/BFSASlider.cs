﻿using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class BFSASlider : MonoBehaviour
{
    private Animator _anim;
    private BFSUTransition _blackoutScript;
    private bool _isClicked;
    private Vector2 _mouseNumOne;
    private Vector2 _mouseNumTwo;
    private float _mouseTemp;
    private float _mouseTotal;
    private BtnController _scrBtnCon;
    private int _tempAnimNum;
    public string[] AnimNames;
    public GameObject GoBlackout;
    public GameObject GoBtnController;
    public string[] ModelList;
    public int Sensitivity = 10;
    public int SelectedAction { get; set; }
    public GameObject Controller;
    private bool _usingPhysicalSlider;
    public GameObject PhysicalSlider;
    private UISlider _physicalSlider;
    public GameObject RoomAnimator;

    public void Start()
    {
        _physicalSlider = PhysicalSlider.GetComponent<UISlider>();
        _scrBtnCon = Controller.GetComponent<BtnController>();
        _blackoutScript = GoBlackout.GetComponent<BFSUTransition>();
        SortActionButtons("Overview");
        ResetAnimArray();
        if(PlayerPrefs.GetInt(MagicStrings.SliderOptionKey)==1)
        {
            _usingPhysicalSlider = true;
        }
        else
        {
            _usingPhysicalSlider = false;
        }

        
    }

    void OnEnable()
    {
        if (_usingPhysicalSlider)
            NGUITools.SetActive(PhysicalSlider, true);
    }

    void OnDisable()
    {
        NGUITools.SetActive(PhysicalSlider, false);
    }

    public void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (_isClicked)
            {
                SustainedClick();
            }
            else
            {
                InitialClick();
            }

            UpdateMouseTotal();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            UnClicked();
        }

        if (_isClicked)
        {
            UpdateSelectedAnimation();
        }
    }

    private void UnClicked()
    {
        _isClicked = false;
        _mouseTemp = _mouseTotal;
    }

    private void SustainedClick()
    {
        _mouseNumTwo = Input.mousePosition;
    }

    private void InitialClick()
    {
        _mouseNumOne = _mouseNumTwo = Input.mousePosition;
        _isClicked = true;
    }

    private void UpdateSelectedAnimation()
    {
        if(_usingPhysicalSlider)
        {
            float temp = _physicalSlider.value;
            if (temp == 1.0f)
                temp = 0.9999f;
            _anim.Play(AnimNames[SelectedAction], 0, (temp));
            BFSRSwitchController.Instance.AnimationToRecord(AnimNames[SelectedAction], temp);
        }
        else
        {
            _anim.Play(AnimNames[SelectedAction], 0, (_mouseTotal / 100.0f));
            BFSRSwitchController.Instance.AnimationToRecord(AnimNames[SelectedAction], _mouseTotal / 100.0f);
        } 
    }

    private void UpdateMouseTotal()
    {
        _mouseTotal = _mouseTemp + (_mouseNumOne.x - _mouseNumTwo.x) / Sensitivity;
        _mouseTotal = LimitNum(_mouseTotal);
    }

    private static float LimitNum(float num)
    {
        if (num > 99)
        {
            num = 99;
        }
        if (num < 0)
        {
            num = 0;
        }
        return num;
    }

    public void SortActionButtons(string objName)
    {
        Debug.Log("Obj Name is : "+objName);
        for (var i = 0; i <= ModelList.Length - 1; i++)
        {
            if (ModelList[i] == objName)
            {
                DeactivateButtonArray(_scrBtnCon.OverviewArray);
                DeactivateButtonArray(_scrBtnCon.AdnexaArray);
                DeactivateButtonArray(_scrBtnCon.CiliaryArray);
                DeactivateButtonArray(_scrBtnCon.OpticNerveArray);
                DeactivateButtonArray(_scrBtnCon.MaculaArray);
                DeactivateButtonArray(_scrBtnCon.FrontSectionArray);
                DeactivateButtonArray(_scrBtnCon.RoomViewArray);

                switch (ModelList[i])
                {
                    case "Overview":
                        ActivateButtonArray(_scrBtnCon.OverviewArray);
                        break;
                    case "Adnexa":
                        ActivateButtonArray(_scrBtnCon.AdnexaArray);
                        break;
                    case "Ciliary Body":
                        ActivateButtonArray(_scrBtnCon.CiliaryArray);
                        break;
                    case "Optic Nerve":
                        ActivateButtonArray(_scrBtnCon.OpticNerveArray);
                        break;
                    case "Macula":
                        ActivateButtonArray(_scrBtnCon.MaculaArray);
                        break;
                    case "Anterior Segment":
                        ActivateButtonArray(_scrBtnCon.FrontSectionArray);
                        break;
                    case "Vision Simulator":
                        ActivateButtonArray(_scrBtnCon.RoomViewArray);
                        break;
                }
            }
        }
    }

    private void ActivateButtonArray(GameObject[] array)
    {
        _scrBtnCon.ActionButtons = array;
        for (var j = 0; j <= array.Length - 1; j++)
        {
            NGUITools.SetActive(array[j], true);
        }
    }

    private void DeactivateButtonArray(IList<GameObject> array)
    {
        for (var j = 0; j <= array.Count - 1; j++)
        {
            array[j].SetActive(false);
        }
    }

    public void ResetAnimArray()
    {
        GetNewAnimator();

        AnimNames = new string[_scrBtnCon.ActionButtons.Length];

        PopulateAnimNamesList();

        
    }

    private void GetNewAnimator()
    {
        if (_blackoutScript.CurrentObj.name == "Vision Simulator")
        {
            _anim = RoomAnimator.GetComponent<Animator>();
        }
        else
        {
            _anim = _blackoutScript.CurrentObj.GetComponent<Animator>();
        }
        
        _anim.speed = 0;
    }

    private void PopulateAnimNamesList()
    {
        _tempAnimNum = 0;
        foreach (var g in _scrBtnCon.ActionButtons)
        {
            AnimNames[_tempAnimNum] = g.name;
            _tempAnimNum++;
        }
    }

    public void ResetAnims()
    {
        
        ResetAnimArray();

        SetAnimsToZero();

        ResetAmounts();

        
    }

    private void SetAnimsToZero()
    {
            foreach (var s in AnimNames)
            {
                _anim.Play(s, 0, 0.0f);
                BFSRSwitchController.Instance.AnimationToRecord(s, 0.0f);
            }

            _physicalSlider.value = 0;
    }

    public void ResetAmounts()
    {
        _mouseTotal = 0;
        _mouseTemp = 0;
    }
}