﻿using UnityEngine;

public class UpdateAnchors : MonoBehaviour
{
    private UISprite _spritescr;
    public GameObject Slider1;
    public GameObject Slider2;
    public UISlider Sliderscr1;
    public UISlider Sliderscr2;
    public GameObject Thumb1;
    public GameObject Thumb2;

    private void OnEnable()
    {
        _spritescr = gameObject.GetComponent<UISprite>();
        Sliderscr1 = Slider1.GetComponent<UISlider>();
        Sliderscr2 = Slider2.GetComponent<UISlider>();
    }

    private void Update()
    {
        if (Sliderscr1.value > Sliderscr2.value)
        {
            _spritescr.leftAnchor.target = Thumb2.transform;
            _spritescr.rightAnchor.target = Thumb1.transform;
        }
        else
        {
            _spritescr.leftAnchor.target = Thumb1.transform;
            _spritescr.rightAnchor.target = Thumb2.transform;
        }
    }
}