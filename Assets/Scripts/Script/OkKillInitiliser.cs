﻿using UnityEngine;
using System.Collections;

public class OkKillInitiliser : MonoBehaviour {

    public GameObject Button1;
    public GameObject Description;
    public GameObject Title;
    public UILabel TitleText { get; set; }
    public UILabel DescText { get; set; }
    public UILabel Btn1 { get; set; }

    private void Awake()
    {
        TitleText = Title.GetComponentInChildren<UILabel>();
        DescText = Description.GetComponent<UILabel>();
        Btn1 = Button1.GetComponentInChildren<UILabel>();
    }

    public void DeleteSelf()
    {
        Destroy(gameObject);
        Application.Quit();
    }

    public void CreateOkayButton(string btn1, string titleText, string descripText)
    {
        SetUpText(btn1, titleText, descripText);
    }

    private void SetUpText(string btn1, string titleText, string descripText)
    {
        Btn1.text = btn1;
        TitleText.text = titleText;
        DescText.text = descripText;
    }
}
