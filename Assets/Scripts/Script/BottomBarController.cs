﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class BottomBarController : MonoBehaviour
{

    private UISprite _homeBtnSprite;
    private UILabel _homeLabel;
    public GameObject HomeButton;
    private UISprite _settingsBtnSprite;
    private UILabel _settingsLabel;
    public GameObject SettingsButton;
    private UISprite _libraryBtnSprite;
    private UILabel _libraryLabel;
    public GameObject LibraryButton;

    public GameObject SyncButton;
    public GameObject SyncButtonBackground;
    public GameObject Syncs;
    private UISprite _sprSyncBtn;
    public GameObject SyncLabel;
    private UILabel _syncLabel;
    private bool _syncActive;
    private ContinuousVars _continuousVars;

    private VideoPlayController _videoPlayController;

    // Use this for initialization
    void Start()
    {
        _syncLabel = SyncLabel.GetComponent<UILabel>();
        _videoPlayController = gameObject.GetComponent<VideoPlayController>();
        _homeBtnSprite = HomeButton.GetComponent<UISprite>();
        _settingsBtnSprite = SettingsButton.GetComponent<UISprite>();
        _sprSyncBtn = SyncButton.GetComponent<UISprite>();
        _libraryBtnSprite = LibraryButton.GetComponent<UISprite>();
        _continuousVars = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
        _homeLabel = HomeButton.GetComponentInChildren<UILabel>();
        _settingsLabel = SettingsButton.GetComponentInChildren<UILabel>();
        _libraryLabel = LibraryButton.GetComponentInChildren<UILabel>();

    }

    public void OverviewLoad()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        Application.LoadLevel("Overview");
    }

    public void HomeHoverOver()
    {
        _homeBtnSprite.spriteName = MagicStrings.HomeBtnSpriteHit;
        _homeLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void HomeHoverOut()
    {
        _homeBtnSprite.spriteName = MagicStrings.HomeBtnSprite;
        _homeLabel.color = Color.white;
    }

    public void ActivateSettings() 
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        Application.LoadLevel("SettingsPage");
    }

    public void SettingsHoverOver()
    {
        _settingsBtnSprite.spriteName = MagicStrings.SettingsBtnSpriteHit;
        _settingsLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }

    public void SettingsHoverOut()
    {
        _settingsBtnSprite.spriteName = MagicStrings.SettingsBtnSprite;
        _settingsLabel.color = Color.white;
    }

    public void SyncBtnClick()
    {
        _syncActive = !_syncActive;
        if (_syncActive)
        {
            EnableSync();
        }
        else
        {
            DisableSync();
        }
    }

    public void DisableSync()
    {
        _syncLabel.text = "0";
        if (Application.loadedLevelName == "Overview")
        {
            gameObject.GetComponent<BtnController>().Reset();
            Debug.Log("Should have reset now.");
        }
        _sprSyncBtn.spriteName = MagicStrings.SyncBtnSprite;
        NGUITools.SetActive(SyncButtonBackground, false);
        NGUITools.SetActive(Syncs, false);
        _syncActive = false;
    }

    private void EnableSync()
    {
        _continuousVars.SyncCount = 0;
        if(Application.loadedLevelName == "Overview")
            gameObject.GetComponent<BtnController>().DisableAllFunctions();
        _sprSyncBtn.spriteName = MagicStrings.SyncBtnSprite;
        NGUITools.SetActive(SyncButtonBackground, true);
        NGUITools.SetActive(Syncs, true);
    }

    public void LoadLibrary()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        Application.LoadLevel("TVLibrary");
    }

    public void LoadEditView()
    {
        if (_continuousVars.LoadedVideo.Locked)
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Cannot Edit", "You cannot edit a public movie that you have not created.", "Ok");
        }
        else
        {
            BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
            Application.LoadLevel("VideoEdit");
        }
        
    }

    public void LoadVideoView()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        Application.LoadLevel("VideoView");
    }

    public void LibraryHoverOut() 
    {
        _libraryBtnSprite.spriteName = MagicStrings.TvBtnSprite;
        _libraryLabel.color = Color.white;
    }
    public void LibraryHoverOver() 
    {
        _libraryBtnSprite.spriteName = MagicStrings.TvBtnSpriteHit;
        _libraryLabel.color = new Color(0.121f, 0.6f, 0.92f, 1.0f);
    }


}
