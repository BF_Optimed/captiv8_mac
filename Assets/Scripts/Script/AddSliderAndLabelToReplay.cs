﻿using UnityEngine;
using System.Collections;

public class AddSliderAndLabelToReplay : MonoBehaviour
{
    public GameObject Label;
    public GameObject Slider;
    private bool _test = true;

    public void OnClick()
    {
        BFSRSwitchController.Instance.AddSliderToReplay(Slider.GetComponent<UISlider>());
        BFSRSwitchController.Instance.AddTimerLabelToReplay(Label.GetComponent<UILabel>());
    }

    public void hover()
    {
        Debug.Log("HOVERING");
    }
}
