﻿using Assets.DBHelper;
using Assets.SynchroniseScript;
using UnityEngine;
using System.Threading;
using System.Collections;
using System;
using Assets.Scripts;
using Mono.Data.SqliteClient;
using Assets.OptimedClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

public class ContinuousVars : MonoBehaviour
{
    public string LoadedVideoDataPath;
    public string CatagoryString = "Default"; 
    public Video LoadedVideo;
    private SynchroniseManager _syncManager;
    public int SyncCount;
    private static string _connectionString;
    public string LastLevel;
    public string[] Processes = { "devenv" };
    private CreateNotification _createNotification;
    public string ConnectionString
    {
        get { return _connectionString; }
    }
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt(MagicStrings.QualitySettingsKey));
        _createNotification = gameObject.GetComponent<CreateNotification>();
        _connectionString = Application.persistentDataPath + "/OptimedDB.db";
        StartCoroutine(GetSyncCount(UpdateCount));
        
    }

    private IEnumerator GetSyncCount(Action<int> updateCount)
    {
        yield return null;
        try
        {
            if (!System.IO.File.Exists(ConnectionString))
            {
                new InitDatabase(ConnectionString).CreateTables();
            }
            var count = new Video(ConnectionString).GetAllVideos(" Status = '" + Status.Display.ToString() + "'").Count;
        updateCount(count);
        }
        catch (SqliteSyntaxException ex)
        {
            UnityEngine.Debug.Log("Ex : " + ex.Message);
        }
      
        
    }

    private void UpdateCount(int i)
    {
        SyncCount = i;
    }

    public void UpdateCountNumber()
    {
        if (CheckIfSyncShouldRun())
        {
                StartCoroutine(GetSyncCount(UpdateCount));
        }
        
    }

    bool CheckIfSyncShouldRun()
    {
        switch(Application.loadedLevelName)
        {
            case "SplashScreen":
            case "Login":
                return false;
            default:
                return true;
        }
    }

    private void CheckForApplication() 
    {
        foreach (string s in Processes)
        {
            try
            {
                Process[] array = System.Diagnostics.Process.GetProcessesByName(s);

                try
                {
                    if (array.Length > 0)
                    {
                        _createNotification.CreateOkayKillButton("Error", "Conflicting software has been detected. Please close these instances before running CAPTIV8+ again.", "Exit");
                    }
                }
                catch
                {

                }
            }
            catch
            {

            }
        }
    }

    private IEnumerator KickDownload(IEnumerable<string> videoIds, Video[] videos)
    {
        yield return null;
        foreach (var id in videoIds)
        {
            var video = videos.Where(x => x.Id == id).First();
            ChangeStatus(video, Status.Downloading);
        }
        SynchroniseSingleton.Instance.AddAnimationToDownload(videoIds.ToList());

    }
    private void ChangeStatus(Video video, Status status)
    {
        video.Status = status;
        video.UpdateVideo();
    }
    public void StartDownload(IEnumerable<string> videosId, Video[] videos)
    {
        StartCoroutine(KickDownload(videosId, videos));
    }
}

