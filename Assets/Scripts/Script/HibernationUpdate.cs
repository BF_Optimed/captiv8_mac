﻿using UnityEngine;
using System.Collections;

public class HibernationUpdate : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject temp = GameObject.Find("PersistentController");
        temp.GetComponent<Hibernation>().UpdateHibernationTime();
        temp.GetComponent<ContinuousVars>().UpdateCountNumber();
	}
}
