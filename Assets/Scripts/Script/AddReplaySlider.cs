﻿using UnityEngine;

public class AddReplaySlider : MonoBehaviour
{
	void Start ()
	{
        BFSReplayManager _bfsReplayManager = GameObject.Find("BFSRecorder").GetComponent<BFSReplayManager>();
	    UISlider _uiSlider = gameObject.GetComponent<UISlider>();

        _uiSlider.onChange.Add(new EventDelegate(_bfsReplayManager.onSliderValueChanged));
	}
}
