﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class DrawingOptions : MonoBehaviour {

    public GameObject Green;
    public GameObject Blue;
    public GameObject Red;
    public GameObject Yellow;
    public GameObject Black;
    private UISprite _greenSprite;
    private UISprite _blueSprite;
    private UISprite _redSprite;
    private UISprite _yellowSprite;
    private UISprite _blackSprite;
    public GameObject SmallWidth;
    public GameObject MediumWidth;
    public GameObject LargeWidth;
    private UISprite _smallWidthSprite;
    private UISprite _mediumWidthSprite;
    private UISprite _largeWidthSprite;

    private string _defaultColor;

    private SettingsBtnController _settingsBtnController;

    // Use this for initialization
    void Start()
    {
        _greenSprite = Green.GetComponent<UISprite>();
        _blueSprite = Blue.GetComponent<UISprite>();
        _redSprite = Red.GetComponent<UISprite>();
        _yellowSprite = Yellow.GetComponent<UISprite>();
        _blackSprite = Black.GetComponent<UISprite>();
        _smallWidthSprite = SmallWidth.GetComponent<UISprite>();
        _mediumWidthSprite = MediumWidth.GetComponent<UISprite>();
        _largeWidthSprite = LargeWidth.GetComponent<UISprite>();

        _settingsBtnController = gameObject.GetComponent<SettingsBtnController>();

        Debug.Log("Selected Color is : " + PlayerPrefs.GetString(MagicStrings.DefaultColourKey));

        switch (PlayerPrefs.GetString(MagicStrings.DefaultColourKey))
        {
            case "green":
                GreenSelect();
                break;
            case "red":
                RedSelect();
                break;
            case "blue":
                BlueSelect();
                break;
            case "yellow":
                YellowSelect();
                break;
            case "black":
                BlackSelect();
                break;
            
        }

        switch (PlayerPrefs.GetString(MagicStrings.DefaultWidthKey))
        {
            case "1":
                SmallWidthSelect();
                break;
            case "2":
                MediumWidthSelect();
                break;
            case "3":
                LargeWidthSelect();
                break;

        }

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void ResetColors()
    {
        _greenSprite.spriteName = MagicStrings.ColorunhighlightGreen;
        _redSprite.spriteName = MagicStrings.ColorunhighlightRed;
        _blueSprite.spriteName = MagicStrings.ColorunhighlightBlue;
        _yellowSprite.spriteName = MagicStrings.ColorunhighlightYellow;
        _blackSprite.spriteName = MagicStrings.ColorunhighlightBlack;
    }

    private void ResetWidths()
    {
        _smallWidthSprite.spriteName = MagicStrings.SmallWidthSpriteBox;
        _mediumWidthSprite.spriteName = MagicStrings.MediumWidthSprite;
        _largeWidthSprite.spriteName = MagicStrings.LargeWidthSprite;
    }

    public void GreenSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultColourKey, "green");
        ResetColors();
        _greenSprite.spriteName = MagicStrings.ColorhighlightGreen;
        _settingsBtnController.DefaultColour = "green";
    }

    public void RedSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultColourKey, "red");
        ResetColors();
        _redSprite.spriteName = MagicStrings.ColorhighlightRed;
        _settingsBtnController.DefaultColour = "red";
    }

    public void BlueSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultColourKey, "blue");
        ResetColors();
        _blueSprite.spriteName = MagicStrings.ColorhighlightBlue;
        _settingsBtnController.DefaultColour = "blue";
    }

    public void YellowSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultColourKey, "yellow");
        ResetColors();
        _yellowSprite.spriteName = MagicStrings.ColorhighlightYellow;
        _settingsBtnController.DefaultColour = "yellow";
    }

    public void BlackSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultColourKey, "black");
        ResetColors();
        _blackSprite.spriteName = MagicStrings.ColorhighlightBlack;
        _settingsBtnController.DefaultColour = "black";
    }

    public void SmallWidthSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultWidthKey, "1");
        ResetWidths();
        _smallWidthSprite.spriteName = MagicStrings.SmallWidthSpriteHitBox;
        _settingsBtnController.DefaultWidth = "1";
    }

    public void MediumWidthSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultWidthKey, "2");
        ResetWidths();
        _mediumWidthSprite.spriteName = MagicStrings.MediumWidthSpriteHit;
        _settingsBtnController.DefaultWidth = "2";
    }

    public void LargeWidthSelect()
    {
        PlayerPrefs.SetString(MagicStrings.DefaultWidthKey, "3");
        ResetWidths();
        _largeWidthSprite.spriteName = MagicStrings.LargeWidthSpriteHit;
        _settingsBtnController.DefaultWidth = "3";
    }
}
