﻿using UnityEngine;
using System.Collections;

public class HoverScript : MonoBehaviour {

	public GameObject HoverObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void HoverOver()
	{
		NGUITools.SetActive (HoverObject, true);
	}

	public void HoverOverOut()
	{
		NGUITools.SetActive (HoverObject, false);
	}
}
