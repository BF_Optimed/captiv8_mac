﻿using UnityEngine;
using System.Collections;

public class MiscCategoryButtons : MonoBehaviour {

    public int ButtonType = 0;

    void OnClick()
    {
        switch(ButtonType)
        {
            case 0:
                PublicClick();
                break;
            case 1:
                PrivateClick();
                break;
            case 2:
                AllClick();
                break;
        }
        gameObject.GetComponentInParent<UIGrowthScript>().InputRecieved();
    }

    private void AllClick()
    {
        GameObject.Find("TVGrid").GetComponent<BFSGTVVideoList>().GetListFromArray("", 2);
        GameObject.Find("CategoryLabel").GetComponent<UILabel>().text = "All";
    }

    private void PublicClick()
    {
        GameObject.Find("TVGrid").GetComponent<BFSGTVVideoList>().GetListFromArray("", 0);
        GameObject.Find("CategoryLabel").GetComponent<UILabel>().text = "Public";
    }

    private void PrivateClick()
    {
        GameObject.Find("TVGrid").GetComponent<BFSGTVVideoList>().GetListFromArray("", 1);
        GameObject.Find("CategoryLabel").GetComponent<UILabel>().text = "Private";
    }
}
