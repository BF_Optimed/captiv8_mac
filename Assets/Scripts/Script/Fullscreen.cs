﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class Fullscreen : MonoBehaviour
{

    public GameObject PlayPause;
    public GameObject Time;
    public GameObject SmallSlider;
    public GameObject BigSlider;
    public GameObject FullScreen;
    public GameObject Subtitles;
    public Transform UIroot2;
    private UISprite _playpause;
    private UISprite _slider;
    private UISprite _fullscreen;
    private UILabel _time;
    private UILabel _subtitles;
    private BFSERecordAudio scrAudio;

    private bool _isFullscreen = false;

    void Start()
    {
       _playpause = PlayPause.GetComponent<UISprite>();
       _time = Time.GetComponent<UILabel>();
       _subtitles = Subtitles.GetComponent<UILabel>();
       _slider = SmallSlider.GetComponent<UISprite>();
       _fullscreen = FullScreen.GetComponent<UISprite>();
       scrAudio = Camera.main.GetComponent<BFSERecordAudio>();

        if(PlayerPrefs.GetInt(MagicStrings.FullscreenOptionKey) == 1)
        {
            MakeFullscreen();
        }
    }

    public void MakeFullscreen()
    {

        SetNewAnchors(_slider, 0.15f, 0.85f, 0.05f, 0.06f);
        SetNewAnchors(_time, 0.07f, 0.14f, 0.048f, 0.062f);
        SetNewAnchors(_fullscreen, 0.899f, 0.914f, 0.042f, 0.069f);
        SetNewAnchors(_subtitles, 0.07f, 0.875f, 0.07f, 0.1f);
        SetNewAnchors(_playpause, 0.5f, 0.5f, 0.5f, 0.5f);
        NGUITools.SetActive(BigSlider,true);
        BigSlider.GetComponent<UISlider>().value = 0.0f;
        SmallSlider.GetComponent<UISlider>().value = 0.0f;
        NGUITools.SetActive(SmallSlider, false);

       // var temp = BFSRSwitchController.Instance.FrameId;
        BFSRSwitchController.Instance.AddSliderToReplay(BigSlider.GetComponent<UISlider>());
       // BFSRSwitchController.Instance.FrameId = temp;

        _fullscreen.spriteName = "fullscreen_icon_inV2";
        Debug.Log("Fulscreen mode!");
        Camera.main.rect = new Rect(0.00f, 0.00f, 1f, 1f);
        _isFullscreen = true;
    }

     void MakeRegularScreen()
    {
        SetNewAnchors(_slider, 0.15f, 0.64f, 0.18f, 0.19f);
        SetNewAnchors(_fullscreen, 0.67f, 0.685f, 0.173f, 0.197f);
        SetNewAnchors(_subtitles, 0.07f, 0.685f, 0.2f, 0.225f);
        SetNewAnchors(_time, 0.07f, 0.14f, 0.175f, 0.19f);
        SetNewAnchors(_playpause, 0.393f,0.394f,0.5f,0.5f);
        NGUITools.SetActive(SmallSlider, true);
        BigSlider.GetComponent<UISlider>().value = 0.0f;
        SmallSlider.GetComponent<UISlider>().value = 0.0f;
        NGUITools.SetActive(BigSlider, false);

       // var temp = BFSRSwitchController.Instance.FrameId;
        BFSRSwitchController.Instance.AddSliderToReplay(SmallSlider.GetComponent<UISlider>());
       // BFSRSwitchController.Instance.FrameId = temp;

        _fullscreen.spriteName = "fullscreen_icon_outV2";
        Debug.Log("Regular Screen mode!");
        Camera.main.rect = new Rect(0.07f, 0.15f, 0.63f, 0.67f);
        _isFullscreen = false;
    }


    void OnClick()
    {
        //float value = SmallSlider.GetComponent<UISlider>().value;
        ////SmallSlider.GetComponent<UISlider>().value = 1.0f;
        ////SmallSlider.GetComponent<UISlider>().value = 0.0f;
        if(_isFullscreen)
        {
            MakeRegularScreen();
        }
        else
        {
            MakeFullscreen();
        }


        //SmallSlider.GetComponent<UISlider>().value = value;
    }

    private void SetNewAnchors(UIWidget spr, float l, float r, float b, float t)
    {
        spr.rightAnchor.Set(r,0);
        spr.leftAnchor.Set(l, 0);
        spr.bottomAnchor.Set(b, 0);
        spr.topAnchor.Set(t, 0);
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            if(_isFullscreen)
            {
                MakeRegularScreen();
            }
        }
    }
}
