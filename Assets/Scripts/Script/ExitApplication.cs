﻿using UnityEngine;
using System.Collections;

public class ExitApplication : MonoBehaviour
{
    public GameObject Controller;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnClick()
    {
        Controller.GetComponent<CreateNotification>().CreateOneButton("Quit?", "Are you sure you want to quit?", "Yes", "No", new EventDelegate(Application.Quit));
        //Application.Quit();
    }

}
