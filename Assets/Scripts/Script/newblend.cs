﻿using UnityEngine;
using System.Collections;

public class newblend : MonoBehaviour {

	public float angle_slider = 0.0F;

	private SkinnedMeshRenderer skinMeshRenderer;
	
	void Start () 
	{
		skinMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
	}
	
	void OnGUI() {

		angle_slider = GUI.HorizontalSlider (new Rect (25, 25, 100, 30), angle_slider, 0.0F, 100.0F);
		GUI.Label (new Rect (25, 5, 100, 20), "angle");

	}

	void Update () 
	{
		skinMeshRenderer.SetBlendShapeWeight (0, angle_slider);

	}
}
