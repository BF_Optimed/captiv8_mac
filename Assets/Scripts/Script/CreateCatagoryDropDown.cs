﻿using UnityEngine;
using System.Collections;

public class CreateCatagoryDropDown : MonoBehaviour {

	private string [] catagoryList;
	
	//the script used to fade out and move the scene to a new one
	//private BlackoutScript blackoutScript;
	//the button created by this script
	public GameObject uiObject;
	//the object the buttons are placed in, usually a table
	public GameObject parentObject;
	//the list of existing bookmarks to make
	private GameObject button; 
	public UISprite background;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	void OnEnable()
	{
		catagoryList = BFSRSwitchController.Instance.CategoryList ();
		
		for (int i = 0; i < catagoryList.Length; i++) 
		{
			button = NGUITools.AddChild(parentObject, uiObject);
			UILabel newButtonLabel = button.GetComponentInChildren<UILabel>();
			button.name = catagoryList[i];
			newButtonLabel.text = catagoryList[i];
		}

        
		int backgroundsize = parentObject.transform.childCount * 30 * -1;
		background.bottomAnchor.absolute = backgroundsize;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
