﻿using UnityEngine;
using System.Collections;

public class ActivateSearch : MonoBehaviour
{

    public GameObject Search;
    private bool _active;

    // Use this for initialization
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void OnClick()
    {
        _active = !_active;
        if (_active)
        {
            NGUITools.SetActive(Search, true);
        }
        else
        {
            NGUITools.SetActive(Search, false);
        }
    }
}
