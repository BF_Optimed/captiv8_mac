﻿using System;
using UnityEngine;
using System.Collections;

public class ActivateSyncNumber : MonoBehaviour
{
    public GameObject Background;
    public UILabel _number;
    public UISprite _background;
    public int Number;
	// Use this for initialization
	void Start ()
	{
	    _background = Background.GetComponent<UISprite>();
	    _number = gameObject.GetComponent<UILabel>();
        _number.text =  GameObject.Find("PersistentController").GetComponent<ContinuousVars>().SyncCount.ToString();
	}

    // Update is called once per frame
	void Update () 
    {
        Number = Int32.Parse(_number.text);
	    if (Number > 0)
	    {
            NGUITools.SetActive(Background, true);
	        _number.enabled = true;
	    }
	    else
	    {
            NGUITools.SetActive(Background, false);
            _number.enabled = false;
	    }
    }
}
