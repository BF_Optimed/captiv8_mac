﻿using UnityEngine;
using System.Collections;

public class DeleteSubClick : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick()
    {
        GameObject.Find("Subtitle").GetComponent<SubtitleCreator>().DeleteSubtitle(gameObject.transform.parent.gameObject);
    }
}
