﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class Hibernation : MonoBehaviour {

    private float _currentTime = 0.0f;
    private int _maxTime = 30;
    public GameObject RecordingBar;
    public bool CanHibernate;

	// Use this for initialization
	void Start () {
        CanHibernate = true;
        UpdateHibernationTime();
	}
	
	// Update is called once per frame
	void Update () {

        if (Application.loadedLevelName != "Hibernation" || Application.loadedLevelName != "VideoView")
        {
            if(CanHibernate)
            {
                _currentTime += Time.deltaTime;
            }
            else
            {
                _currentTime = 0.0f;
            }
            CheckTimerAmount();

            CheckForInput();
        }
	}

    private void CheckTimerAmount()
    {
        if (_currentTime >= _maxTime)
        {
            _currentTime = 0;
            BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
            if (Application.loadedLevelName != "Hibernation")
            {
                GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LastLevel = Application.loadedLevelName;
                Application.LoadLevel("Hibernation");
            }
        }
    }

    private void CheckForInput()
    {
        if (Input.anyKey)
        {
            _currentTime = 0;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            _currentTime = 0;
        }

        if (Input.GetAxis("Mouse X") > 0)
        {
            _currentTime = 0;
        }

        if (Input.GetAxis("Mouse Y") > 0)
        {
            _currentTime = 0;
        }
    }

    public void UpdateHibernationTime()
    {
        _currentTime = 0;
        _maxTime = PlayerPrefs.GetInt(MagicStrings.HibernationTimeKey);
        if (_maxTime < 30)
            _maxTime = 30;
    }
}
