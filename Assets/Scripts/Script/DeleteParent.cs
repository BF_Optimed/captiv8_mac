﻿using UnityEngine;

public class DeleteParent : MonoBehaviour
{
    public void DestroyParent()
    {
        NGUITools.SetActive(gameObject.transform.parent.gameObject, false);
    }
}
