﻿using UnityEngine;
using System.Collections;

public class HibernationReturn : MonoBehaviour {

    public GameObject Label;
    private UILabel _label;

	// Use this for initialization
	void Start () {
	    _label = Label.GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.anyKey)
        {
            LevelReturn();
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            LevelReturn();
        }

        if (Input.GetAxis("Mouse X") > 0)
        {
            LevelReturn();
        }

        if (Input.GetAxis("Mouse Y") > 0)
        {
            LevelReturn();
        }

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2)) 
        {
            LevelReturn();
        }
	}

    private void LevelReturn()
    {
        _label.text = "Please wait...";
        Debug.Log("Before: " + GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LastLevel);
        Application.LoadLevel(GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LastLevel);
        Debug.Log("After: " + GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LastLevel);
    }
}
