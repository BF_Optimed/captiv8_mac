﻿using UnityEngine;

public class TableResetScript : MonoBehaviour
{
    private void Update()
    {
        gameObject.GetComponent<UITable>().Reposition();
    }
}