﻿using System;
using UnityEngine;
using System.Runtime.Serialization;
[Serializable]
public class BFSRDrawingData : BFSRData
{
    #region Fields
        public Vector2 MousePosition { get; set; }
        public BFSRDrawing Drawing { get; set; }
        public int Index { get; set; }
        public int TotalNoOfDrawing { get; set; }
        public int DrawingID { get; set; }
        public int DrawInfoID { get; set; }
        public int SetID { get; set; }

        public int TransformFrameID { get; set; }
        
    #endregion

    #region default Constuctor
        public BFSRDrawingData()
        { 
        }
     #endregion
}

