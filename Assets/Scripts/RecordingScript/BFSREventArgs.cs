﻿using System;
using System.Collections.Generic;
   public class BFSREventArgs:EventArgs
    {
       private BFSRMode mode;
       List<BFSRData> recording;
       byte[] image;
       public BFSREventArgs(BFSRMode Mode)
       {
           this.mode = Mode;
       }
       public BFSREventArgs(BFSRMode Mode, List<BFSRData> recording)
       {
           this.recording = recording;
           this.mode = Mode;
       }
       public BFSREventArgs(BFSRMode Mode, List<BFSRData> recording,byte[] image)
       {
           this.recording = recording;
           this.image=image;
           this.mode=Mode;
       }
       public BFSRMode Mode
       {
           get
           {
               return mode;
           }
       }
       public List<BFSRData> Recording
       {
           get 
           {
               return this.recording;
           }
       }
       public byte[] Image
       {
            get
            {
                return this.image;
            }
       }
    }

