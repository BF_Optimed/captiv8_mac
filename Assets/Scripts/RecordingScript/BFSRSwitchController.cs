﻿using Assets.DBHelper;
/// <summary>
// <summary>
/// Author : Aizaz Ahmed.
/// Its is an controller to start,stop and play recording.
/// Handels record storage.
/// Recieve command from interface and interact with recorder and replay manager.
/// 
/// All the related object are tag with BFSR = Bright Future Software Recorder.
/// </summary>
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using UnityEngine;
public sealed class BFSRSwitchController :Assets.SynchroniseScript.SynchroniseBase<BFSRSwitchController> 
{
	private static BFSReplayManager replayManager;
	private static BFSRRecordManager recordingManager;
	private BFSRComponent component;
	private BFSRMode mode;
	private BFSRDrawing drawing;
	private static BFSRSwitchController instance;
	private static BFSRStorage storage;
	private const string BFS_RECORDER_TAG = "BFSRecorder";
	private const string BFS_OBJECT_TAG = "Main";
	private const string BFS_PLAYINGSLIDER_TAG = "BFSPlayingSlider";
    /// <summary>
    /// 
    /// </summary>
    public event StartEventHandler StartEvent;
    public event StopEventHandler Stop;
    public event OnFinishEventHandler finish;
    void Start()
	{
		storage = new BFSRStorage();
        component = BFSRComponent.Transformation;
        mode = BFSRMode.Stop;
		replayManager = GameObject.FindGameObjectWithTag(BFS_RECORDER_TAG).GetComponent<BFSReplayManager>();
		recordingManager = GameObject.FindGameObjectWithTag(BFS_RECORDER_TAG).GetComponent<BFSRRecordManager>();
	}
	/// <summary>
	/// Get the shared instance of BFSRSwitchController
	/// </summary>
    
	public static BFSRSwitchController Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<BFSRSwitchController>();
                try
                {
                    DontDestroyOnLoad(instance.gameObject);
                }
                catch{}
			}
			
			return instance;
		}
	}

    public void OnFinish()
    {
        if (!ReferenceEquals(finish, null))
            finish();
    }

	public BFSRDrawing Drawing
	{
		get
		{
			return drawing;
		}
	}
    //void Awake()
    //{
    //    if (instance == null)
    //    {
    //        //If I am the first instance, make me the Singleton
    //        instance = this;
    //        DontDestroyOnLoad(this);
    //        initRecorderSwitchController();
    //    }
    //    else
    //    {
    //        //If a Singleton already exists and you find another reference in scene, destroy it!
    //        if (this != instance)
    //            Destroy(this.gameObject);
    //    }
    //}
	/// <summary>
	/// Set the property for mode.
	/// </summary>
	public BFSRMode Mode
	{  
		set
		{
			Debug.Log("Current mode is has changed from " + mode + " to " + value);
			BFSRMode previous = mode;
			mode = value;
			ModeHandler(previous, value);
		}
		get
		{
			return mode;
		}
	}

    public void CaptureImage(string ImageLocation, Camera camera, int width, int height, Action callBack)
    {
        StartCoroutine(replayManager.CaptureImage(ImageLocation, camera, width, height, callBack));
    }
	/// <summary>
	/// Set the property for Recording Component
	/// </summary>
	public BFSRComponent Component
	{
		set
		{
			BFSRComponent previous = component;
			component = value;
			ComponentHandler(previous, component);
		}
	}
    public int FrameId
    {
        get
        {
            return replayManager.FrameId;
        }
        set
        {
            replayManager.FrameId = value;
        }
    }
	/// <summary>
	/// Handles the different mode whether to start,stop,pause and play.
	/// </summary>
	/// <param name="PreviousMode">Previous mode of BFSRMode</param>
	/// <param name="CurrentMode">Current mode of BFSRMode</param>
	private void ModeHandler(BFSRMode PreviousMode, BFSRMode CurrentMode)
	{
        if(replayManager==null) replayManager = GameObject.FindGameObjectWithTag(BFS_RECORDER_TAG).GetComponent<BFSReplayManager>();
        if (recordingManager == null)  recordingManager = GameObject.FindGameObjectWithTag(BFS_RECORDER_TAG).GetComponent<BFSRRecordManager>();
		switch (mode)
		{
		case BFSRMode.Record:
			if(PreviousMode==BFSRMode.Stop && recordingManager.enabled==false)
			{
				recordingManager.enabled = true;
				recordingManager.StartNewRecording(component);
			}
			else if(PreviousMode==BFSRMode.Pause && recordingManager.enabled==true)
			{
				recordingManager.PauseRecording(component);
			}
			break;
		case BFSRMode.Stop:
			if (recordingManager.enabled)
			{ 
				recordingManager.StopRecording();
				recordingManager.enabled = false;
			}
			else if (replayManager.enabled)
			{
				replayManager.StopPlaying();
				replayManager.enabled = false;
			}
			break;
		case BFSRMode.Pause:
			if (recordingManager.enabled)
				recordingManager.PauseRecording(component);
			if (replayManager.enabled)
				replayManager.PauseReplay();
                OnFinish();
			break;
		case BFSRMode.Play:
			if((PreviousMode==BFSRMode.Record || PreviousMode ==BFSRMode.Pause)&&recordingManager.enabled)
			{
				recordingManager.StopRecording();
				recordingManager.enabled = false;
                replayManager.enabled = true;
                BFSREventArgs e = new BFSREventArgs(BFSRMode.Play);
                OnStart(e);
                
                
			}
			else if ((PreviousMode == BFSRMode.Pause || PreviousMode==BFSRMode.Play ) && replayManager.enabled)
			{
				replayManager.PauseReplay();
			}
			else
			{
				replayManager.enabled = true;
                BFSREventArgs e = new BFSREventArgs(BFSRMode.Play);
                OnStart(e);
			}
			
			break;
		}
	}
    public string LabelUpdate(float value)
    {
        if(replayManager.enabled)
        {
            return replayManager.LabelUpdate(value);
        }
        return "00:00:000";
    }
    private void OnStart(BFSREventArgs e)
    {
        if (StartEvent != null)
        {
            StartEvent(e);
        }
    }
    private void OnStop(BFSREventArgs e)
    {
        if (Stop != null)
        {
            Stop(e);
        }
    }
	private void ComponentHandler(BFSRComponent PreviousComponent, BFSRComponent CurrentComponent)
	{
		if((mode==BFSRMode.Record || mode==BFSRMode.Pause) && recordingManager.enabled)
		{
			recordingManager.ChangeReordingComponent(CurrentComponent);
		}
	}
    public void PlayData(List<BFSRData>  recording)
    {
        //Debug.Log("Passing Data to replay manager 0 :" + recording.Count);
        if (recording != null && recording.Count > 0)
        {
            //Debug.Log("Passing Data to replay manager");
            replayManager.PlayRecording(recording);
        }
    }
    public int VideoLength()
    {
        //Debug.Log("Switch Controller Length is :" + replayManager.VideoLength());
        if (replayManager.enabled)
            return replayManager.VideoLength();
        return 1;
    }
	/// <summary>
	/// List the name of category created.
	/// </summary>
	/// <returns>Category name array of type string </returns>
	public string[] CategoryList()
	{
		return storage.CategoryList();
	}
    public IEnumerable RecordingList(string CategoryName = "Other")
	{
        if(storage==null) storage=new BFSRStorage();
		return storage.RecordingList(CategoryName);
	}
    public void GetSliderValue(string lowerTime,string higherTime,Action<float,string,float,string> callback)
    {
        if (replayManager.enabled)
        {
            StartCoroutine( replayManager.SliderValueOnTime(lowerTime,higherTime, callback));
        }
    }
	public bool IsRecordingExist()
	{
		return storage.IsRecordingExist();
	}
    public void SaveToFile(List<BFSRData> recording, string FileName, string Category = "Other", string Discription = "None")
	{
        storage.SaveRecordingToDisk(recording,FileName, Category, Discription);
	}
    public void ModifyFile(List<BFSRData> recording, string path)
    {
        storage.ModifyVideo(recording, path);
    }
    /// <summary>
    /// Load recording from the filename specified.
    /// </summary>
    /// <param name="FileName">string location of file including file name.</param>
	public List<BFSRData> ReadFromFile(string FileName)
	{
		return storage.ReadFromDisk(FileName);
	}
	/// <summary>
	/// Call when the model on current scence is changed.
	/// </summary>
	/// <param name="ModelName">Name of the model in string</param>
	public void ManageNewModel(string ModelName)
	{
		if (mode == BFSRMode.Record)
		{
			recordingManager.CurrentObjectName = ModelName;
		}
	}
    public void AnimationToRecord(string AnimationName,float normalizeTime)
    {
        if (recordingManager != null)
        {
            if (recordingManager.enabled)
                recordingManager.AnimationToRecord(AnimationName, normalizeTime);
        }
    }
    public void AddTimerLabelToReplay(UILabel label)
    {
        replayManager.TimerLabel = label;
    }
    /// <summary>
    /// Pass the object UISlider to replay manager.
    /// </summary>
    /// <param name="slider">UISlider for replay manager</param>
	public void AddSliderToReplay(UISlider slider)
    {
        if (replayManager.enabled)
        {
            replayManager.HigherSlider = slider;
        }
    }
    /// <summary>+
    /// Info record manager that the drawing type has changed.
    /// </summary>
    /// <param name="drawing"> Drawing type</param>
	public void ManageNewDrawing(BFSRDrawing drawing)
	{
		this.drawing = drawing;
		if (mode == BFSRMode.Record)
		{
			recordingManager.Drawing = drawing;
		}
		
	}
	/// <summary>
	/// Instruct the switch controller to save the recording in cache.
	/// <remarks>This may override your previous recording.</remarks>
	/// </summary>
	/// <param name="RecordedData"> Recording data to save temporary. </param>
	public void SaveToCache(List<BFSRData> RecordedData, byte[] image)
	{
        BFSREventArgs e = new BFSREventArgs(BFSRMode.Record, RecordedData, image);
        OnStop(e);   
	}
    /// <summary>
    /// Creates categories.
    /// </summary>
    /// <param name="categories">Arrays of category to create </param>
	public void CreateCategories(string[] categories)
	{
		storage.CreateCategories (categories);
	}
    public void FinishPlayingData(List<BFSRData> RecordedData)
    {
        BFSREventArgs e = new BFSREventArgs(BFSRMode.Play, RecordedData);
        OnStop(e);
    }
    public static int NumberOfFrames2Play()
    {
        if (replayManager.enabled)
            return (int) replayManager.NumberOfFrames2Play;
        return 0;
    }
    public void RemoveFrame(string  fromTime, string toTime,Action callback)
    {
        if (replayManager.enabled)
           StartCoroutine(replayManager.RemoveFrame(fromTime, toTime,callback));
        else
            throw new System.InvalidOperationException("Invalid operation on replay manager");
    }
    public float GetNewSliderValue(float value)
    {
        if (replayManager.enabled)
        {
            return replayManager.GetNewSliderValue(value);
        }
        return 0.0f;
    }
    public string TimerValueOnFrame(int frameNo)
    {
        if (replayManager.enabled)
            return replayManager.TimerValueOnFrame(frameNo);
        else
            return ""+ 0.0f;
    }

    public void DeleteRecording(string videoId)
    {
        try
        {
            var video = new Video().GetVideo(videoId);
            if (video.Owner == Owner.captiv8plus)
            {
                DirectoryInfo dir = new DirectoryInfo(Path.GetDirectoryName(video.XmlPath));
                dir.Delete(true);
            }
            video.Delete();
        }
        catch
        {
        }
    }

    public void AddSprite(UISprite sprite)
    {
        if(replayManager.enabled)
        {
            replayManager.Sprite = sprite;
        }
    }
}
