using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Vectrosity;

public class BFSRRecordManager : MonoBehaviour
{
	private List<BFSRData> recordingdata = new List<BFSRData>();
	private BFSRMode Mode = BFSRMode.Stop;
	private BFSRComponent component = BFSRComponent.Default;
    private byte[] ThumnailImage;
    private bool IsImageCaptured = false;
	//int frame = 0;
	/// <summary>
	/// Objects for recording transformation. 
	/// </summary>
	private Transform cameraTransform;
	private Transform objectTransform;
	private const string BFS_MAINCAMERA_TAG = "MainCamera";
	private const string BFS_OBJECT_TAG = "Main";
	private const string BFS_PLAYINGSLIDER_TAG = "BFSPlayingSlider";
    private const string BFS_PIVOT_TAG = "Pivot";
    private const string BFS_CONTROLLER_TAG = "GeoController";
    private Transform controllerTransform;
    private Transform pivotTransform;
	public static string currentObjectName;
	
	/// <summary>
	/// Objects for recording drawing.
	/// </summary>
	private BFSRDrawing drawing;
	private bool canDraw;
	private int maxPoints = 1000;
	private int lineIndex = 0;
	private Vector2 previousPosition;
	private int sqrMinPixelMove;
	private int CurrentLineNoOnFrame = 0;
    private int totalNumberOfLineInSet = 0;
	private const string BFS_GUICONTROLLER_TAG = "BtnController";
	private ColorSelection colorSelected;
    private BFSDLineWidth lineWidth;
	private int DrawingInfoID = 0;
	public int minPixelMove = 5;
	private int FrameID = 0;
	private int setID = 0;
	private bool isTrans4LineRecorded = false;
	private int Trans4LineRecordedID = 0;
    private int oldWidth;

    private string AnimationName = string.Empty;
    private float NormalizeTime = 0.0f;
	/// <summary>
	/// Object name is an model name whose position and transformation to record.
	/// When ever the model is chaged while recording this need to inform the record
	/// manager.
	/// </summary>
	public string CurrentObjectName
	{
		set
		{ 
			currentObjectName = value;
			objectTransform = GameObject.Find(value).GetComponent<Transform>();
		}
	}
    public bool _isPivoting=false;

    public bool IsPivoting{ 
        set{_isPivoting=value;} 
    }

	public BFSRDrawing Drawing
	{
		set
		{
			drawing = value;
		}
	}


	/// <summary>
	/// 
	/// </summary>
	void Update()
	{
		switch (Mode)
		{
               
		case BFSRMode.Record:
			switch (component)
			{ 
				
			case BFSRComponent.Transformation:
				    FrameID++;
				    BFSRTransformationData data = new BFSRTransformationData();
                        if(!string.IsNullOrEmpty(AnimationName))
                        {
                            data.Animation = AnimationName;
                            data.NormalizedTime = NormalizeTime;
                            AnimationName = string.Empty;
                        }
				    data.ObjectName = objectTransform.name;
				    data.CameraPosition = cameraTransform.transform.position;
				    data.CameraRotation = cameraTransform.transform.rotation;
				    data.ObjectPostion = objectTransform.position;
				    data.ObjectRotation = objectTransform.rotation;
				    data.ObjectScale = objectTransform.localScale;
				    data.FieldOfView = Camera.main.fieldOfView;
				    data.FrameID = FrameID;
                    data.Deleted = false;
                    data.Time = Time.deltaTime;
				    recordingdata.Add(data);
				break;
			case BFSRComponent.Drawing:
				FrameID++;
				Vector2 mousePos = Input.mousePosition;
				BFSRDrawingData drawingData = new BFSRDrawingData();
				if (!isTrans4LineRecorded)
				{
					BFSRTransformationData tData = new BFSRTransformationData();
					tData.ObjectName = objectTransform.name;
					tData.CameraPosition = cameraTransform.transform.position;
					tData.CameraRotation = cameraTransform.transform.rotation;
					tData.ObjectPostion = objectTransform.position;
					tData.ObjectRotation = objectTransform.rotation;
					tData.ObjectScale = objectTransform.localScale;
					tData.FieldOfView = Camera.main.fieldOfView;
					tData.FrameID = FrameID;
                    tData.Deleted = false;
                    tData.Time = Time.deltaTime;
					recordingdata.Add(tData);
					Trans4LineRecordedID = FrameID;
					isTrans4LineRecorded = true;
				}
				if (Input.GetMouseButtonDown(0))
				{
					
					BFSRDrawingInfo drawingInfo = new BFSRDrawingInfo();
					drawingInfo.Color = colorSelected.CurrentColor;
                    drawingInfo.ScreenWidth = (float)Camera.main.pixelRect.width;
                    drawingInfo.ScreenHeight = (float)Camera.main.pixelRect.height;
                    drawingInfo.Width = lineWidth.GetCurrentLineWidth();
                    if (GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<lineDrawMouse>().enabled)
                    {
                        drawingInfo.Material = GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<lineDrawMouse>().LineMaterial;
                    }
					drawingInfo.TransformID = Trans4LineRecordedID;
					drawingInfo.id = ++DrawingInfoID;
					recordingdata.Add(drawingInfo);
					previousPosition = mousePos;
					//drawingData.DrawingID = ++CurrentLineNoOnFrame;
					canDraw = true;
				}
				switch (drawing)
				{ 
				case BFSRDrawing.Line:
					if (Input.GetMouseButtonDown(0))
					{
                        Debug.Log("New Line created");
                        drawingData.DrawingID = ++CurrentLineNoOnFrame;
						drawingData.Index = lineIndex = 0;
					}
					else if (Input.GetMouseButton(0) && (mousePos - previousPosition).sqrMagnitude > sqrMinPixelMove && canDraw)
					{
						previousPosition = mousePos;
						drawingData.DrawingID = CurrentLineNoOnFrame;
						drawingData.Index = ++lineIndex;
						if (lineIndex >= maxPoints - 1) canDraw = false;
					}
					drawingData.Drawing = BFSRDrawing.Line;
					break;
				case BFSRDrawing.Square:
					if (Input.GetMouseButtonDown(0))
					{
                        Debug.Log("New Box Created");
						drawingData.Index = lineIndex = 1;
						drawingData.DrawingID = ++CurrentLineNoOnFrame;
                        //totalNumberOfLineInSet++;
					}
					if (Input.GetMouseButton(0))
					{
						drawingData.DrawingID = CurrentLineNoOnFrame;
						drawingData.Index = ++lineIndex;
					}
                    if (Input.GetMouseButtonUp(0))
                    {
                        drawingData.Index = lineIndex = 0;
                    }
                    if (Input.GetMouseButtonDown(1))
                    {
                        Debug.Log("Box Deleted");

                    }
					drawingData.Drawing = BFSRDrawing.Square;
					break;
				case BFSRDrawing.Circle:
					if (Input.GetMouseButtonDown(0))
					{
						drawingData.Index = lineIndex = 1;
						drawingData.DrawingID = ++CurrentLineNoOnFrame;
					}
					if (Input.GetMouseButton(0))
					{
						drawingData.DrawingID = CurrentLineNoOnFrame;
						drawingData.Index = ++lineIndex;
					}
                    if (Input.GetMouseButtonUp(0))
                    {
                        drawingData.Index = lineIndex = 0;
                    }
                    if (Input.GetMouseButtonDown(1))
                    {
                        Debug.Log("Circle Deleted");
                        //CurrentLineNoOnFrame=CurrentLineNoOnFrame-1;
                    }
					drawingData.Drawing = BFSRDrawing.Circle;
					break;
				}
				
				drawingData.TransformFrameID = Trans4LineRecordedID;
				drawingData.SetID = setID;
				drawingData.FrameID = FrameID;
				drawingData.DrawInfoID = DrawingInfoID;
				drawingData.TotalNoOfDrawing = CurrentLineNoOnFrame;
                drawingData.MousePosition = mousePos;
                drawingData.Time = Time.deltaTime;
                drawingData.Deleted = false;
				recordingdata.Add(drawingData);
				break;
			case BFSRComponent.Default:
				break;
			}
			break;
		case BFSRMode.Pause:
			break;
		}
	}
    public void AnimationToRecord(string animationName, float normalizeTime)
    {
        this.AnimationName = animationName;
        this.NormalizeTime = normalizeTime;
    }
	/// <summary>
	/// Start new recording with BFSRecordmanager.
	/// </summary>
	/// <remarks>This action will clear the previous recorded data.</remarks>
	/// <param name="component">component that the recorder should record.</param>
	public void StartNewRecording(BFSRComponent Component)
	{
        FrameID = setID = Trans4LineRecordedID = lineIndex =DrawingInfoID=CurrentLineNoOnFrame= 0;
        IsImageCaptured = false;
		PrepareObjectsForRecording(component);
		recordingdata.Clear();
		this.component = Component;
        Application.targetFrameRate = 30;
		Mode = BFSRMode.Record;
		Debug.Log("Record manager has recived command to record");
        
	}
	/// <summary>
	/// Change the component while recording.
	/// </summary>
	/// <param name="Component">Component to change.</param>
	public void ChangeReordingComponent(BFSRComponent Component)
	{
		PrepareObjectsForRecording(component);
		this.component = Component;
	}
	
	/// <summary>
	/// Stop the BFSRecordManager from recording data.
	/// <remarks>This will automatically store recorded data to cache.</remarks>
	/// </summary>
	public void StopRecording()
	{
		Debug.Log("Record manager has recived command to stop record");
		Mode = BFSRMode.Stop;
		BFSRSwitchController.Instance.SaveToCache(recordingdata.ToList<BFSRData>(), ThumnailImage);
		recordingdata.Clear();
	}
	/// <summary>
	/// Alter the recording from stop to pause or vice versa.
	/// </summary>
	public void PauseRecording(BFSRComponent Component)
	{
		if (Mode == BFSRMode.Record)
		{
			Debug.Log("Record manager has recived command to pause");
			Mode = BFSRMode.Pause;
		}
		else if (Mode == BFSRMode.Pause)
		{
			Debug.Log("Record manager has recived command to start recording again");
			PrepareObjectsForRecording(Component);
			Mode = BFSRMode.Record;
		}
	}
	/// <summary>
	/// Intialize objects for recording. 
	/// </summary>
	/// <param name="Component">Component whose object to intialized.</param>
	private void PrepareObjectsForRecording(BFSRComponent Component)
	{
		cameraTransform = GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<Transform>();
		objectTransform = GameObject.FindGameObjectWithTag(BFS_OBJECT_TAG).GetComponent<Transform>();
        pivotTransform = GameObject.FindGameObjectWithTag(BFS_PIVOT_TAG).GetComponent<Transform>();
        controllerTransform = GameObject.FindGameObjectWithTag(BFS_CONTROLLER_TAG).GetComponent<Transform>();
		currentObjectName = objectTransform.name;
		colorSelected = GameObject.FindGameObjectWithTag(BFS_GUICONTROLLER_TAG).GetComponent<ColorSelection>();
        lineWidth = GameObject.FindGameObjectWithTag(BFS_GUICONTROLLER_TAG).GetComponent<BFSDLineWidth>();
		sqrMinPixelMove = minPixelMove * minPixelMove;
		if (component == BFSRComponent.Drawing)
		{
			drawing = BFSRSwitchController.Instance.Drawing;
			isTrans4LineRecorded = false;
			setID++;
		}
	}

    
}
