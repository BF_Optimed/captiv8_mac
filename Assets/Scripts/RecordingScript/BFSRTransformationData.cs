/// <summary>
/// Author : Aizaz Ahmed.
/// BFSRTransformationData is data type extended  from BFSRdata.
/// Caplable to store transformation of one camera and one model at a time.
/// </summary>
using System;
using System.Runtime.Serialization;
using UnityEngine;
[Serializable]
public class BFSRTransformationData : BFSRData
{
    /// <summary>
    /// Name of the model to store.
    /// </summary>
    [SerializeField]
    public string ObjectName
    { set; get; }
    /// <summary>
    /// Camera position in form of vector3
    /// </summary>
    [SerializeField]
    public Vector3 CameraPosition
    { set; get;}
    /// <summary>
    /// Camera rotation in form of Quaternion
    /// </summary>
    public Quaternion CameraRotation { set; get; }
    [SerializeField]
    public Vector3 ObjectScale { set; get; }
    [SerializeField]
	public float FieldOfView { set; get;}
    [SerializeField]
    public float blendShape1Value { get; set; }
    [SerializeField]
    public Vector3 ObjectPostion { get; set; }
    [SerializeField]
    public Quaternion ObjectRotation { set; get; }
    public string Animation { get; set; }
    public float NormalizedTime { get; set; }
    /// <summary>
    /// Empty construtor required for XMLSerialization 
    /// </summary>
    public BFSRTransformationData()
    {
    }
}

