﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

 public class BFSRDrawingInfo : BFSRData
{
     public int id { get; set; }
     public float Width { get; set; }
     public Color Color { get; set; }
     public string Material { get; set; }
     public int TransformID { get; set; }

     public float ScreenHeight { get; set; }

     public float ScreenWidth { get; set; }
}

