﻿public delegate void StartEventHandler(BFSREventArgs e);
public delegate void StopEventHandler(BFSREventArgs e);
public delegate void OnFinishEventHandler();