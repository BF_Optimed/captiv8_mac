﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Assets.DBHelper;
public class BFSRStorage
{
	private  List<BFSRData> RecordedData = new List<BFSRData>();
	private byte[] Thumbnail;
    private List<string> category;
	/// <summary>
	/// Save the recording in cache.
	/// <remarks>This may override your previous recording.</remarks>
	/// </summary>
	/// <param name="RecordingData"> Recording data to save temporary. </param>
	/// <param name="ImagePath">Temp path of image.</param>
	public void SaveToCache(List<BFSRData> RecordingData, byte[] image)
	{
		RecordedData = RecordingData;
		Thumbnail = image;
	}
	public List<BFSRData> PlayCurrentFile()
	{
        if (RecordedData != null)
        {
            List<BFSRData> tempRecordData = new List<BFSRData>(RecordedData);
            return tempRecordData;
        }
        return null;
	}
	/// <summary>
	/// Save the cache recording data to the disk.
	/// </summary>
	/// <param name="FileName">Name of the file.</param>
	/// <param name="Category">Category the recording to save with, its an optional parameter. By default it save to Optimized.</param>
	/// <param name="Discription">Discription for the recording,its an optional parameter. By default it save to None.</param>
	/// <exception cref=""></exception>
    public void SaveRecordingToDisk(List<BFSRData> recording, string fileName, string Category, string Discription)
    {
        if (string.IsNullOrEmpty(fileName) && recording != null && recording.Count > 0)
        {
            throw new System.ArgumentNullException("File name and Recording is required to store recording.");
        }
        else
        {
            if (!checkedValidation())
            {
                throw new Exception("Animations creation limit has reached.");
            }
            
           
            var video = new Video()
            {
                Name = fileName,
                Category = Category,
                DateCreated = DateTime.Now,
                Duration =(int) recording.Where(x => x.Deleted == false).Sum(x => x.Time),
                Description = Discription,
                Owner=Owner.captiv8plus,
                Status=Status.Local
            };
            video.SaveNewVideo();
            var location = GetLocation(Category, video.Id);
            DirectoryValidator(location);
            //video.XmlPath = ;
            video.UpdateVideo(AnimationModel.AnimationCoordinates,SaveXmlFile(location, video.Id, recording));
        }
    }

    private bool checkedValidation()
    {
      return  new Video().GetAllVideos("Owner ='" + Owner.captiv8plus.ToString() + "' AND Status In ('" + Status.Completed + "','" + Status.Local + "')").Count <= 40;
    }

    public void ModifyVideo(List<BFSRData> recording,string path)
    {
        SaveObject(recording, path);
    }

    private string SaveXmlFile(string location, string fileName,List<BFSRData> recording)
    {
        var XmlPath = Path.ChangeExtension(Path.Combine(location, fileName), "xml");
        SaveObject(recording,XmlPath);
        return XmlPath;
    }
    private string SaveImage(string location, string fileName, byte[] Image)
    {
        if (Image == null) return null;
        var PngPath = Path.ChangeExtension(Path.Combine(location, fileName), "png");
        File.WriteAllBytes(PngPath, Image);
        return PngPath;
    }
    private string GetLocation(string category, string fileName)
    {
        return Path.Combine( Path.Combine(Application.persistentDataPath, category),fileName);
    }
    private void DirectoryValidator(string location)
    {
        if (!Directory.Exists(location))
        {
            Directory.CreateDirectory(location);
        }
    }
	public bool IsRecordingExist()
	{
        try
        {
            if (this.RecordedData.Count > 0)
                return true;
            return false;
        }
        catch
        {
            return false;
        }
	}
	public List<BFSRData> ReadFromDisk(string FileName)
	{
        Debug.Log(FileName);
		if (string.IsNullOrEmpty (FileName)) {
			throw new System.MissingFieldException ("File name is required to read recording.");
		} 
        else
        {
			var xmldata = new List<BFSRData> ();
			GetObject (ref xmldata, FileName);
			return xmldata;
		}
	}
	/// <summary>
	/// Serialize the recorded data into XML and stores to disk on specified location.
	/// </summary>
	/// <typeparam name="T">List of recorded data.</typeparam>
	/// <param name="optimizedData">List of recorded data.</param>
	/// <param name="location">Full location of file to store.</param>
	/// <returns> Result of xml serialization.</returns>
	private static bool SaveObject<T>(T optimizedData, string location)
	{
		try
		{
			var optimizedtype = new XmlSerializer(optimizedData.GetType());
			using (var Writer = new StreamWriter(location, false))
			{
				optimizedtype.Serialize(Writer, optimizedData);
			}
			return true;
		}
		catch (Exception ex)
		{
			UnityEngine.Debug.Log("error : " + ex.ToString());
			return false;
		}
	}
	/// <summary>
	/// Deserialize the XML into recording list from specified location and stores to object provided.
	/// </summary>
	/// <typeparam name="T">Deserialize object to store.</typeparam>
	/// <param name="optimizedData">Deserialize object to store. </param>
	/// <param name="location">Full location of file to retrive.</param>
	/// <returns>Result of xml deserialization.</returns>
	public static bool GetObject(ref List<BFSRData> optimizedData, string location)
	{
		try
		{
			using (FileStream stream = new FileStream(location, FileMode.Open))
			{
				XmlTextReader reader = new XmlTextReader(stream);
                var x = new XmlSerializer(typeof(List<BFSRData>));
                optimizedData = (List < BFSRData >) x.Deserialize(reader);
				UnityEngine.Debug.Log("Saved to object from XML");
				return true;
			}
		}
		catch (Exception e)
		{
			UnityEngine.Debug.Log("error : " + e.ToString());
		}
		return false;
	}
	/// <summary>
	/// List the name of category created.
	/// </summary>
	/// <returns>Category name array of type string </returns>
	public  string[] CategoryList()
	{
        if(ReferenceEquals(category,null))
        {
            category = new List<string>(new Category().GetCategories());
        }
	    return category.ToArray();
	}

	public void CreateCategories (string[] categories)
	{
		foreach(string category in categories)
		{
			string location = Path.Combine(Application.persistentDataPath, category);

			if(!Directory.Exists(location))
			{
				Directory.CreateDirectory(location);
			}
		}
	}
    public List<BFSRData> ReLoadRecording()
    {
        if (RecordedData != null)
        {
            List<BFSRData> tempRecordData = new List<BFSRData>(RecordedData);
            return tempRecordData;
        }
        return null;
    }
	public IEnumerable RecordingList(string CategoryName)
	{
        var video = new Video();
        if (CategoryName.ToLower().Equals(Owner.captiv8.ToString()))
        {
            return video.GetCaptiv8Video();
        }
        return video.GetVideos(CategoryName);
	}
	public int VideoLength()
	{
		if(RecordedData !=null && RecordedData.Count>0)
		{
			var length = RecordedData.Max(x=>x.Time);
			return (int) length ;
		}
		return 0;
	}
}