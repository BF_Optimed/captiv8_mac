﻿
/// <summary>
///  Author : Aizaz Ahmed.
/// Its is an controller to start,stop and play recording
/// </summary>
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using Vectrosity;
public class BFSReplayManager : MonoBehaviour
{
    private List<BFSRData> replayData = new List<BFSRData>();
    private List<BFSRDrawingInfo> drawingInfo;
    private BFSRMode mode = BFSRMode.Stop;
    private BFSRComponent component = BFSRComponent.Default;
    private int frameNumber;
    /// <summary>
    /// Objects for playing transformation. 
    /// </summary>
    private Transform cameraTransform;
    private Transform objectTransform;
    private Transform controllerTransform;
    private Transform pivotTransform;
    private const string BFS_MAINCAMERA_TAG = "MainCamera";
    private const string BFS_OBJECT_TAG = "Main";
    private const string BFS_PLAYINGSLIDER_TAG = "BFSPlayingSlider";
    private const string BFS_PIVOT_TAG = "Pivot";
    private const string BFS_CONTROLLER_TAG="GeoController";
    public static string currentObjectName;
    private const string visionSimulator = "Vision Simulator";
    private const string BFS_ROOMANIMATOR_TAG = "RoomAnimator";
    /// <summary>
    /// Objects for playing Drawings.
    /// </summary>
    private Dictionary<int, VectorLine> Drawings;
    public Material Continuousline;
    public Material DottedLine;
    
    /// <summary>
    /// Slider objects
    /// </summary>
    public long NumberOfFrames2Play; 
    private UISlider playingSlider;
    private UILabel timerLabel;
    private UISprite _sprite;
    private const string playSprite = "play";
    private const string pauseSprite = "pause";
    private bool IsTimeSub=false;
    private int width = 1024;
    private int height = 768;

    /// <summary>
    /// Set the Slider for controlling replay
    /// </summary>
    public int ImageFrameId { set; private get; }
    public UISlider HigherSlider
    {
        set
        {
            playingSlider = value;
            frameNumber = (int)Math.Round(playingSlider.value * NumberOfFrames2Play);
        }
    }
    public int FrameId
    {
        get
        {
            return frameNumber;
        }
        set
        {
            frameNumber = value;
        }
    }
    public bool IsMovieCapture { get; set; }
    private BFSRMode Mode
    {
        set
        {
            mode = value;
            if(_sprite!=null)
            {
                if (mode == BFSRMode.Play)
                {
                    _sprite.spriteName = pauseSprite;
                }
                if (mode == BFSRMode.Pause)
                {
                    _sprite.spriteName = playSprite;
                }
            }
            
        }
        get
        {
            return mode;
        }
    }
   
    public UISprite Sprite
    {
        set
        {
            _sprite = value;
        }
    }
    public UILabel TimerLabel
    {
        set
        {
            timerLabel = value;
            timerLabel.text = GetTime(frameNumber);
        }
    }
    void LateUpdate()
    {
        switch (Mode)
        {
            case BFSRMode.Play:
            case BFSRMode.Pause:
                if (replayData.Count > frameNumber)
                {

                    BFSRTransformationData replayTransformation = replayData[frameNumber] as BFSRTransformationData;
                    BFSRDrawingData replayDrawing = replayData[frameNumber] as BFSRDrawingData;

                    //Playing Transformation
                    if (replayTransformation != null)
                    {
                        //Timer Value calculations
                        DisplayTimer(replayTransformation.FrameID);

                        //Deletes drawing  
                        if (Drawings.Count > 0)
                        {
                            VectorLine.Destroy(Drawings.Values.ToArray<VectorLine>());
                            Drawings.Clear();
                        }
                        //Checking if model is changed.
                        SetTransformOject(currentObjectName, replayTransformation.ObjectName);

                        //Appling camera transformation
                        cameraTransform.transform.rotation = replayTransformation.CameraRotation;
                        cameraTransform.transform.position = replayTransformation.CameraPosition;
                        //Applying transformation to objects
                        objectTransform.transform.localScale = replayTransformation.ObjectScale;
                        objectTransform.position = replayTransformation.ObjectPostion;
                        objectTransform.rotation = replayTransformation.ObjectRotation;
                        Camera.main.fieldOfView = replayTransformation.FieldOfView;

                        //Appling Animation
                        if (!string.IsNullOrEmpty(replayTransformation.Animation))
                        {
                            GetAnimator().Play(replayTransformation.Animation, 0, replayTransformation.NormalizedTime);
                        }

                    }
                    //Playing Drawings
                    else if (replayDrawing != null)
                    {
                        //Timer Value calculations
                        DisplayTimer(replayDrawing.FrameID);
                        if (replayDrawing.TotalNoOfDrawing > 0)
                        {
                            //Debug.Log("Total Number of box " + replayDrawing.TotalNoOfDrawing);
                            //Deletes all drawing. changes where applied for slider to support backward and forward 
                            if (Drawings != null || Drawings.Count > 0)
                            {
                                VectorLine.Destroy(Drawings.Values.ToArray<VectorLine>());
                                Drawings.Clear();
                            }
                            //Draws all the drawing accept the current on going drawing...
                            RedrawDrawingObjects(replayDrawing.TotalNoOfDrawing - 1, replayDrawing.FrameID, replayDrawing.SetID);
                            VectorLine drawObject;
                            Vector2[] drawingPoints;

                            //Setting Transformation for current Drawing
                            var trans = from transdata
                                                   in replayData.OfType<BFSRTransformationData>()
                                                   .Where(s => s.FrameID == replayDrawing.TransformFrameID)
                                        select transdata;
                            foreach (BFSRTransformationData lineTrans in trans)
                            {

                                SetTransformOject(currentObjectName, lineTrans.ObjectName);
                                cameraTransform.transform.rotation = lineTrans.CameraRotation;
                                cameraTransform.transform.position = lineTrans.CameraPosition;
                                objectTransform.transform.localScale = lineTrans.ObjectScale;
                               
                                objectTransform.position = lineTrans.ObjectPostion;
                                objectTransform.rotation = lineTrans.ObjectRotation;
                                Camera.main.fieldOfView = lineTrans.FieldOfView;
                                break;
                            }

                            BFSRDrawing drawing = DrawingType(replayDrawing.TotalNoOfDrawing);
                            drawingPoints = DrawingPoints(replayDrawing.TotalNoOfDrawing, replayDrawing.FrameID, replayDrawing.SetID);

                            //Making the ongoing drawing
                            if(drawingPoints != null)
                            if (drawingPoints.Length > 1)
                            {
                                if (Drawings.ContainsKey(replayDrawing.DrawingID))
                                {
                                    drawObject = Drawings[replayDrawing.DrawingID];
                                }
                                else
                                {
                                    drawObject = NewDrawing(replayDrawing.TotalNoOfDrawing, drawingPoints);
                                    Drawings.Add(replayDrawing.TotalNoOfDrawing, drawObject);
                                }
                                switch (drawing)
                                {
                                    case BFSRDrawing.Line:
                                        drawObject.maxDrawIndex = drawingPoints.Length - 1;
                                        drawObject.Draw();
                                        break;
                                    case BFSRDrawing.Square:
                                        drawObject.MakeRect(drawingPoints[0], drawingPoints[drawingPoints.Length - 1]);
                                        drawObject.Draw();
                                        break;
                                    case BFSRDrawing.Circle:
                                        drawObject.MakeCircle(drawingPoints[0], drawingPoints[drawingPoints.Length - 1].x - drawingPoints[0].x, 30, 0, 0);
                                        drawObject.Draw();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            if (Drawings != null || Drawings.Count > 0)
                            {
                                VectorLine.Destroy(Drawings.Values.ToArray<VectorLine>());
                                Drawings.Clear();
                            }

                        }
                    }
                    if(ImageFrameId==frameNumber && IsMovieCapture)
                    {
                        string imageLocation= GameObject.Find("BFSRecorder").GetComponent<BFSRenderer>().GetImageLocation();
                        StartCoroutine(CaptureImage( imageLocation, Camera.main, Screen.width, Screen.height, null));
                    }
                    if (Mode == BFSRMode.Play)
                    {
                        if (playingSlider != null)
                        {
                            playingSlider.value += (1f / NumberOfFrames2Play);
                        }
                        frameNumber++;
                    }
                    if (Mode == BFSRMode.Pause)
                    {
                        if (Input.GetKeyDown(KeyCode.LeftArrow))
                        {
                            if (playingSlider != null)
                                playingSlider.value -= (1f / NumberOfFrames2Play);
                        }
                        if (Input.GetKeyDown(KeyCode.RightArrow))
                        {
                            if (playingSlider != null)
                                playingSlider.value += (1f / NumberOfFrames2Play);
                        }
                    }
                }
                else
                {
                    if (IsMovieCapture)
                    {
                        Mode = BFSRMode.Pause;
                        GameObject.Find("BFSRecorder").GetComponent<BFSRenderer>().StopCapture();
                    }
                    else if (!ReferenceEquals(!BFSRSwitchController.Instance, null))
                    {
                        GameObject.FindGameObjectWithTag(BFS_ROOMANIMATOR_TAG).GetComponent<Animator>().speed = 0;
                        BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
                        if(Application.loadedLevelName!="VideoEdit")
                        {
                            playingSlider.value = 0;
                        }
                        BFSRSwitchController.Instance.OnFinish();
                    }
                }
                break;
        }
    }

    private Animator GetAnimator()
    {
        if (currentObjectName != visionSimulator)
        {
            GameObject.FindGameObjectWithTag(BFS_ROOMANIMATOR_TAG).GetComponent<Animator>().Play(0, 0, 0.0f);
            GameObject.FindGameObjectWithTag(BFS_ROOMANIMATOR_TAG).GetComponent<Animator>().speed = 0;
            return GameObject.FindGameObjectWithTag(BFS_OBJECT_TAG).GetComponent<Animator>();
        }
        else
        {
            return  GameObject.FindGameObjectWithTag(BFS_ROOMANIMATOR_TAG).GetComponent<Animator>();
        }
        
    }
    /// <summary>
    /// Change the target object depending upon the new value from recording.
    /// Compare the object that is already set with the new object to set
    /// if the are same no action is taken.
    /// else it will change the target object.
    /// </summary>
    /// <param name="setObjectName"> Object that is already set.</param>
    /// <param name="object2SetName">New object to set.</param>
    private void SetTransformOject(string setObjectName,string object2SetName)
    {
        if (currentObjectName != object2SetName)
        {
            if (IsMovieCapture)
            {
                GameObject.Find("BFSRecorder").GetComponent<BFSRenderer>().GetModelObject(currentObjectName).SetActive(false);
                GameObject.Find("BFSRecorder").GetComponent<BFSRenderer>().GetModelObject(object2SetName).SetActive(true);
            }
            else
            {
                GameObject.Find("Controller").GetComponent<VideoPlayController>().GetModelObject(currentObjectName).SetActive(false);
                GameObject.Find("Controller").GetComponent<VideoPlayController>().GetModelObject(object2SetName).SetActive(true);
            }
            objectTransform = GameObject.Find(object2SetName).GetComponent<Transform>();
            currentObjectName = object2SetName;
            GetAnimator().speed = 0;
        }
    }

    internal int GetImageFrameId
    {
        get
        {
            return frameNumber;
        }
    }
    /// <summary>
    /// Calculate the time in mm:ss:fff depending upon the frame ID supplied.
    /// Its sum all the delta time upto the frame ID.
    /// </summary>
    /// <param name="frameID">int frame id for which the time to calculate</param>
	private void DisplayTimer(int frameId)
	{
		if (timerLabel != null) 
		{
            timerLabel.text =GetTime(frameId) ;
		}
	}
    private string GetTime(int frameId)
    {
        var timer = replayData.Where(x => x.FrameID <= frameId-1 && x.Deleted == false).Sum(x => x.Time);
        int minutes = (int)timer / 60;
        int seconds = (int)timer % 60;
        int fraction = (int)(timer * 1000) % 1000;
        return String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
    }
    /// <summary>
    /// Creates new vector line by searching its color,material and with.
    /// </summary>
    /// <param name="drawingID">ID of a drawing to draw.</param>
    /// <param name="linePoints">Array of points for drawing to draw on.</param>
    /// <returns>New vector line.</returns>
    public VectorLine NewDrawing(int drawingID, Vector2[] drawingPoints)
    {
        VectorLine drawObject=null;
        Material lineMaterial=new Material(Continuousline);
        var drawInfo = from info in drawingInfo.Where(x => x.id == drawingID)
                       select info;
        float lineWidth = 4.0f;
        BFSRDrawing drawing = DrawingType(drawingID);
        foreach (BFSRDrawingInfo info in drawInfo)
        {
            if (string.IsNullOrEmpty(info.Material) || info.Material == "Continuousline" )
            {
                lineMaterial = new Material(Continuousline);
            }
            else
            {
                lineMaterial = new Material(DottedLine);
                
            }
            lineMaterial.color = info.Color;
           
            float y1 = (float)Camera.main.pixelRect.height / (float)info.ScreenHeight;

            lineWidth = info.Width * y1;
           
            break;
        }
        switch (drawing)
        {
            case BFSRDrawing.Line:
                drawObject = new VectorLine("DrawnLine", drawingPoints, lineMaterial, lineWidth, LineType.Continuous, Joins.Weld);
                break;
            case BFSRDrawing.Square:
                drawObject = new VectorLine("DrawnLine", new Vector2[5], lineMaterial, lineWidth, LineType.Continuous);
                break;
            case BFSRDrawing.Circle:
                drawObject = new VectorLine("DrawnLine", new Vector2[31], lineMaterial, lineWidth, LineType.Continuous);
                break;
        }
        drawObject.depth = drawingID;
        return drawObject;
    }

    private BFSRDrawing DrawingType(int drawingID)
    {
        BFSRDrawing drawing=BFSRDrawing.Line ;
        var drawingType = from linedata
                                 in replayData.OfType<BFSRDrawingData>()
                                .Where(s => s.DrawingID == drawingID)
                          select linedata.Drawing;
        foreach (BFSRDrawing draw in drawingType)
        {
            drawing = draw;
            break;
        }
        return drawing;
    }
    /// <summary>
    /// Draws all the drawing from start upto the ID.
    /// </summary>
    /// <param name="uptoID">Line id upto all the line to draw.</param>
    /// <param name="SetID">Set ID where lines belong to.</param>
    public void RedrawDrawingObjects(int uptoID, int frameid,int SetID)
    {
        VectorLine drawObject;
        Vector2[] drawingPoints;
        BFSRDrawing drawing;
        for (int i = 1; i <= uptoID; i++)
        {
            drawingPoints = DrawingPoints(i, frameid, SetID);
            drawing = DrawingType(i);
            if(drawingPoints != null)
            if (drawingPoints.Length > 1)
            {
                if (Drawings.ContainsKey(i))
                {
                    drawObject = Drawings[i];
                    Drawings[i] = drawObject;
                }
                else
                {
                    drawObject = NewDrawing(i, drawingPoints);
                    Drawings.Add(i, drawObject);
                }
                switch (drawing)
                {
                    case BFSRDrawing.Line:
                        drawObject.maxDrawIndex = drawingPoints.Length - 1;
                        drawObject.Draw();
                        break;
                    case BFSRDrawing.Square:
                        drawObject.MakeRect(drawingPoints[0], drawingPoints[drawingPoints.Length - 1]);
                        drawObject.Draw();
                        break;
                    case BFSRDrawing.Circle:
                        drawObject.MakeCircle(drawingPoints[0], drawingPoints[drawingPoints.Length - 1].x - drawingPoints[0].x, 30, 0, 0);
                        drawObject.Draw();
                        break;

                }
            }
        }
    }

    /// <summary>
    /// Search for all the points for drawing.
    /// </summary>
    /// <param name="lineID">ID for drawing whose points to be search.</param>
    /// <param name="SetID">Set ID where drawing belong to.</param>
    /// <returns>Array of Vector2</returns>
    /// Drawing Calculation playback
    public Vector2[] DrawingPoints(int drawingID,int frameid,int SetID)
    {
        //Debug.Log(drawingID);
        var drawInfo = (from info in drawingInfo.Where(x => x.id == drawingID)
                       select info).FirstOrDefault();
        if (drawInfo == null) return null;
        var y1 = (float)Camera.main.pixelRect.height /(float) drawInfo.ScreenHeight;
        var x1 = (float)Camera.main.pixelRect.width / (float)drawInfo.ScreenWidth;
        //float scaleFactor = x1 > y1 ? y1 : x1;
        float yOff = ((float)Camera.main.pixelRect.height - (y1 * (float)drawInfo.ScreenHeight)) / 2;
        float xOff = ((float)Camera.main.pixelRect.width - (y1 * (float)drawInfo.ScreenWidth)) / 2;
        var resultDrawingPoints = from drawingdata
                                in replayData.OfType<BFSRDrawingData>()
                               .Where(s => s.DrawingID == drawingID && s.FrameID < frameid && s.Index != 0 && s.SetID == SetID)
                                  select (
                                  new Vector2((((float)(drawingdata.MousePosition.x * y1) + Camera.main.pixelRect.xMin)) + xOff,
                                  (((float)(drawingdata.MousePosition.y * y1) + Camera.main.pixelRect.yMin)) + yOff
                                  ));
        return resultDrawingPoints.ToArray<Vector2>(); ;
    }

    public IEnumerator CaptureImage(string ImageLocation, Camera camera, int width, int height, Action callBack)
    {
        yield return new WaitForEndOfFrame();
        Texture2D captured = new Texture2D((int)Camera.main.pixelRect.width, (int)Camera.main.pixelRect.height, TextureFormat.RGB24, false);
        camera.Render();
        RenderTexture.active = camera.targetTexture;
        captured.ReadPixels(new Rect(Camera.main.pixelRect.x, Camera.main.pixelRect.y, Camera.main.pixelRect.width, Camera.main.pixelRect.height), 0, 0);
        captured.Apply();
        RenderTexture.active = null;
        var Thumbnail = captured.EncodeToPNG();
        Destroy(captured);

  #if UNITY_STANDALONE_WIN
    Save(new Bitmap(new MemoryStream(Thumbnail)), 1366, 768, 1, ImageLocation);
  #endif
   #if UNITY_STANDALONE_OSX
    if (Thumbnail != null && !string.IsNullOrEmpty(ImageLocation))
        {
            File.WriteAllBytes(ImageLocation, Thumbnail);
        }
   #endif
    if (!IsMovieCapture)
        {
            GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo.ImageFrameId = GetImageFrameId;
            GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo.UpdateVideo();
            if (callBack != null)
            {
                callBack();
            }
        }


    }
    public void Save(Bitmap image, int maxWidth, int maxHeight, int quality, string filePath)
    {
        // Get the image's original width and height
        int originalWidth = image.Width;
        int originalHeight = image.Height;

        // To preserve the aspect ratio
        float ratioX = (float)maxWidth / (float)originalWidth;
        float ratioY = (float)maxHeight / (float)originalHeight;
        float ratio = Math.Min(ratioX, ratioY);

        // New width and height based on aspect ratio
        int newWidth = (int)(originalWidth * ratio);
        int newHeight = (int)(originalHeight * ratio);

        // Convert other formats (including CMYK) to RGB.
        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

        // Draws the image in the specified size with quality mode set to HighQuality
        using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(newImage))
        {
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.DrawImage(image, 0, 0, newWidth, newHeight);
        }

        // Get an ImageCodecInfo object that represents the JPEG codec.
        ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Png);

        // Create an Encoder object for the Quality parameter.
        var encoder = System.Drawing.Imaging.Encoder.Quality;

        // Create an EncoderParameters object. 
        EncoderParameters encoderParameters = new EncoderParameters(1);

        // Save the image as a JPEG file with quality level.
        EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
        encoderParameters.Param[0] = encoderParameter;
        newImage.Save(filePath, imageCodecInfo, encoderParameters);
    }

    /// <summary>
    /// Method to get encoder infor for given image format.
    /// </summary>
    /// <param name="format">Image format</param>
    /// <returns>image codec info.</returns>
    private ImageCodecInfo GetEncoderInfo(ImageFormat format)
    {
        return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
    }
    /// <summary>
    /// Replay the recording.
    /// </summary>
    /// <param name="playRecordData">Data that should be replayed</param>
    public void PlayRecording(List<BFSRData> playRecordData)
    {
        Debug.Log("Replay manager has recived command to play : "+ playRecordData.Count);
        PrepareObjectsForReplay();
        replayData.Clear();
        replayData = playRecordData;
        Refresh();
        SeprateDrawingInfo();
        frameNumber = 0;
        if (playingSlider != null)
        {
            playingSlider.value = 0;
        }
        Application.targetFrameRate = 30;
        Mode = BFSRMode.Play;
        
    }
    /// <summary>
    /// Intialize objects for replay. 
    /// </summary>
    private void PrepareObjectsForReplay()
    {
        cameraTransform = GameObject.FindGameObjectWithTag(BFS_MAINCAMERA_TAG).GetComponent<Transform>();
        objectTransform = GameObject.FindGameObjectWithTag(BFS_OBJECT_TAG).GetComponent<Transform>();
        pivotTransform = GameObject.FindGameObjectWithTag(BFS_PIVOT_TAG).GetComponent<Transform>();
        controllerTransform = GameObject.FindGameObjectWithTag(BFS_CONTROLLER_TAG).GetComponent<Transform>();
        GameObject.FindGameObjectWithTag(BFS_OBJECT_TAG).GetComponent<Animator>().speed = 0;
        currentObjectName = objectTransform.name;
        VectorLine.SetCamera(Camera.main);
        Drawings = new Dictionary<int, VectorLine>();
    }
    /// <summary>
    /// Seprates the drawing info objects drom the current replay 
    /// data which is not required for playing
    /// </summary>
    public void SeprateDrawingInfo()
    {
        drawingInfo = replayData.OfType<BFSRDrawingInfo>().ToList();
        foreach (var dra in drawingInfo)
        {
            replayData.Remove(dra);
        }
        
    }

    /// <summary>
    /// Alter the replay from stop to pause or vice versa.
    /// </summary>
    public void PauseReplay()
    {
        if (Mode == BFSRMode.Play)
        {
            Debug.Log("Replay manager has recived command to pause");
            Mode = BFSRMode.Pause;
            BFSRSwitchController.Instance.OnFinish();
        }
        else if (Mode == BFSRMode.Pause)
        {
            Debug.Log("Replay manager has recived command to start playing again");
            Mode = BFSRMode.Play;
            if (playingSlider != null)
            {
                frameNumber = (int)Math.Round(playingSlider.value * NumberOfFrames2Play);
            }
            
        }
    }
    private void Refresh()
    {
        replayData = replayData.Where(x => x.Deleted == false).ToList<BFSRData>();
        NumberOfFrames2Play = replayData.Count - 1;
    }
    public void onSliderValueChanged()
    {
        if (Mode == BFSRMode.Play && playingSlider != null)
            if ( (int)Math.Round(playingSlider.value * NumberOfFrames2Play)-frameNumber  !=1)
            { 
                Mode = BFSRMode.Pause;
                Camera.main.GetComponent<BFSERecordAudio>().PlayAudio();

            }
        if (Mode == BFSRMode.Pause && playingSlider != null)
            if (playingSlider != null)
            frameNumber = (int)Math.Round(playingSlider.value * NumberOfFrames2Play);
    }
    public void StopPlaying()
    {
        Debug.Log("Replay manager has finish playing");
        Mode = BFSRMode.Stop;
       
        if(Drawings != null)
            if (Drawings.Count > 0)
            {
                VectorLine.Destroy(Drawings.Values.ToArray<VectorLine>());
                Drawings.Clear();
            }
        if(playingSlider != null)
        {
            playingSlider.value = 0;
        }
            
        BFSRSwitchController.Instance.FinishPlayingData(replayData.ToList<BFSRData>());
        replayData.Clear();
    }

    public IEnumerator RemoveFrame(string  fromTime, string toTime,Action callback)
    {
        var video = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo;
        int FrameFrom = -1;
        int FrameTo = -1;
        Debug.Log(string.Format("Record count {0} from time {1} to frame {2}", replayData.Count, fromTime, toTime));
        yield return null;
        foreach (var id in replayData.Select(x => x.FrameID))
        {
            var timer = replayData.Where(x => x.FrameID <= id && x.Deleted == false).Sum(x => x.Time);
            if (timer > 0)
            {
                int minutes = (int)timer / 60;
                int seconds = (int)timer % 60;
                int fraction = (int)(timer * 1000) % 1000;
                if (String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction) == fromTime.Trim())
                {
                    FrameFrom = id;
                }
                if (String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction) == toTime.Trim())
                {
                    FrameTo = id;
                    break;
                }
            }
        }
        if(FrameFrom==-1)
        {
            FrameFrom = 1;
        }

        Debug.Log(string.Format("Record count {0} from frame {1} to frame {2}", replayData.Count, FrameFrom, FrameTo));
        if (replayData != null)
        {
            var drawingFrame = (from data in replayData.OfType<BFSRTransformationData>()
                                join info in drawingInfo on data.FrameID equals info.TransformID
                                select data).ToList();
            //Removing Drawing Frames
            var dRemoveData = from data in replayData.OfType<BFSRTransformationData>()
                .Where(s => s.FrameID >= FrameFrom && s.FrameID <= FrameTo)
                              select data;

            var TRemoveData = from data in replayData.OfType<BFSRDrawingData>()
               .Where(s => s.FrameID >= FrameFrom && s.FrameID <= FrameTo)
                              select data;
            foreach (var dra in dRemoveData.ToList())
            {
                dra.Deleted = true;
            }

            //Removing Transformation Frames

            foreach (var dra in TRemoveData.ToList())
            {
                dra.Deleted = true;
            }
            foreach (var info in drawingFrame)
            {
                info.Deleted = false;
            }
            Debug.Log("Frame found for deleted : " + (dRemoveData.ToList().Count + TRemoveData.ToList().Count));
            Refresh();
        }
        var tempReplayData = replayData.OrderBy(x => x.FrameID).ToList<BFSRData>();
        foreach (var drawInfo in drawingInfo)
        {
            tempReplayData.Add(drawInfo);
        }
        
        BFSRSwitchController.Instance.ModifyFile(tempReplayData, video.XmlPath);
        video.Duration = (int)tempReplayData.Where(x=>x.Deleted==false).Sum(x => x.Time);
        video.AnimationId = "";
        video.Public = false;
        video.Private = false;
        video.UpdateVideo();
        callback();

    }
    public int VideoLength()
    {
        var length = replayData.Where(x=>x.Deleted == false).Sum(x => x.Time);
        return (int)length;
    }
    public string TimerValueOnFrame(int frameID)
    {
        var timer = replayData.Where(x => x.FrameID <= frameID-1 && x.Deleted == false).Sum(x => x.Time);
        if (timer > 0)
        {
            int minutes = (int)timer / 60;
            int seconds = (int)timer % 60;
            int fraction = (int)(timer * 1000) % 1000;
            return String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
        }
        return "00:00:000";
    }
    public string LabelUpdate(float value)
    {
        var CalFrame=(int)Math.Round(value * NumberOfFrames2Play);
        if (value == 0.0f)
        {
				return "00:00:000";
        }
        var timer = replayData.Where(x => x.FrameID <= CalFrame && x.Deleted == false).Sum(x => x.Time);
        if (timer > 0)
        {
            int minutes = (int)timer / 60;
            int seconds = (int)timer % 60;
            int fraction = (int)(timer * 1000) % 1000;
            return String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
            
        }
        return "00:00:000";
    }
    public float GetNewSliderValue(float value)
    {
        value += 0.1f;
        return value + (1f / NumberOfFrames2Play);
    }
    public IEnumerator SliderValueOnTime(string lowerTime,string higherTime, Action<float, string,float,string> callBack)
    {
        yield return null;
        float lowerSlider=0.0f;
        float higherSlider=0.0f;
        string lowerLabelTime="00:00:000";
        string higherLabelTime = "00:00:000";
        foreach (var id in replayData.Select(x => x.FrameID))
        {
            var timer = replayData.Where(x => x.FrameID <= id && x.Deleted == false).Sum(x => x.Time);
            if (timer > 0)
            {
                int minutes = (int)timer / 60;
                int seconds = (int)timer % 60;
                int fraction = (int)(timer * 1000) % 1000;
                if (String.Format("00:{0:00}:{1:00}:{2:000}", minutes, seconds, fraction) == lowerTime.Trim())
                {
                    lowerLabelTime = String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
                    lowerSlider = (id * 1.0f) / NumberOfFrames2Play;
                }
                if (String.Format("00:{0:00}:{1:00}:{2:000}", minutes, seconds, fraction) == higherTime.Trim())
                {
                    higherLabelTime = String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
                    higherSlider=(id * 1.0f) / NumberOfFrames2Play;
                    break;
                }
            }
        }
        yield return new WaitForSeconds(1);
        callBack(lowerSlider, lowerLabelTime, higherSlider, higherLabelTime);
    }    
}


