﻿/// <summary>
/// Author : Aizaz Ahmed.
/// BFSRData is the base class of custom data types required for recording data types.
/// So different extended datatypes can be converted to BFSRData and can stored/retrive in/from single/multiple
/// collection.
/// 
/// Data can handled more generically
/// 
/// All the related object are tag with BFSR = Bright Future Software Recorder.
/// </summary>
using System;
using System.Xml.Serialization;
[Serializable]
[XmlInclude(typeof(BFSRTransformationData))]
[XmlInclude(typeof(BFSRDrawingData))]
[XmlInclude(typeof(BFSRDrawingInfo))]
public class BFSRData
{
    public float Time { get; set; }
    public int FrameID { get; set; }
    public bool Deleted { get; set; }
    public BFSRData()
    { 
    }
}