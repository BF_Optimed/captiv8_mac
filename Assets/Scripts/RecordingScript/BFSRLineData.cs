﻿using System;
using UnityEngine;
using System.Runtime.Serialization;
[Serializable]
public class BFSRLineData : BFSRData
{
    #region Fields
        public Vector2 MousePosition { get; set; }
        public int LineIndex { get; set; }
        public int TotalNoOfLines { get; set; }
        public int LineID { get; set; }
        public int DrawInfoID { get; set; }
        
    #endregion

    #region default Constuctor
        public BFSRLineData()
        { 
        }
     #endregion
}

