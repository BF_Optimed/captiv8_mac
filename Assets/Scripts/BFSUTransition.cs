﻿using UnityEngine;

public class BFSUTransition : MonoBehaviour
{
    private bool _begin;
    private UISprite _blackoutSprite;
    private BtnController _btnController;
    private BFSMRotation _rotationScript;
    private BFSASlider _slider;
    public GameObject CurrentObj;
    public float FadeInValue = 0.05f;
    public float FadeOutValue = 0.01f;
    public GameObject GeoController;
    public GameObject GoBtnController;
    public GameObject SliderController;
    public GameObject Grid;
    public GameObject[] ObjPrefabs;
    public GameObject Overview;


    public string ObjName { get; set; }

    private void Start()
    {
        ObjName = "Overview";
        _rotationScript = Camera.main.GetComponent<BFSMRotation>();
        _blackoutSprite = gameObject.GetComponent<UISprite>();
        _btnController = GoBtnController.GetComponent<BtnController>();
        _slider = SliderController.GetComponent<BFSASlider>();

        GridGlitchFix();
    }

    /*Required for recording*/

    public GameObject GetModelObject(string modelName)
    {
        for (var i = 0; i <= ObjPrefabs.Length - 1; i++)
        {
            if (ObjPrefabs[i].name == modelName)
            {
                return ObjPrefabs[i];
            }
        }
        return null;
    }

    public void Begin(string objName, bool performFade)
    {
        ObjName = objName;
        if (performFade)
        {
            _begin = true;
        }
        else
        {
            LoadNewModel();
            Debug.Log("Loading new model: " + objName);
        }
    }

    private void Update()
    {
        if (_begin)
        {
            FadeIn();
        }
        else
        {
            if (_blackoutSprite.alpha > 0)
            {
                FadeOut();
            }
        }
    }

    private void FadeIn()
    {
        _blackoutSprite.alpha += FadeInValue;
        if (_blackoutSprite.alpha >= 1.0f)
        {
            LoadNewModel();
            Debug.Log("LOADING NEW MODEL.");
        }
    }

    private void FadeOut()
    {
        _blackoutSprite.alpha -= FadeOutValue;
        GridGlitchFix();
    }

    private void GridGlitchFix()
    {
        if (_blackoutSprite.alpha <= 0.9 && _blackoutSprite.alpha >= 0.8)
        {
            Grid.SetActive(false);
            Grid.SetActive(true);
            GeoController.SetActive(false);
            GeoController.SetActive(true);
        }
    }

    private void LoadNewModel()
    {
        _begin = false;
        _btnController.Reset();

        for (var i = 0; i <= ObjPrefabs.Length - 1; i++)
        {
            FindAndLoadNewObject(i);
        }
        _btnController.Autopiloting = false;
    }

    private void FindAndLoadNewObject(int i)
    {
        ObjPrefabs[i].SetActive(false);

        if (ObjPrefabs[i].name == ObjName)
        {
            GeoController.transform.rotation = Quaternion.Euler(Vector3.zero);
            if(CurrentObj.name == "Optic Nerve" || CurrentObj.name == "Anterior Segment")
            {
                CurrentObj.transform.rotation = Quaternion.Euler(new Vector3(0,0,-90));
            }
            else
            {
                CurrentObj.transform.rotation = Quaternion.Euler(Vector3.zero);
            }
            

            SetUpNewObject(i);

            CleanUpOtherScripts();

            CheckAndSetMovementLimits();

            _btnController.RotationScript.Init();
        }
    }

    private void CheckAndSetMovementLimits()
    {
        switch (CurrentObj.name)
        {
            case "Adnexa":
                SetUpLimits(-20, -250, 20, -160);
                break;
            case "Anterior Segment":
                SetUpLimits(-50,-220,50,-50);
                break;
            case "Macula":
                SetUpLimits(10, 130, 35, 230);
                break;
            case "Vision Simulator":
                SetUpLimits(-5,-210,25,-90);
                break;
            default:
                SetUpLimits(-75, -360, 75, 360);
                break;
        }
    }

    private void CleanUpOtherScripts()
    {
        _btnController.MenuActive = false;
        _slider.ResetAnimArray();
        //_btnController.Reset();
        _btnController.CamZoomScript.DesiredDistance = 35.0f;
    }

    private void SetUpNewObject(int i)
    {
        ObjPrefabs[i].SetActive(true);
        CurrentObj = ObjPrefabs[i];
        BFSRSwitchController.Instance.ManageNewModel(ObjPrefabs[i].name);
        _slider.SortActionButtons(ObjName);
    }

    private void SetUpLimits(int YMinLimit, int XMinLimit, int YMaxLimit, int XMaxLimit)
    {
        _rotationScript.YMinLimit = YMinLimit;
        _rotationScript.XMinLimit = XMinLimit;
        _rotationScript.YMaxLimit = YMaxLimit;
        _rotationScript.XMaxLimit = XMaxLimit;
    }
}