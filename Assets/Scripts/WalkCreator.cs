﻿using UnityEngine;
using System.Collections;

public class WalkCreator : MonoBehaviour {

    public GameObject WalkPrefab;
    public GameObject UIRoot;

	public void CreateWalkthrough (string title, string [] contents, string key, string icon) {
        if(PlayerPrefs.GetFloat(key) == 0)
        {
            NGUITools.AddChild(UIRoot, WalkPrefab);

            WalkthroughCreator temp = UIRoot.GetComponentInChildren<WalkthroughCreator>();
            temp.WalkEntries = contents;
            temp.TitleText = title;
            temp.IconName = icon;
            
            //var clone = Instantiate(WalkPrefab, Vector3.zero, transform.rotation) as GameObject;
            //clone.GetComponent<WalkthroughCreator>().WalkEntries = contents;
            //clone.GetComponent<WalkthroughCreator>().TitleText = title;
            PlayerPrefs.SetFloat(key, 1);
        }
	}
}
