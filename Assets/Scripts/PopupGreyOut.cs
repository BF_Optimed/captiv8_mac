﻿using UnityEngine;
using System.Collections;

public class PopupGreyOut : MonoBehaviour {
	public GameObject Close;
	public GameObject NotifBackground;
	//public GameObject Notification;
	//private UISprite NotificationSprite;
	// Use this for initialization
	void Start () {
		//NotificationSprite = Notification.GetComponent<UISprite> ();
	}
	// Update is called once per frame
	void Update () {
	}
	public void CloseClick()
	{
        if (Close.tag == "Notification")
        {
            Destroy(Close);
        }
		//NotificationSprite.spriteName = "eye";
		GameObject.Find("Controller").GetComponent<BottomBarController>().DisableSync();
	}
}
