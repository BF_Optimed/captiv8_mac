﻿using UnityEngine;

public class GridReOrg : MonoBehaviour
{
    private UIGrid _grid;
    private UITable _table;

    private void Start()
    {
        if (gameObject.GetComponent<UIGrid>() != null)
            _grid = gameObject.GetComponent<UIGrid>();

        if (gameObject.GetComponent<UITable>() != null)
            _table = gameObject.GetComponent<UITable>();
    }

    private void Update()
    {
        if (_grid != null)
            _grid.Reposition();

        if (_table != null)
            _table.Reposition();
    }
}