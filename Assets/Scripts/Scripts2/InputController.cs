﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    private BtnController _buttonController;
    private BFSMRotation _movementScript;
    public GameObject ButtonController;

    private void Start()
    {
        _buttonController = ButtonController.GetComponent<BtnController>();
        _movementScript = Camera.main.GetComponent<BFSMRotation>();
    }

    private void Update()
    {
        if (Input.GetKey("escape"))
        {
            _buttonController.Reset();
        }
    }

    public void MovementAllowed()
    {
        _movementScript.InputAllowed = false;
    }

    public void MovementNotAllowed()
    {
        _movementScript.InputAllowed = true;
    }
}