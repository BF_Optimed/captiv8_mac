﻿using UnityEngine;

public class GrowListTween : MonoBehaviour
{
    public UIGrid Grid;
    public int State = 1;
    public TweenAlpha Tween;

    private void OnEnable()
    {
        Tween = GetComponentInParent<TweenAlpha>();
    }

    private void Update()
    {
        if (State == 1)
        {
            if (Grid.cellHeight >= 20)
            {
                State = 0;
            }
            else
            {
                Grid.cellHeight += 4;
            }
            Grid.Reposition();
        }
        else if (State == 2)
        {
            if (Grid.cellHeight <= 0)
            {
                gameObject.SetActive(false);
            }
            else
            {
                Grid.cellHeight -= 4;
            }
            Grid.Reposition();
        }
    }

    public void Open()
    {
        State = 1;
    }

    public void Close()
    {
        State = 2;
    }
}