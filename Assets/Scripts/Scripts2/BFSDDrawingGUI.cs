﻿using UnityEngine;
using Assets.Scripts;

public class BFSDDrawingGUI : MonoBehaviour
{
    public GameObject BoxBtn;
    private boxSelect boxdrawing;
    public UISprite BoxSprite;
    public GameObject CircleBtn;
    private SelectCircle circlerawing;
    public UISprite CircleSprite;
    private bool flag = true;
    public GameObject LargeWidthBtn;
    public UISprite LargeWidthSprite;
    private BFSDLineWidth LineWidth;
    public GameObject MediumWidthBtn;
    public UISprite MediumWidthSprite;
    public GameObject options;
    private lineDrawMouse pendrawing;
    public GameObject SmallWidthBtn;
    public UISprite SmallWidthSprite;

    private void Start()
    {
     
        CircleSprite = CircleBtn.GetComponent<UISprite>();
        BoxSprite = BoxBtn.GetComponent<UISprite>();
        SmallWidthSprite = SmallWidthBtn.GetComponent<UISprite>();
        MediumWidthSprite = MediumWidthBtn.GetComponent<UISprite>();
        LargeWidthSprite = LargeWidthBtn.GetComponent<UISprite>();
        pendrawing = Camera.main.GetComponent<lineDrawMouse>();
        circlerawing = Camera.main.GetComponent<SelectCircle>();
        boxdrawing = Camera.main.GetComponent<boxSelect>();
        LineWidth = gameObject.GetComponent<BFSDLineWidth>();
        
    }

    public void OnEnable()
    {

        DefaultSprites();
        WidthDefaultSprites();


        switch (PlayerPrefs.GetString(MagicStrings.DefaultWidthKey))
        {
            case "1":
                LineWidthSmall();
                break;
            case "2":
                LineWidthMedium();
                break;
            case "3":
                LineWidthLarge();
                break;
            default:
                LineWidthMedium();
                break;
        }
    }

    public void DefaultSprites()
    {
        CircleSprite = CircleBtn.GetComponent<UISprite>();
        BoxSprite = BoxBtn.GetComponent<UISprite>();
        Debug.Log("Setting circlee and box to defaults");
        CircleSprite.spriteName = MagicStrings.CircleSprite;
        BoxSprite.spriteName = MagicStrings.SquareSprite;
    }

    private void WidthDefaultSprites()
    {
        SmallWidthSprite = SmallWidthBtn.GetComponent<UISprite>();
        MediumWidthSprite = MediumWidthBtn.GetComponent<UISprite>();
        LargeWidthSprite = LargeWidthBtn.GetComponent<UISprite>();
        SmallWidthSprite.spriteName = MagicStrings.SmallWidthSprite;
		MediumWidthSprite.spriteName = MagicStrings.MediumWidthSprite;
		LargeWidthSprite.spriteName = MagicStrings.LargeWidthSprite;
    }

    public void ActivateDrawing()
    {
        NGUITools.SetActive(options, true);
        //DrawPen();
    }


    public void TurnOffDrawing()
    {
        NGUITools.SetActive(options, false);
        if (pendrawing != null)
            pendrawing.deleteArray();
        if (circlerawing != null)
            circlerawing.deleteArray();
        if (boxdrawing != null)
            boxdrawing.deleteArray();
        DisableAll();
    }

    private void DisableAll()
    {
        Camera.main.GetComponent<SelectCircle>().enabled = false;
        Camera.main.GetComponent<lineDrawMouse>().enabled = false;
        Camera.main.GetComponent<boxSelect>().enabled = false;
    }

    public void DrawCircle()
    {
        DisableAll();
        DefaultSprites();
        WidthDefaultSprites();
        CircleSprite.spriteName = MagicStrings.CircleSpriteHit;
        Camera.main.GetComponent<SelectCircle>().enabled = true;
        BFSRSwitchController.Instance.ManageNewDrawing(BFSRDrawing.Circle);
        Debug.Log("CLICKED THE CIRCLE.");
    }

    public void DrawPen()
    {
        DisableAll();
        DefaultSprites();
        WidthDefaultSprites();
        pendrawing = Camera.main.GetComponent<lineDrawMouse>();
        Camera.main.GetComponent<lineDrawMouse>().enabled = true;
        if (pendrawing != null)
        {
            pendrawing.ContinuousLine();
        }
        BFSRSwitchController.Instance.ManageNewDrawing(BFSRDrawing.Line);
    }

    public void DrawBox()
    {
        DisableAll();
        DefaultSprites();
        WidthDefaultSprites();
        BoxSprite.spriteName = MagicStrings.SquareSpriteHit;
        Camera.main.GetComponent<boxSelect>().enabled = true;
        BFSRSwitchController.Instance.ManageNewDrawing(BFSRDrawing.Square);
    }

    public void LineWidthSmall()
    {
		DrawPen ();
        DefaultSprites();
        WidthDefaultSprites();
        SmallWidthSprite.spriteName = MagicStrings.SmallWidthSpriteHit;
        LineWidth.GetComponent<BFSDLineWidth>().SmallWidth();
    }

    public void LineWidthMedium()
    {
        Debug.Log("Line = med");
		DrawPen ();
        DefaultSprites();
        WidthDefaultSprites();
        MediumWidthSprite.spriteName = MagicStrings.MediumWidthSpriteHit;
        if(LineWidth != null)
        LineWidth.GetComponent<BFSDLineWidth>().MediumWidth();
    }

    public void LineWidthLarge()
    {
		DrawPen ();
        DefaultSprites();
        WidthDefaultSprites();
        LargeWidthSprite.spriteName = MagicStrings.LargeWidthSpriteHit;
        LineWidth.GetComponent<BFSDLineWidth>().HighWidth();
    }
	
}