﻿using UnityEngine;

public class BackgroundGrow : MonoBehaviour
{
    private UISprite _selfSprite;
    public UIGrid Grid;

    private void Start()
    {
        _selfSprite = gameObject.GetComponent<UISprite>();
    }

    private void Update()
    {
        _selfSprite.height = (int) ((Grid.GetChildList().Count + 3)*Grid.cellHeight);
    }
}