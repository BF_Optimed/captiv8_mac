﻿using Assets.DBHelper;
using System;
using System.Globalization;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.OptimedClient;
using System.Collections;
public class SplashScreen : MonoBehaviour
{
    //private UILabel DeviceLbl;

    public static GameObject Username;
    public GameObject BadDetails;
    public GameObject Description;
    public UIInput DescriptionLbl;
    public GameObject Device;
    public GameObject Password;
    public UIInput PasswordLbl;
    public GameObject Register;
    public GameObject SuccessNotif;
    public UIInput UsernameLbl;

    private int counter = 0;


    public void Awake()
    {
        if (PlayerPrefs.GetInt(MagicStrings.ResolutionXKey) < 1024)
            PlayerPrefs.SetInt(MagicStrings.ResolutionXKey, 1024);

        if (PlayerPrefs.GetInt(MagicStrings.ResolutionYKey) < 768)
            PlayerPrefs.SetInt(MagicStrings.ResolutionYKey, 768);

        Screen.SetResolution(PlayerPrefs.GetInt(MagicStrings.ResolutionXKey), PlayerPrefs.GetInt(MagicStrings.ResolutionYKey), false);

        Debug.Log("RES LOADED AS : " + PlayerPrefs.GetInt(MagicStrings.ResolutionXKey) + " - " + PlayerPrefs.GetInt(MagicStrings.ResolutionYKey));
    }

    public void Submit()
    {
        UsernameLbl = Register.GetComponent<UIInput>();
        PasswordLbl = Password.GetComponent<UIInput>();
        DescriptionLbl = Description.GetComponent<UIInput>();

        try
        {
            Camera.main.GetComponent<BFSNRegistrationLicense>().OptimiedRegistration(UsernameLbl.value, PasswordLbl.value, DescriptionLbl.value, 1, Notification);
               
        }
        catch(ArgumentNullException)
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "All fields are required.", "Ok");
        }
    }
    public void Notification(bool success, string title, string message)
    {
        if (success)
        {
            Debug.Log("Registration Success");
            NGUITools.SetActive(SuccessNotif, true);
        }
        else
        {
            NGUITools.SetActive(BadDetails, true);
            GameObject.Find("LabelToChange").GetComponent<UILabel>().text = message;
        }
    }
    public void TryAgain()
    {
        NGUITools.SetActive(BadDetails, false);
    }

    public void LoadMainScene()
    {
        Application.LoadLevel("Overview");
    }

    private static bool CheckLicenseExpire(string strExpireDate, string strLastCheckDate)
    {
        try
        {
            var expireDate = DateTime.ParseExact(strExpireDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var lastCheckDate = DateTime.ParseExact(strLastCheckDate, "dd/MM/yyyy HH:mm:ss",
                CultureInfo.InvariantCulture);
            Debug.Log(expireDate > DateTime.Now &&
                      ((expireDate - lastCheckDate) < TimeSpan.Zero
                          ? -(expireDate - lastCheckDate)
                          : expireDate - lastCheckDate) >
                      ((expireDate - DateTime.Now) < TimeSpan.Zero
                          ? -(expireDate - DateTime.Now)
                          : (expireDate - DateTime.Now)));
            if (expireDate > DateTime.Now &&
                ((expireDate - lastCheckDate) < TimeSpan.Zero
                    ? -(expireDate - lastCheckDate)
                    : expireDate - lastCheckDate) >
                ((expireDate - DateTime.Now) < TimeSpan.Zero
                    ? -(expireDate - DateTime.Now)
                    : (expireDate - DateTime.Now)))
                return false;
        }
        catch (FormatException e)
        {
            Debug.Log(e.Message);
            //throw;
        }
        return true;
    }

    private static bool CheckLicenseExits()
    {
        try
        {
            var temp = InstallationId.GetInstallationId;
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            return false;
        }

        return true;
    }

    void Update()
    {
        StartCoroutine(WaitForTime());
    }
    public IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(2);
        if (CheckLicenseExits())
        {
            var encption = new BFSEncryption();
            string ExpirationDate;
            string LastDateCheck;
            encption.TryDecrypt(Runtime.Default.SystemInterrupts, SystemInfo.deviceUniqueIdentifier, out ExpirationDate);
            encption.TryDecrypt(Runtime.Default.RunManager, SystemInfo.deviceUniqueIdentifier, out LastDateCheck);
            if (!CheckLicenseExpire(ExpirationDate, LastDateCheck))
            {
                Runtime.Default.RunManager = encption.Encrypt(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), SystemInfo.deviceUniqueIdentifier);
                Runtime.Default.Save();
                Debug.Log("License is valid or not experied");
                LoadMainScene();
            }
            else
            {
                Application.LoadLevel("Login");
            }
        }
        else
        {
            Application.LoadLevel("Login");
        }
       
    }

}