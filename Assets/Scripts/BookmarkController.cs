﻿using UnityEngine;

public class BookmarkController : MonoBehaviour
{
    private BFSUTransition _blackoutScript;
    private GameObject _button;

    public string[] BookmarkList =
    {
        "Ciliary_Body", "Cornea1", "inferior_Oblique", "Inferior_Rectus", "inner_eye_iris", "InnerBloodVessels",
        "lateral_Rectus", "OuterOpticNerve", "Schlera", "Zonules"
    };

    public GameObject ParentObject;
    public GameObject UiObject;

    private void Start()
    {
        CreateAllButtons();
    }

    private void CreateAllButtons()
    {
        foreach (var t in BookmarkList)
        {
            CreateNewButton(t);
        }
    }

    private void CreateNewButton(string t)
    {
        _button = NGUITools.AddChild(ParentObject, UiObject);
        var newButtonLabel = _button.GetComponentInChildren<UILabel>();
        _button.name = t;
        newButtonLabel.text = t;
    }
}