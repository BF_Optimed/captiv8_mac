﻿using UnityEngine;
using System.Collections;

public class SubtitleEditor : MonoBehaviour {

    public GameObject sprite;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SelectThisSub()
    {
        SubtitleCreator temp = GameObject.Find("Subtitle").GetComponent<SubtitleCreator>();
        if(!temp.isEditing)
        {
            temp._editingSprite = sprite.GetComponent<UISprite>();
            temp._editingSprite.spriteName = "grey";
            
            temp.EditSubtitle(gameObject);
        }
        else
        {
            temp._editingSprite.spriteName = "IconBackground";

            temp._editingSprite = sprite.GetComponent<UISprite>();
            temp._editingSprite.spriteName = "grey";

            temp.EditSubtitle(gameObject);

        }
    }
}
