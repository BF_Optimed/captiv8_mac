﻿using UnityEngine;

public class NotificationInitiliser : MonoBehaviour
{
    public GameObject Button1;
    public GameObject Button2;
    public GameObject Description;
    public GameObject Title;
    public UILabel TitleText { get; set; }
    public UILabel DescText { get; set; }
    public UILabel Btn1 { get; set; }
    public UILabel Btn2 { get; set; }

    private void Awake()
    {
        TitleText = Title.GetComponentInChildren<UILabel>();
        DescText = Description.GetComponent<UILabel>();
        Btn1 = Button1.GetComponentInChildren<UILabel>();
        Btn2 = Button2.GetComponentInChildren<UILabel>();
    }

    public void DeleteSelf()
    {
        Destroy(gameObject);
    }

    public void CreateOneButtonNotif(string btn1, string btn2, string titleText, string descripText,
        EventDelegate btn1Action)
    {
        SetUpText(btn1, btn2, titleText, descripText);
        SetButtonAction(btn1Action, Button1);
        SetButtonAction(new EventDelegate(DeleteSelf), Button2);
    }

    private void SetButtonAction(EventDelegate btn1Action, GameObject button)
    {
        var temp = button.GetComponent<UIEventTrigger>();
        temp.onClick.Clear();
        temp.onClick.Add(btn1Action);
        temp.onClick.Add(new EventDelegate(DeleteSelf));
    }

    private void SetUpText(string btn1, string btn2, string titleText, string descripText)
    {
        Btn1.text = btn1;
        Btn2.text = btn2;
        TitleText.text = titleText;
        DescText.text = descripText;
    }

    public void CreateTwoButtonNotif(string btn1, string btn2, string titleText, string descripText,
        EventDelegate btn1Action, EventDelegate btn2Action)
    {
        SetUpText(btn1, btn2, titleText, descripText);
        SetButtonAction(btn1Action, Button1);
        SetButtonAction(btn2Action, Button2);
    }
}