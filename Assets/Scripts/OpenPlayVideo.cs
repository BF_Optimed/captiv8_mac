﻿using Assets.DBHelper;
using UnityEngine;
using System;
using Assets.SynchroniseScript;
using System.IO;

public class OpenPlayVideo : MonoBehaviour
{
    private string _id;
    private Texture _tex;
    private UITexture _uiTex;
    private Video _loadedVideo;
    private ContinuousVars _continuousVars;

    private void Start()
    {
        _continuousVars = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
        _id = gameObject.transform.FindChild("FileName").GetComponent<UILabel>().text;
        _loadedVideo = new Video().GetVideo(_id);

    }

    public void OnClick()
    {
        if(_loadedVideo.Owner == Owner.captiv8)
        {
            Captiv8Videos.Play(_loadedVideo);
        }
        else if(File.Exists(_loadedVideo.XmlPath))
        {
            BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
            Application.LoadLevel("VideoView");
            _continuousVars.LoadedVideo = _loadedVideo;
            Debug.Log("Change Scene to video : " + _loadedVideo.Name);
        }
        else
        {
            GameObject.Find("Controller").GetComponent<CreateNotification>().CreateOkayButton("Error", "There has been an error when trying to load this video, please try again.","Ok");
        }  
    }
}