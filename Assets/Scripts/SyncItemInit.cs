﻿using UnityEngine;

public class SyncItemInit : MonoBehaviour
{


    public string Id { get; set; }
    public bool IsChecked { get; private set; }
    public void SetName(string tName)
    {
        gameObject.GetComponentInChildren<UILabel>().text = tName;
    }

    public void AnimationChecked()
    {
        IsChecked = gameObject.GetComponent<UIToggle>().value;
    }
}