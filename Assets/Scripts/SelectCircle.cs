﻿using System.Collections.Generic;
using UnityEngine;
using Vectrosity;

public class SelectCircle : MonoBehaviour
{
    private bool clickFlag = false;
    private List<VectorLine> LineArray;
    public Material lineMaterial;
    private BFSDLineWidth LineWidth;
    private int oldWidth;
    private Vector2 originalPos;
    private ColorSelection SelectColor;
    private VectorLine selectionLine;
    public float textureScale;

    public GameObject BtnController;
    private BtnController _btnController;

    private void Start()
    {
        _btnController = BtnController.GetComponent<BtnController>();
        LineWidth = BtnController.GetComponent<BFSDLineWidth>();
        SelectColor = BtnController.GetComponent<ColorSelection>();
        selectionLine = new VectorLine("Selection", new Vector2[31], lineMaterial, 4.0f, LineType.Continuous);
        lineMaterial.color = SelectColor.GetCurrentColor();
        selectionLine.depth = _btnController.DepthCount;
        _btnController.DepthCount++;
        selectionLine.textureScale = textureScale;
        oldWidth = Screen.width;
        LineArray = new List<VectorLine>();
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            originalPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            if(ReferenceEquals(null,selectionLine))
            {
                lineMaterial.color = SelectColor.GetCurrentColor();
                selectionLine = new VectorLine("Selection", new Vector2[31], lineMaterial, LineWidth.GetCurrentLineWidth(),
                LineType.Continuous);
                selectionLine.depth = _btnController.DepthCount;
            }
            selectionLine.MakeCircle(originalPos, Input.mousePosition.x - originalPos.x, 30, 0, 0);
            selectionLine.lineWidth = LineWidth.GetCurrentLineWidth();
            selectionLine.Draw();
        }

        if (Input.GetMouseButtonUp(0))
        {
            LineArray.Add(selectionLine);
            selectionLine = null;
            _btnController.DepthCount++;
            Debug.Log(_btnController.DepthCount);
        }
        //selectionLine.textureOffset = -Time.time*2.0 % 1;

        if (oldWidth != Screen.width)
        {
            oldWidth = Screen.width;
            VectorLine.SetCamera();
        }


        if (Input.GetMouseButtonDown(1))
        {
            var countArray = LineArray.Count;
            print(countArray);
            if (countArray != 0)
            {
                var newLineRefrence = LineArray[countArray - 1];
                if (newLineRefrence != null)
                {
                    VectorLine.Destroy(ref newLineRefrence);
                    LineArray.RemoveAt(countArray - 1);
                }
            }
        }
    }

    public void deleteArray()
    {
        if (LineArray != null)
            VectorLine.Destroy(LineArray);
    }
}