﻿using UnityEngine;
using System.Collections;

public class TextColorChanger : MonoBehaviour {
	public UILabel DropDownLabel;
	// Use this for initialization
	void Start () {
		DropDownLabel = gameObject.GetComponentInChildren<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void DropDownHoverOver()
	{
		DropDownLabel.color =  new Color (0.121f,0.6f,0.92f,1.0f);
	}
	public void DropDownHoverOut()
	{
		DropDownLabel.color = Color.black;
	}
}
