﻿using UnityEngine;

public class WalkButtonScript : MonoBehaviour
{
    public string Info;

    void OnClick()
    {
        Debug.Log("Clicked the walkthrough dot.");
        GameObject.Find("CurrentLabel").GetComponent<UILabel>().text = Info;
    }
}