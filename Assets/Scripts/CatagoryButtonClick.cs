﻿using UnityEngine;

public class CatagoryButtonClick : MonoBehaviour
{
    public void InputRecieved()
    {
        ContinuousVars temp = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
        temp.CatagoryString = gameObject.name;
        GameObject.Find("TVGrid").GetComponentInChildren<BFSGTVVideoList>().GetListFromCategory(temp.CatagoryString);
        gameObject.GetComponentInParent<UIGrowthScript>().InputRecieved();
    }
}