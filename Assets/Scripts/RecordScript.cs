﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Assets.Scripts;

public class RecordScript : MonoBehaviour
{
    private string _category;
    private string _description;
    private UISlider _editSlider1;
    private bool _expanded;
    private bool _hasClicked = false;
    private string _imagelocation;
    private string _name;
    private UISprite _playPause;
    private UISprite _saveSprite;
    private UISprite _recordSprite;
    private UISprite _pauseSprite;
    private List<BFSRData> _recordData;
    private byte[] _thumnailImage;
    //private TVGUIController _tvGuiCon;
    private string _videoPath;
    public GameObject Catagory;
    public GameObject Description;
    public string Fname;
    public GameObject GoEditSlider1;
    public GameObject GoEditSlider2;
    public GameObject Name;
    public GameObject PauseBtn;
    public GameObject SaveBtn;
    public GameObject RecordBtn;
    public GameObject RecordingToolbar;
    public GameObject SaveRecordedVideoPopUp;
    public bool SaveRecording;
    public GameObject VideoTimeLabel;
    private Hibernation _hibernation;

    public void SetSaveCategory(string newCategory)
    {
        _category = newCategory;
    }

    private void Start()
    {
        //_editSlider1 = GoEditSlider1.GetComponent<UISlider>();
        //_tvGuiCon = GameObject.Find("TVOverlayController").GetComponent<TVGUIController>();
        BFSRSwitchController.Instance.StartEvent += Instance_Start;
        BFSRSwitchController.Instance.Stop += Instance_Stop;
        _saveSprite = SaveBtn.GetComponent<UISprite>();
        _recordSprite = RecordBtn.GetComponent<UISprite>();
        _pauseSprite = PauseBtn.GetComponent<UISprite>();
        _hibernation = GameObject.Find("PersistentController").GetComponent<Hibernation>();
        //_playPause = PlayBtn.GetComponent<UISprite>();
    }

    public void ActivateRecordingToolbar()
    {
        _hibernation.CanHibernate = false;
        NGUITools.SetActive(RecordingToolbar, true);
    }

    private void OnDisable()
    {
        try
        {
            BFSRSwitchController.Instance.StartEvent -= Instance_Start;
            BFSRSwitchController.Instance.Stop -= Instance_Stop;
        }
        catch
        {
            // ignored
        }
    }

    public void DeactivateRecord()
    {
        _hibernation.CanHibernate = true;
        NGUITools.SetActive(RecordingToolbar, false);
        DeactivateSaveRecordedVideoPopUp();
    }

    private void DeactivateSaveRecordedVideoPopUp()
    {
        NGUITools.SetActive(SaveRecordedVideoPopUp, false);
    }

    private void Instance_Stop(BFSREventArgs e)
    {
        if (e.Mode == BFSRMode.Play && SaveRecording)
        {
            _recordData = e.Recording;
            var info = new DirectoryInfo(_videoPath);
            BFSRSwitchController.Instance.SaveToFile(_recordData, info.Name);
            SaveRecording = false;
        }
        // Temporary
        else if (e.Mode == BFSRMode.Play)
        {
            //var temp = Camera.main.GetComponent<BFSERecordAudio>();
            //temp.StopRecording();
            //temp.StopPlayingAudio();
            //temp.Label1.text = "00:00:000";
            //_tvGuiCon.MainPlayBtnClick();
        }
        if (e.Mode == BFSRMode.Record)
        {
            _recordData = e.Recording;
            _thumnailImage = e.Image;
        }
    }

    private void Instance_Start(BFSREventArgs e)
    {
        if (e.Mode == BFSRMode.Play)
        {
            if (!string.IsNullOrEmpty(_videoPath))
            {
                _recordData = BFSRSwitchController.Instance.ReadFromFile(_videoPath);
            }

            BFSRSwitchController.Instance.PlayData(new List<BFSRData>(_recordData));

            BFSRSwitchController.Instance.AddTimerLabelToReplay(VideoTimeLabel.GetComponent<UILabel>());
        }
    }

    public void Record()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Record;
        _recordSprite.spriteName = "record_on";
        _pauseSprite.spriteName = "pause 1";
       // StartRecButtonAnim();
    }

    public void RecordPause()
    {
        if (BFSRSwitchController.Instance.Mode == BFSRMode.Record)
        {
            BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
            _pauseSprite.spriteName = "pause_on";
            _recordSprite.spriteName = "record";
        }
        else
        {
            _pauseSprite.spriteName = "pause 1";
        }
       
    }

   // private void StartRecButtonAnim()
   // {
      //  var temp = RecordBtn.GetComponent<TweenAlpha>();
      //  temp.enabled = true;
     //   temp.PlayForward();
   // }

    public void StopRecord()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        _recordSprite.spriteName = "record";
        //StopRecButtonAnim();
    }

    //private void StopRecButtonAnim()
   // {
       // var temp = RecordBtn.GetComponent<TweenAlpha>();
       // temp.value = 1.0f;
      //  temp.enabled = false;
    //}

    public void PlayLoadedVideo()
    {
        _playPause.spriteName = MagicStrings.Pause;
        BFSRSwitchController.Instance.Mode = BFSRMode.Play;
    }

    public void PauseLoadedVideo()
    {
        _playPause.spriteName = MagicStrings.Play;
        BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
    }

    public void OpenSaveRecordedVideoPopUp()
    {
        gameObject.GetComponent<BtnController>().DisableDrawing();
        BFSRSwitchController.Instance.Mode = BFSRMode.Pause;
        NGUITools.SetActive(SaveRecordedVideoPopUp, true);
        _saveSprite.spriteName = "save_on";
        _recordSprite.spriteName = "record";
        _pauseSprite.spriteName = "pause 1";
        //StopRecButtonAnim();
    }

    public void CloseSaveRecordedVideoPopUp()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;
        NGUITools.SetActive(SaveRecordedVideoPopUp, false);
        _saveSprite.spriteName = "save";
        gameObject.GetComponent<BtnController>().ResetButtonClick();
    }

    public void SaveRecordedVideoOkay()
    {
        BFSRSwitchController.Instance.Mode = BFSRMode.Stop;

        _name = Name.GetComponent<UIInput>().value;
        _description = Description.GetComponent<UIInput>().value ?? "";

        _name = _name.Trim();
        _description = _description.Trim();

        if (_name == null || _description == null)
        {
            gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", "Either name or description is empty, please try again.", "Ok");
        }
        else
        {
            if (_recordData != null)
            {
                if (_recordData.Count > 0)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(_category))
                            BFSRSwitchController.Instance.SaveToFile(_recordData, _name, _category, _description);
                        else
                            BFSRSwitchController.Instance.SaveToFile(_recordData, _name);
                    }
                    catch (Exception e)
                    {
                        gameObject.GetComponent<CreateNotification>().CreateOkayButton("Error", e.Message, "Ok");
                        Debug.Log("error: " + e);
                    }
                }
            }
            CloseSaveRecordedVideoPopUp();
        }

    }

    public void LoadVideoFiles(string filename)
    {
        _videoPath = filename;
        _imagelocation = filename;

        BFSRSwitchController.Instance.Mode = BFSRMode.Play;

        //Camera.main.GetComponent<BFSERecordAudio>().LoadAudio(filename);
        //Camera.main.GetComponent<BFSERecordAudio>().PlayBackClick();

        GameObject.Find("Subtitle").GetComponent<SubtitleCreator>().LoadSubFile(filename);
    }

    public void Thumbnail()
    {
        var info = new DirectoryInfo(_imagelocation);

        BFSRSwitchController.Instance.CaptureImage(Path.Combine(_imagelocation, info.Name), Camera.main, Screen.width,
            Screen.height,null);
    }

    public string GetFileName()
    {
        if (_videoPath == null) return "";

        var info = new DirectoryInfo(_videoPath);
        var path = Path.Combine(_videoPath, info.Name);
        return path;
    }

    public void RecordVoiceOver()
    {
        _recordData = BFSRSwitchController.Instance.ReadFromFile(_videoPath);
        //Camera.main.GetComponent<BFSERecordAudio>().RecordClick(BFSRSwitchController.Instance.VideoLength());
    }

    public void StopRecordVoiceoOver()
    {
        _editSlider1.value = 0;
        Camera.main.GetComponent<BFSERecordAudio>().StopRecording();
    }

    public void CutFrames(float one, float two)
    {
        //var frames = BFSRSwitchController.NumberOfFrames2Play();
        //BFSRSwitchController.Instance.RemoveFrame((int) Math.Round(one*frames), (int) Math.Round(two*frames));
    }

    public void ChangeActiveSlider(GameObject obj)
    {
        BFSRSwitchController.Instance.AddSliderToReplay(obj.GetComponent<UISlider>());
    }

    public void UpdateLabel(UILabel label, UISlider slider)
    {
        label.text =
            BFSRSwitchController.Instance.TimerValueOnFrame(
                (int) Mathf.Round(BFSRSwitchController.NumberOfFrames2Play()*slider.value));
    }
}