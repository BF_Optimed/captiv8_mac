﻿using UnityEngine;

public class UIGrowthScript : MonoBehaviour
{
    private GameObject _guiBox;
    private Vector2 _newTo;
    public bool _showing;
    public GameObject GrowthObject;
    public string Type;

    public void InputRecieved()
    {
        Debug.Log("WE JUST GOT INPUT RECIEVED.");
        if (_showing)
        {
            Close();
        }
        else
        {
            Open();
        }
        _showing = !_showing;
    }

    public void Close()
    {
        if (_guiBox != null)
        {
            _guiBox.GetComponent<TweenPosition>().PlayReverse();
            _guiBox.GetComponent<TweenAlpha>().PlayReverse();
            _guiBox.GetComponent<TweenScale>().PlayReverse();
            _guiBox.GetComponent<TweenScale>().AddOnFinished(DestroyThisGui);
        }
    }

    private void Open()
    {
        _guiBox = NGUITools.AddChild(gameObject, GrowthObject);
        _guiBox.transform.position = gameObject.transform.position;


        if (Type == "BottomLeft")
        {
            BottomLeft();
        }

        if (Type == "Bottom")
        {
            Bottom();
        }

        if (Type == "BottomRight")
        {
            BottonRight();
        }


        _guiBox.GetComponent<TweenPosition>().to = _newTo*-1.0f;
    }

    private void BottonRight()
    {
        _newTo = (gameObject.GetComponent<UISprite>().localSize*0.5f) +
                 (_guiBox.GetComponent<UIPanel>().GetViewSize()*0.5f);
        _newTo.x = _newTo.x - gameObject.GetComponent<UISprite>().localSize.x*1.5f;
        _newTo.y = 86;
    }

    private void Bottom()
    {
        _newTo.Set(0,
            gameObject.GetComponent<UISprite>().localSize.y*0.5f + _guiBox.GetComponent<UIPanel>().GetViewSize().y*0.5f);
        _newTo.y += 5;
    }

    private void BottomLeft()
    {
        _newTo = (gameObject.GetComponent<UISprite>().localSize*0.5f) +
                 (_guiBox.GetComponent<UIPanel>().GetViewSize()*0.5f);
        _newTo.x = _newTo.x - gameObject.GetComponent<UISprite>().localSize.x;
    }

    private void DestroyThisGui()
    {
        Destroy(_guiBox);
    }
}