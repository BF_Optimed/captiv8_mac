﻿using UnityEngine;

public class BFSMPan : MonoBehaviour
{
    private float _currDis;
    private Vector3 _lastPosition;
    private Vector3 _newPos;
    private Quaternion _rotation;
    private BFSMRotation _rotationScript;
    public float MouseSensitivity = 0.01f;
    public GameObject Blackout;
    private BFSUTransition _transition;

    public BFSMPan()
    {
        MouseSelection = 2;
    }

    public int MouseSelection { get; set; }
    public Vector3 DesiredOffset { get; set; }

    private void Start()
    {
        _transition = Blackout.GetComponent<BFSUTransition>();
        _rotationScript = Camera.main.GetComponent<BFSMRotation>();
    }

    private void Update()
    {
        if (_transition.CurrentObj.name == "Vision Simulator") return;

            MouseLastPosition();

            DesiredOffsetChangeAmount();

            ChangeRotationSpeedVsFov();

            SetNewRotationPosition();

            SetNewRotationScriptRotation();
    }

    private void SetNewRotationScriptRotation()
    {
        _rotation = _rotationScript.Rotation;
        _rotationScript.Position = _rotation*_rotationScript.PositionNew + _rotationScript.Target.position;
        _rotationScript.transform.position = _rotationScript.Position;
    }

    private void SetNewRotationPosition()
    {
        _currDis = _rotationScript.CurrentDistance;
        _newPos = new Vector3(0.0f, 0.0f, -_currDis) + _rotationScript.TargetOffset;
        _rotationScript.PositionNew = _newPos;
    }

    private void ChangeRotationSpeedVsFov()
    {
        var factor = GetFactor();
        RotationLerpSpeedVsFov(factor);
    }

    public float GetFactor()
    {
        if (Camera.main.fieldOfView > 10.0f)
        {
            return 2.5f;
        }
        if (Camera.main.fieldOfView < 10.0f && Camera.main.fieldOfView > 5.0f)
        {
            return 3.5f;
        }
        if (Camera.main.fieldOfView < 5.0f && Camera.main.fieldOfView > 3.0f)
        {
            return 5.5f;
        }

        return 6.5f;
    }

    private void RotationLerpSpeedVsFov(float factor)
    {
        _rotationScript.TargetOffset = Vector3.Lerp(_rotationScript.TargetOffset, DesiredOffset, Time.deltaTime*factor);
    }

    private void MouseLastPosition()
    {
        if (Input.GetMouseButtonDown(MouseSelection))
        {
            _lastPosition = Input.mousePosition;
        }
    }

    private void DesiredOffsetChangeAmount()
    {
        if (!Input.GetMouseButton(MouseSelection)) return;

        var delta = Input.mousePosition - _lastPosition;
        DesiredOffset -= delta/65;

        _lastPosition = Input.mousePosition;
    }
}