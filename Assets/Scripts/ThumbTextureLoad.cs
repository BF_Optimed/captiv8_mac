﻿using Assets.DBHelper;
using Captiv8OnlineClient.Contracts;
using UnityEngine;
using File = System.IO.File;

public class ThumbTextureLoad : MonoBehaviour
{

    private Video _video;

	// Use this for initialization
    void OnEnable()
    {
        _video = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo;

        LoadNewTex();
    }

    public void LoadNewTex()
    {
        if (File.Exists(_video.PngPath))
            PopulateTexture(_video.PngPath);
    }

    private void PopulateTexture(string imagepath)
    {
        if (string.IsNullOrEmpty(imagepath)) return;
        var url = "file:///" + imagepath;
        var www = new WWW(url);
        while (!www.isDone)
        {
        }
        gameObject.GetComponent<UITexture>().mainTexture = www.texture;
    }
}
