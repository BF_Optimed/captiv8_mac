﻿using UnityEngine;

public class greyOutScript : MonoBehaviour
{
    private BtnController _btnController;
    // Use this for initialization
    private void Start()
    {
        if (GameObject.Find("Controller"))
            _btnController = GameObject.Find("Controller").GetComponent<BtnController>();
    }

    private void OnClick()
    {
        if(_btnController != null)
        {
            _btnController.Reset();
            _btnController.RotationScript.MovementAllowed = true;
        }
        
        transform.parent.transform.parent.GetComponent<UIGrowthScript>().InputRecieved();
    }
}