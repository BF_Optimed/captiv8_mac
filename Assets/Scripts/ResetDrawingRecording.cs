﻿using UnityEngine;

public class ResetDrawingRecording : MonoBehaviour
{

    public GameObject controller;


    private void OnEnable()
    {
        controller.GetComponent<BFSDDrawingGUI>().OnEnable();
    }

    private void OnDisable()
    {
        if (controller != null)
            controller.GetComponent<BFSDDrawingGUI>().DefaultSprites();
        BFSRSwitchController.Instance.Component = BFSRComponent.Transformation;
    }
}
