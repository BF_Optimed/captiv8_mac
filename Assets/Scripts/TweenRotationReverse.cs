﻿using UnityEngine;
using System.Collections;

public class TweenRotationReverse : MonoBehaviour {
    public GameObject Buttonicon;
    public TweenRotation Reverse;
	// Use this for initialization
	void Start () {
        Buttonicon = GameObject.Find("Button Icon");
        if (Buttonicon != null)
            Reverse = Buttonicon.GetComponent<TweenRotation>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnClick() 
    {
        if(Reverse != null)
            Reverse.PlayReverse();
    }
}
