﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class PinAutopilot : MonoBehaviour
{

    private BFSUAutopilot autopilot;
    private BtnController _btnController;
    public GameObject Controller;

    // Use this for initialization
    void Start()
    {
        autopilot = gameObject.GetComponent<BFSUAutopilot>();
        _btnController = Controller.GetComponent<BtnController>();
    }

    // Update is called once per frame
    void Update()
    {
            if (Camera.main.fieldOfView < 15.0f && !_btnController.Autopiloting)
            {
                if (PlayerPrefs.GetInt(MagicStrings.PinOptionKey) == 1)
                {
                    gameObject.GetComponent<MeshRenderer>().enabled = true;
                    gameObject.GetComponent<SphereCollider>().enabled = true;
                }
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
                gameObject.GetComponent<SphereCollider>().enabled = false;
            }
    }

    void OnMouseDown()
    {
        Debug.Log("YOU CLICKED ON : " + gameObject.name);
        autopilot.MoveTo();
    }
}