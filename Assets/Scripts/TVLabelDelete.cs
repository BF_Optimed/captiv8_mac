﻿using UnityEngine;
using System;
public class TVLabelDelete : MonoBehaviour
{
    public static BFSGTVVideoList List;
    public static string videoId;

    private void Start()
    {
        List = GameObject.Find("TVGrid") != null
            ? GameObject.Find("TVGrid").GetComponent<BFSGTVVideoList>()
            : GameObject.Find("RelatedGrid").GetComponent<BFSGTVVideoList>();
    }

    public void OnClick()
    {
        GameObject.Find("Controller")
            .GetComponent<CreateNotification>()
            .CreateOneButton("Delete Video?", "Are you sure you wish to delete the video?", "Delete", "Dont Delete",
                new EventDelegate(Delete));
        videoId = gameObject.transform.parent.transform.FindChild("FileName").GetComponent<UILabel>().text;
    }

    public void Delete()
    {
        BFSRSwitchController.Instance.DeleteRecording(videoId);
        GameObject.Find("Controller").GetComponent<TvLibraryGuiController>().UpdateList();
    }
}