﻿using System.Linq.Expressions;

namespace Assets.Scripts
{
    public static class MagicStrings
    {

        public const string Overview = "Overview";
        public const string Adnexa = "Adnexa";
        public const string CiliaryBody = "Ciliary Body";
        public const string OpticNerve = "Optic Nerve";
        public const string AnteriorSegment = "Anterior Segment";
        public const string Macula = "Macula";
        public const string PenBtnSprite = "pen1024_black";
		public const string PenBtnSpriteHit = "pen_hit1024_black";
        public const string PanBtnSprite = "pan1024_black";
        public const string PanBtnSpriteHit = "pan_hit1024_black";
		public const string TvBtnSprite = "library";
		public const string TvBtnSpriteHit = "library_active";
        public const string RecordBtnSprite = "record1024_black";
        public const string RecordBtnSpriteHit = "record_hit1024_black";
		public const string HomeBtnSprite = "home1024";
		public const string HomeBtnSpriteHit = "home_hit1024 (1)";
		public const string ResetBtnSprite = "reset1024_black";
		public const string ResetBtnSpriteHit = "reset_hit1024_black";
        public const string SettingsBtnSprite = "settings1024_black";
        public const string SettingsBtnSpriteHit = "settings_hover1024_black";
		public const string SyncBtnSprite = "cloud";
		public const string Pen = "Pen";
		public const string Pan = "Pan";
		public const string TV = "TV";
		public const string Action = "Action";
		public const string Menu = "Menu";
		public const string AudioBtnSprite = "voice";
		public const string AudioBtnSpriteHit = "voice_active";
		public const string SubtitleBtnSprite = "subtitles";
		public const string SubtitleBtnSpriteHit = "subtitles_active";
		public const string ThumbnailBtnSprite = "thumbnail";
		public const string ThumbnailBtnSpriteHit = "thumbnail_active";
		public const string CutBtnSprite = "cut";
		public const string CutBtnSpriteHit = "cut_active";
        public const string EditDetailsBtnSprite = "edit_details";
        public const string EditDetailsBtnSpriteHit = "edit_details_hover";
		public const string SmallWidthSprite = "off_one";
		public const string SmallWidthSpriteHit = "one";
        public const string SmallWidthSpriteBox = "off_one_box";
        public const string SmallWidthSpriteHitBox = "one_box";
        public const string MediumWidthSprite = "off_two";
		public const string MediumWidthSpriteHit = "two";
        public const string LargeWidthSprite = "off_three";
		public const string LargeWidthSpriteHit = "three";
		public const string CircleSprite = "off_circle";
		public const string CircleSpriteHit = "circle 1";
		public const string SquareSprite = "off_square";
		public const string SquareSpriteHit = "square 1";
        public const string ColorpickerGreen = "off_colour_green";
        public const string ColorpickerGreenHit = "colour_green";
        public const string ColorpickerRed = "off_colour";
        public const string ColorpickerRedHit = "colour";
        public const string ColorpickerBlack = "off_colour_black";
        public const string ColorpickerBlackHit = "colours_black";
        public const string ColorpickerYellow = "off_colour_yellow";
        public const string ColorpickerYellowHit = "colour_yellow";
        public const string ColorpickerBlue = "off_colour_blue";
        public const string ColorpickerBlueHit = "colour_blue";
        public const string ColorhighlightGreen = "colour_green_box";
        public const string ColorhighlightRed = "colour_red_box";
        public const string ColorhighlightBlue = "colour_blue_box";
        public const string ColorhighlightYellow = "colour_yellow_box";
        public const string ColorhighlightBlack = "colours_black_box";
        public const string ColorunhighlightGreen = "off_colour_green_box";
        public const string ColorunhighlightRed = "off_colour_red_box";
        public const string ColorunhighlightBlue = "off_colour_blue_box";
        public const string ColorunhighlightYellow = "off_colour_yellow_box";
        public const string ColorunhighlightBlack = "off_colour_black_box";
        public const string DefaultColourKey = "green";
        public const string DefaultWidthKey = "2";
        public const float AnimSliderLength = 100.0f;
        public const int OffsetDampening = 65;
        public const string Camera = "Camera";
        public const string Pin = "Pin";
        public const string Object = "Object";
		public const string Play = "Play";
		public const string Pause = "Pause";
        public const string SettingsDeactive = "IconBackground";
        public const string SettingsActive = "blue";
        public const string ArrowActive = "search_triangle";
        public const string ArrowDeactive = "search_triangle_blue";
        public static readonly int[] AdnexaLimits = { -20, -225, 20, -180 };
        public static readonly int[] AnteriorLimits = { 40, -30, 20, 180};
        public static readonly int[] StandardLimits = { -75, -360, 75, 360 };
        public static readonly string[] BookmarkList =
        {
            "Overview", "Adnexa", "Anterior Segment", "Ciliary Body", "Macula", "Optic Nerve"
        };

        public const string ModelsAndInteractionsKey = "ModelsAndInteractionsKey";
        public const string ModelsAndInteractionsTitle = "Models & Interactions";
        public const string ModelsAndInteractionsIcon = "animation";
        public static readonly string[] ModelsAndInteractionsContent = {"Press and hold the left mouse and move the mouse up and down.  If you have a slider, then move the slider up and down." };

        public const string RecordKey = "RecordKey";
        public const string RecordTitle = "Record";
        public const string RecordIcon = "record";
        public static readonly string[] RecordContent = { "Click the red record button to start recording. "};

        public const string CutKey = "CutKey";
        public const string CutTitle = "Cut";
        public const string CutIcon = "scissors";
        public static readonly string[] CutContent = { "To cut frames, position the sliders over the area you wish to remove.", "Click the 'Delete Frames' button to remove the selected frames." };

        public const string ThumbnailKey = "ThumbnailKey";
        public const string ThumbnailTitle = "Thumbnail";
        public const string ThumbnailIcon = "camera";
        public static readonly string[] ThumbnailContent = { "To add a thumbnail, position the slider on the frame you wish to capture.", "Click the 'Add Current Frame As Thumb' to capture a thumbnail." };

        public const string SubtitlesKey = "SubtitlesKey";
        public const string SubtitlesTitle = "Subtitles";
        public const string SubtitleIcon = "subtitles";
        public static readonly string[] SubtitlesContent = { "To add subtitles, position the sliders over the area you wish to subtitle.", "Type in the text you wish to display during the selected frames, and then press 'Add Subtitle'", "Repeat this process to add multiple subtitles to multiple sections." };

        public const string VoiceoverKey = "VoiceoverKey";
        public const string VoiceoverTitle = "Voiceover";
        public const string VoiceoverIcon = "voiceover";
        public static readonly string[] VoiceoverContent = { "Click the ‘Start Recording Audio’ button.  Ensure you use a good quality microphone." };

        public const string DrawKey = "DrawKey";
        public const string DrawTitle = "Draw";
        public const string DrawIcon = "draw";
        public static readonly string[] DrawContent = { "Select the Pen icon in the bottom toolbar, press the left mouse button and draw over the model to highlight important points.  Press [ESCAPE] to move the model." };

        public const string ControllingTheModelKey = "ControllingTheModelKey";
        public const string ControllingTheModelTitle = "Controlling The Model";
        public const string ControllingTheModelIcon = "interaction";
        public static readonly string[] ControllingTheModelContent = { "Press and hold the left mouse button and move the mouse to rotate the model. ", "Press and hold the right mouse button and move the mouse up to zoom in and down to zoom out.", "Click the Pan button in the bottom menu and move the mouse.  Deactivate Pan by deselecting the Pan button or by pressing the [ESCAPE] key.", "Hold the left [SHIFT] key and move your mouse left and right to spin the model." };

        public const string ShareKey = "ShareKey";
        public const string ShareTitle = "Share";
        public const string ShareIcon = "share";
        public static readonly string[] ShareContent = {"Share animations with patients via email by clicking the share icon.", "Public videos are available for download on any devices within your account.  Public videos can also be shared with patients, embedded on your website and played in your waiting area.","Private videos will only be shown on this device.  You can share these videos with patients via email." };

        public const string PinOptionKey = "PinOptionKey";
        public const string SliderOptionKey = "SliderOptionKey";
        public const string FullscreenOptionKey = "FullscreenOptionKey";

        public const string HibernationTimeKey = "HibernationTimeKey";

        public const string ConTitle = "Title";
        public const string ConName = "Name";
        public const string ConEmail = "Email";
        public const string ConLastName = "Last Name";

        public const string QualitySettingsKey = "QualitySettingsKey";

        public const string ResolutionXKey = "ResolutionXKey";
        public const string ResolutionYKey = "ResolutionYKey";
    }
}
