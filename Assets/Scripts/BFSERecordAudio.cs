﻿using System.Collections;
using System.IO;
using AudioRecorder;
using UnityEngine;
using Assets.DBHelper;
using System;

public class BFSERecordAudio : MonoBehaviour
{
    public AudioSource _audioSource;
    public bool AutoPlay;
    private Recorder _recorder;
    public GameObject GOlabel1;
    public UILabel Label1;
    private ContinuousVars _continuousVars;

    private void OnEnable()
    {

        _recorder = new Recorder();
        _recorder.onInit += OnInit;
        _recorder.onFinish += OnRecordingFinish;

        SavWav.onSaved += OnRecordingSaved;
    }

    private void OnDisable()
    {
        _recorder.onInit -= OnInit;
        _recorder.onFinish -= OnRecordingFinish;

        SavWav.onSaved -= OnRecordingSaved;
    }

    //Use this for initialization  
    private void Start()
    {
        _audioSource = FindObjectOfType<AudioSource>();
        _recorder.Init();
        _continuousVars = GameObject.Find("PersistentController").GetComponent<ContinuousVars>();
        Label1 = GOlabel1.GetComponent<UILabel>();
    }

    public void StartRecording(int length)
    {
        if (_recorder.IsReady)
        {
            if (!_recorder.IsRecording)
            {
                _recorder.StartRecording(false, length);
                Debug.Log("Started recording audio.");
            }
            else
            {
                Debug.Log("Recorder is already recording");
            }
        }
        else
        {
            Debug.Log("Recorder Isnt ready!");
        }

    }

    public void StopRecording()
    {
        if (_recorder.IsReady)
        {
            Debug.Log("Stop recording audio.");
            _recorder.StopRecording();
        }
        else
        {
            Debug.Log("Recorder Isnt ready!");
        }
    }

    public void PlayAudio()
    {
        //Debug.Log(_audioSource.timeSamples);
        //Debug.Log(_audioSource.clip.length);
        //recorder.PlayRecorded(audioSource);
        if (_audioSource != null)
        {
            if (_audioSource.isPlaying)
            {
                Debug.Log("Stopping Audio via play.");
                _audioSource.Stop();
            }
            else
            {
                if (!_recorder.IsRecording)
                {
                    Debug.Log("Playing Audio.");
                    _audioSource.Play();
                    _audioSource.time = float.Parse(ConvertLabelToAudioStartTime());
                }   
            }
        }
        else
        {
            Debug.Log("Audio source is null. Cant play audio.");
        }
            
    }

    private string ConvertLabelToAudioStartTime()
    {
        var temp = Label1.text;
        temp = temp.Substring(3, 6);
        temp = temp.Replace(':', '.');
        return temp;
    }

    public void StopAudio()
    {
        if (_audioSource != null)
        {
            Debug.Log("Stopping audio. FFF");
            _audioSource.Stop();
        }
        else
        {
            Debug.Log("Cant stop audio due to audio source being null.");
        }
        
    }

    void SaveAudio()
    {
        if (_recorder.hasRecorded)
        {
            Debug.Log("Saving Audio.");
            var savePath =
            Path.ChangeExtension(_continuousVars.LoadedVideo.XmlPath, "wav");
            SavWav.Save(savePath, _recorder.Clip);
            _continuousVars.LoadedVideo.UpdateVideo(AnimationModel.AnimationSound, savePath);
            _continuousVars.LoadedVideo.AnimationId = "";
            _continuousVars.LoadedVideo.Public = false;
            _continuousVars.LoadedVideo.Private = false;
            _continuousVars.LoadedVideo.UpdateVideo();
            
        }
        else
        {
            Debug.Log("Nothing was recorded!");
        }

    }

    private void OnInit(bool success, string error)
    {
        Debug.Log("Success : " + success);
        Debug.Log("Error : " + error);
    }

    private void OnRecordingFinish(AudioClip clip)
    {
        if (AutoPlay)
        {
            _audioSource.clip = clip;
            _audioSource.Play();

            // or you can use
            //recorder.PlayRecorded(audioSource);
        }
        Debug.Log("Try to save: ");
        SaveAudio();
    }

    private void OnRecordingSaved(bool succ, string path)
    {
        if (succ)
        {
            Debug.Log("File Saved at : " + path);
        }
    }

    public void LoadAudio(string filepath)
    {
        Debug.Log("filepath for audio play"+ filepath);
        StartCoroutine(AudioCo(filepath));
    }

    private IEnumerator AudioCo(string videoId)
    {
        var video = GameObject.Find("PersistentController").GetComponent<ContinuousVars>().LoadedVideo;
        if (video.WavPath == null) yield break;

        var www = new WWW("file:///" + video.WavPath);
        yield return www;
        _audioSource.clip = www.audioClip;
    }
}