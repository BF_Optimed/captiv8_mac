﻿using UnityEngine;
using System.Collections;
using Assets.DBHelper;
using System.Linq;

public class TemplateClick : MonoBehaviour {

    public GameObject Title;
    public GameObject Name;
    public GameObject Decription;
    public GameObject DropDown;
    public UILabel _title;
    public UILabel _name;
    public UIInput _description;
    private string s;

    public Templates Templates;

	// Use this for initialization
	void Start ()
    {
        Title = GameObject.FindWithTag("Title");
        Name = GameObject.FindWithTag("Name");
        Decription = GameObject.FindWithTag("Description");
        DropDown = GameObject.FindWithTag("DropDown");
        _title = Title.GetComponentInChildren<UILabel>();
        _name = Name.GetComponentInChildren<UILabel>();
        _description = Decription.GetComponentInChildren<UIInput>();
        s = gameObject.GetComponentInChildren<UILabel>().text;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnClick() 
    {

        Templates = new Templates().GetTemplates("Id = "+gameObject.name).FirstOrDefault();

        _description.value = Templates.Content;
        Destroy(DropDown);

    }
}
