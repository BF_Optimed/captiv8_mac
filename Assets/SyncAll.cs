﻿using UnityEngine;
using Assets.DBHelper;
using System.Linq;
using System.Collections;
using System;
using System.Collections.Generic;
using Assets.SynchroniseScript;

public class SyncAll : MonoBehaviour {

    public GameObject Parent;
    private bool _isChecked;


    void Start() 
    {
    }

	void OnClick()
    {
       if(!_isChecked)
       {
            foreach (var t in Parent.GetComponentsInChildren<UIToggle>())
            {
               t.value = true;
               gameObject.GetComponentInChildren<UILabel>().text = "Deselect All";
            }
            _isChecked = true;
       }
       else
       {
        foreach (var t in Parent.GetComponentsInChildren<UIToggle>())
            {
               t.value = false;
               gameObject.GetComponentInChildren<UILabel>().text = "Select All";
            }
        _isChecked = false;
       }
    }

    void OnEnable() 
    {
        gameObject.GetComponentInChildren<UILabel>().text = "Select All";
    }
}
