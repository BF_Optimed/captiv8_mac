﻿using Assets.DBHelper;
using Assets.OptimedClient;
using Ionic.Zip;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Share
{
    public static class ShareHelper
    {
        public static ShareService.Contracts.Animation[] CreateAnimation(Video video)
        {
            var animations = new List<ShareService.Contracts.Animation>();
            animations.Add(new ShareService.Contracts.Animation(InstallationId.GetInstallationId, video.Name)
            {
                AnimationId = video.AnimationId,
                Category = video.Category,
                duration = video.Duration,
                LocalAnimationId = video.Id.ToString(),
                Description = video.Description,
                Owner=video.Owner.ToString(),
                ImageFrameId=video.ImageFrameId,
                Files = CreateFiles(video)
            });
            return animations.ToArray();
        }
        public static ShareService.Contracts.File[] CreateFiles(Video video)
        { 
            var files= new List<ShareService.Contracts.File>();
            if (!string.IsNullOrEmpty(video.XmlPath)) files.Add(CreateFile(video.XmlPath));
            if (!string.IsNullOrEmpty(video.WavPath)) files.Add(CreateFile(video.WavPath));
            if (!string.IsNullOrEmpty(video.PngPath)) files.Add(CreateFile(video.PngPath));
            if (!string.IsNullOrEmpty(video.VttPath)) files.Add(CreateFile(video.VttPath));
            if (!string.IsNullOrEmpty(video.Mp3Path)) files.Add(CreateFile(video.Mp3Path));
            return files.ToArray();

        }
        private static ShareService.Contracts.File CreateFile(string path)
        {
            return new ShareService.Contracts.File(path);
        }
        public static ShareService.Contracts.Share CreateShare(ShareService.Contracts.ShareType type, ShareService.Contracts.PublicShare publicShare,ShareService.Contracts.PrivateShare privateShare)
        {
            var share = new ShareService.Contracts.Share()
            {
                RegistrationId = InstallationId.GetInstallationId,
                ShareType=type,
            };
            if (type == ShareService.Contracts.ShareType.Public)
            {
                share.PublicShare = publicShare;
            }
            else
            {
                share.Privateshare = privateShare;
            }
            if (string.IsNullOrEmpty(share.GetAnimations().First().AnimationId))
            {
                share.Render = true;
            }
            return share;
        }
        public static byte[] CreateZip(ShareService.Contracts.File[]  files)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var zip = new ZipOutputStream(memoryStream, true))
                {
                    foreach (var file in files)
                    {
                        zip.PutNextEntry(new System.IO.FileInfo(file.Location).Name);
                        var buffer = ReadBytes(file.Location);
                        zip.Write(buffer, 0, buffer.Length);
                    }
                }
                return memoryStream.ToArray();
            }
        }

        private static byte[] ReadBytes(string filePath)
        {
            return System.IO.File.ReadAllBytes(filePath);
        }

        private class Bytes
        {
            public Bytes(byte[] value, string type)
            {
                Value = value;
                Type = type;
            }

            public byte[] Value { get; private set; }
            public string Type { get; private set; }
        }

        private class FactoryWithContent : IHttpFactory
        {
            public Func<Bytes> GetBytes { get; set; }

            public IHttp Create()
            {
                var http = new Http();

                var getBytes = GetBytes;
                if (getBytes != null)
                {
                    var bs = getBytes();
                    http.RequestBodyBytes = bs.Value;
                    http.RequestContentType = bs.Type;
                }

                return http;
            }
        }
    }
}
