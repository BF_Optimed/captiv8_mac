﻿using Assets.DBHelper;
using Captiv8OnlineClient;
using Captiv8OnlineClient.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using UnityEngine;
namespace Assets.Share
{
    public class ShareCaptiv8
    {
        private Video video;
        Client client;
        public ShareCaptiv8(Video video, string cmsUrl)
        {
            this.video = video;
            this.client    = new Client(cmsUrl);
        }
        private Captiv8OnlineClient.Contracts.PrivateShare CreatePrivateShareRequest(string registrationId, string toEmail, string toTitle, string toFirstName, string toLastName,string message)
        {
            var shareRequest = new Installation(registrationId).CreateShare(CreateAnimation());
            shareRequest.FromEmail = BFSSecurePlayerData.GetString(MagicStrings.ConEmail, SystemInfo.deviceUniqueIdentifier);
            shareRequest.FromName = String.Format("{0} {1} {2}", BFSSecurePlayerData.GetString(MagicStrings.ConTitle, SystemInfo.deviceUniqueIdentifier), BFSSecurePlayerData.GetString(MagicStrings.ConName, SystemInfo.deviceUniqueIdentifier), BFSSecurePlayerData.GetString(MagicStrings.ConLastName, SystemInfo.deviceUniqueIdentifier));
            shareRequest.ToEmail = toEmail;
            shareRequest.ToTitle = toTitle;
            shareRequest.ToFirstName = toFirstName;
            shareRequest.ToLastName = toLastName;
            shareRequest.Message = message;
            return shareRequest;
        }
    
        private Captiv8OnlineClient.Contracts.Animation CreateAnimation()
        {
            return new Captiv8OnlineClient.Contracts.Animation(new AnimationId { Value = video.AnimationId})
            {
                Name =video.Name,
                Owner = video.Owner.ToString()
            };
        }
        public void PostPrivateAnimation(string registrationID, string toEmail, string toTitle, string toFirstName, string toLastName, string message, Action<ShareService.Contracts.ShareResponse> callBack)
        {
            var animations = CreateAnimation();
            var privateShareRequest = CreatePrivateShareRequest(registrationID, toEmail, toTitle, toFirstName, toLastName,message);
            var response = client.ShareAnimations(privateShareRequest);
            if (response.Error)
            {
                callBack(JsonConvert.DeserializeObject<ShareService.Contracts.ShareResponse>("{\"Message\" : \"Not able to share private video\"}"));
            }
            callBack(JsonConvert.DeserializeObject<ShareService.Contracts.ShareResponse>("{\"Message\" : \"Done\"}"));
        }
    }
}
