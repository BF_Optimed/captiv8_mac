﻿using Assets.DBHelper;
using Newtonsoft.Json;
using RestSharp;
using ShareService.Contracts;
using System;
using System.Collections;
using System.Linq;
using System.Threading;
using UnityEngine;
using Assets.Scripts;
using System.Net;
using Assets.OptimedClient;
namespace Assets.Share
{
    public class ShareRequestHelper
    {
        private static string uploadURL = Runtime.Default.UploadServiceUrl;
        Video video;
        public ShareRequestHelper(Video video)
        {
            this.video = video;
        }
        
        private RestClient GetClient()
        {
            var client = new RestClient(uploadURL) { Timeout = Timeout.Infinite };
            return client;
        }
        private ShareService.Contracts.PublicShare CreatePublicShare()
        {
            return new ShareService.Contracts.PublicShare()
            {
                Animations=ShareHelper.CreateAnimation(video)
            };
        }
        private ShareService.Contracts.PrivateShare CreatePrivateShare(string toTitle,string toFirstname,string toLastName,string toEmail,string message)
        {
            return new PrivateShare()
            {
                FromEmail = BFSSecurePlayerData.GetString(MagicStrings.ConEmail, SystemInfo.deviceUniqueIdentifier),
                FromName=String.Format("{0} {1} {2}",BFSSecurePlayerData.GetString(MagicStrings.ConTitle, SystemInfo.deviceUniqueIdentifier),BFSSecurePlayerData.GetString(MagicStrings.ConName, SystemInfo.deviceUniqueIdentifier),BFSSecurePlayerData.GetString(MagicStrings.ConLastName, SystemInfo.deviceUniqueIdentifier)),
                ToEmail=toEmail,
                ToFirstName=toFirstname,
                ToLastName=toLastName,
                ToTitle=toTitle,
                Message=message,
                RegistrationId = InstallationId.GetInstallationId,
                Animations = ShareHelper.CreateAnimation(video)
            };
        }
        public void PostShareRequest(RestRequest request, Action<ShareResponse> callBack)
        {
            //yield return null;
            var responseC = GetClient().Execute(request);
           
            var response = responseC.Content;
            Debug.Log( "Response after private Share : " + responseC.StatusCode);
            if (responseC.StatusCode == HttpStatusCode.InternalServerError)
            {
                response = "{\"Message\" : \"Done\"}";
            }
            if(string.IsNullOrEmpty(response) ) response="{\"Message\" : \"Not able to connect to sever\"}";
            callBack(JsonConvert.DeserializeObject<ShareResponse>(response));
            
            
        }
        public RestRequest CreatePublicRequest()
        {
            var share = ShareHelper.CreateShare(ShareType.Public, CreatePublicShare(),null);
            return CreateRequest(share);
        }
        public RestRequest CreatePrivateRequest(string toTitle, string toFirstname, string toLastName, string toEmail, string message)
        {
            if (video.Owner == Owner.captiv8plus)
            { 
                var share = ShareHelper.CreateShare(ShareType.Private, null, CreatePrivateShare(toTitle, toFirstname, toLastName, toEmail, message));
                return CreateRequest(share);
            }
            throw new Exception("Not a valid type of share");
        }
        private RestRequest CreateRequest(ShareService.Contracts.Share share)
        {
            var animation = share.GetAnimations().First();
            RestRequest request;
            if(string.IsNullOrEmpty(animation.AnimationId))
            {
               request =  RequestWithZipFile(share);
            }
            else
            {
                request = RequestWithLocalFile(share);
            }
            return request;
        }

        private RestRequest RequestWithLocalFile(ShareService.Contracts.Share share)
        {
            var request = new RestRequest("Share/WithoutRender", Method.POST)
            {
                //AlwaysMultipartFormData = true
            };
            request.AddParameter("application/json", CreateJson(share), ParameterType.RequestBody);
            return request;
        }

        private RestRequest RequestWithZipFile(ShareService.Contracts.Share share)
        {
            var animation = share.GetAnimations().First();
            var zipBytes = ShareHelper.CreateZip(animation.Files.ToArray());
            var request = new RestRequest("Share", Method.POST)
            {
                AlwaysMultipartFormData = true
            };
            request.AddHeader("content-type", "multipart/form-data");
            request.AddParameter("application/json", CreateJson(share), ParameterType.RequestBody);
            request.AddFile(animation.Name, zipBytes, animation.Name, "application/zip");
            return request;
        }

        private string CreateJson(ShareService.Contracts.Share share)
        {
            return JsonConvert.SerializeObject(share);
        }
    }
}
