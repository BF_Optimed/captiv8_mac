﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.DBHelper
{
   public class Templates:SQLiteDatabase
    {
        public int Id { get; set; }
        public string CmsId { get; set; }
        private string name;
        public string AnimationId { get; set; }
        private string content;
        public Templates()
            :base()
        {
            // = new List<FileDetail>();
        }
        public string Name
        {
            get
            {
                return RemoveSqlString(name);
            }
            set
            {
                name = MakeSqlString(value);
            }
        }
        public string Content
        {
            get
            {
                return RemoveSqlString(content);
            }
            set
            {
                content = MakeSqlString(value);
            }
        }
        private string MakeSqlString(string propertyValue)
        {
            return propertyValue.Replace("'", "''");
        }
        private string RemoveSqlString(string propertyValue)
        {
            return propertyValue.Replace("''", "'");
        }
        public ICollection<Templates> GetTemplates(string where)
        {
            var templates = new List<Templates>();
            var sqlStatement = "SELECT * FROM Templates" + (string.IsNullOrEmpty(where) ? "" : " where " + where);
            var reader = GetDataTable(sqlStatement);
            while (reader.Read())
            {
                var template = new Templates();
                DataRecordHelper.TemplateRecordReader(reader, template);
                templates.Add(template);
            }
            return templates;
        }
        public void Save()
        {
            Insert("Templates", FileDataToSave());
        }
        private Dictionary<string, string> FileDataToSave()
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("CmsId", string.Format("'{0}'", CmsId));
            dataToSave.Add("AnimationId", string.Format("'{0}'", AnimationId));
            dataToSave.Add("Name", string.Format("'{0}'", name));
            dataToSave.Add("Content", string.Format("'{0}'", content));
            return dataToSave;
        }
    }
}
