﻿using Mono.Data.SqliteClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Assets.DBHelper
{
    public enum Owner
    {
        None,
        captiv8plus,
        captiv8
    }
    public enum Status
    {
        None,
        Local,
        Viewed,
        Downloading,
        Completed,
        Display,
        Deleted
    }
    public class Video:SQLiteDatabase
    {
        private class KeyGenerator
        {
            public static string GetUniqueKey(int maxSize)
            {
                char[] chars = new char[62];
                chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[1];
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                
                    crypto.GetNonZeroBytes(data);
                    data = new byte[maxSize];
                    crypto.GetNonZeroBytes(data);
                
                StringBuilder result = new StringBuilder(maxSize);
                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }
                return result.ToString();
            }
        }
        private ICollection<FileDetail> animationFiles;
        internal string Id { get; set; }
        public string AnimationId { get; set; }
        private string _name { get; set; }
        private string _category { get; set; }
        private string _description { get; set; }
        public int Duration { get; set; }
        public bool Private { get; set; }
        public bool Public { get; set; }
        private string _xmlPath;
        public Owner Owner { get; set; }
        private string _wavPath;
        private string _pngPath;
        private string _vttPath;
        public string Mp3Path { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set;}
        public Status Status { get; set; }
        public bool Locked { get; set; }
        public string Location { get; set; }
        public int ImageFrameId { get; set; }
        public Video()
            :base()
        {
            animationFiles = new List<FileDetail>();
        }
        public Video(string filePath)
            : base(filePath)
        {
            animationFiles = new List<FileDetail>();
        }
        public string XmlPath
        {
            get 
            {
                if (string.IsNullOrEmpty(_xmlPath)) return _xmlPath = GetLocalPath(AnimationModel.AnimationCoordinates);
                return _xmlPath;
            }
            set 
            {
                var cordinates = CreateFileInfo(AnimationModel.AnimationCoordinates, value);
                animationFiles.Add(cordinates);
                _xmlPath = cordinates.LocalPath;
            }
        }
        public int FileCount 
        {
            get { return animationFiles.Count; }
        }
        public string PngPath
        {
            get
            {
                if (string.IsNullOrEmpty(_pngPath)) return _pngPath = GetLocalPath(AnimationModel.AnimationImage);
                return _pngPath;
            }
        }
        public string VttPath
        {
            get
            {
                if (string.IsNullOrEmpty(_vttPath)) return _vttPath = GetLocalPath(AnimationModel.AnimationCaptions);
                return _vttPath;
            }

        }
        public string WavPath
        {
            get
            {
                if (string.IsNullOrEmpty(_wavPath)) return _wavPath = GetLocalPath(AnimationModel.AnimationSound);
                return _wavPath;
            }

        }
        private FileDetail CreateFileInfo(AnimationModel model, string path)
        {
            if (!CheckFileExits(path)) throw new System.IO.FileNotFoundException("File not found",path);
            return new FileDetail 
            {
                AnimationId = this.Id,
                LocalPath=path,
                Model=model,
                Size=GetFileSize(path)
            };
        }
        private long GetFileSize(string path)
        {
            return new System.IO.FileInfo(path).Length;
        }
        private bool CheckFileExits(string path)
        {
            return System.IO.File.Exists(path);
        }
        public string GetLocalPath(AnimationModel model)
        {
           return this.animationFiles.Where(x => x.Model == model).Select(x => x.LocalPath).FirstOrDefault();
        }
        public string Name
        {
            get 
            {
                return RemoveSqlString(_name);
            }
            set 
            {
                _name = MakeSqlString(value);
            }
        }
        public string Category
        {
            get
            {
                return RemoveSqlString(_category);
            }
            set
            {
                _category = MakeSqlString(value);
            }
        }
        public string Description
        {
            get
            {
                return RemoveSqlString(_description);
            }
            set
            {
                _description = MakeSqlString(value);
            }
        }
        public void AddAnimationFile(List<FileDetail> files)
        {
            this.animationFiles = files;
        }
        public IEnumerable GetVideos(string category)
        {
            var where = " Category ='" + category.Trim() + "' AND Status IN ('" + Status.Local.ToString() + "','" + Status.Completed.ToString() + "') AND Owner ='"+ Owner.captiv8plus.ToString() +"'";
            return  GetAllVideos(where);
        }
        public IEnumerable GetCaptiv8Video()
        {
            var where = " Status IN ('" + Status.Completed.ToString() + "') AND Owner ='" + Owner.captiv8.ToString() + "'";
            return GetAllVideos(where);
        }
        public Video GetVideo(string id)
        {
            var where = " Id ='" + id + "'";
            return GetAllVideos(where).FirstOrDefault();
        }
        public ICollection<Video> GetAllVideos(string where)
        {
            var videos = new List<Video>();
            var sqlStatement = "SELECT * FROM Animation" + (string.IsNullOrEmpty(where) ? "" : " where " + where);
            var reader = GetDataTable(sqlStatement);
            while (reader.Read())
            {
                Video video = new Video();
                DataRecordHelper.VideoRecordReader(reader, video);
                video.animationFiles = GetFileForVideo(video.Id);
                videos.Add(video);
            }
            return videos;
        }
        private ICollection<FileDetail> GetFileForVideo(string Id)
        {
            var files = new List<FileDetail>();
            var sqlStatement = "SELECT * FROM AnimationFile WHERE AnimationId= '" + Id + "'";
            var reader = GetDataTable(sqlStatement);
            while (reader.Read())
            {
                FileDetail file = new FileDetail();
                DataRecordHelper.FileRecordReader(reader, file);
                files.Add(file);
            }
            return files;
        }
        public void UpdateVideo(AnimationModel model,string value)
        {
            var file = animationFiles.Where(x => x.Model == model).FirstOrDefault();
            if (!ReferenceEquals(file, null))
            {
                file.LocalPath = value;
                Update("AnimationFile", FileDataToSave(this.Id, file), " Id = '" + file.Id + "'");
            }
            else
            {
                file = CreateFileInfo(model, value);
                Insert("AnimationFile", FileDataToSave(this.Id, file));
                animationFiles.Add(file);
            }
        }
        public FileDetail GetFile(AnimationModel model)
        {
            return animationFiles.Where(x => x.Model == model).FirstOrDefault();
        }
        public void UpdateVideo()
        {
            Update("Animation", VideoDataToSave(), " Id = '" + this.Id + "'");
        }
        private string MakeSqlString(string propertyValue)
        {
            return propertyValue.Replace("'", "''");
        }
        private string RemoveSqlString(string propertyValue)
        {
            return propertyValue.Replace("''", "'");
        }
        public void Delete()
        {
            Delete("AnimationFile", "AnimationId='" + this.Id + "'");
            Delete("Animation", "Id='" + this.Id + "'");
        }
        public void SaveNewVideo()
        {
            this.Id=KeyGenerator.GetUniqueKey(16);
            var animation = VideoDataToSave();
            Insert("Animation", animation);
            foreach(var file in animationFiles)
            {
                Insert("AnimationFile", FileDataToSave(this.Id,file));
            }
        }
        private Dictionary<string, string> VideoDataToSave()
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("Id", string.Format("'{0}'", this.Id));
            dataToSave.Add("Name", string.Format("'{0}'", _name));
            dataToSave.Add("Description", string.Format("'{0}'", _description));
            dataToSave.Add("AnimationId", string.Format("'{0}'", AnimationId));
            dataToSave.Add("Duration", string.Format("{0}", Duration));
            dataToSave.Add("Owner", string.Format("'{0}'", Owner.ToString()));
            dataToSave.Add("Status", string.Format("'{0}'", Status.ToString()));
            dataToSave.Add("Location", string.Format("'{0}'", Location));
            dataToSave.Add("Category", string.Format("'{0}'", Category));
            dataToSave.Add("Public",string.Format("{0}",Public?1:0));
            dataToSave.Add("Private", string.Format("{0}", Private ? 1 : 0));
            dataToSave.Add("Locked", string.Format("{0}", Locked ? 1 : 0));
            dataToSave.Add("DateCreated", string.Format("'{0}'", DateCreated.ToString("dd/MM/yyyy HH:mm:ss")));
            dataToSave.Add("ImageFrameId", string.Format("{0}", ImageFrameId));
            return dataToSave;
        }
        private Dictionary<string, string> FileDataToSave(string id, FileDetail fileinfo)
        {
            Dictionary<string, string> dataToSave = new Dictionary<string, string>();
            dataToSave.Add("AnimationId", string.Format("'{0}'", id));
            dataToSave.Add("Size", string.Format("{0}", fileinfo.Size));
            dataToSave.Add("LocalPath", string.Format("'{0}'", fileinfo.LocalPath));
            dataToSave.Add("CloudPath", string.Format("'{0}'", fileinfo.CloudPath));
            dataToSave.Add("Model", string.Format("'{0}'", fileinfo.Model.ToString()));
            return dataToSave;
        }
    }
}
