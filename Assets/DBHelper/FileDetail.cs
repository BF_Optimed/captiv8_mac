﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.DBHelper
{
    public enum AnimationModel
    {
        None,
        AnimationCoordinates,
        AnimationDocument,
        AnimationImage,
        AnimationCaptions,
        AnimationSound
    }

    public class FileDetail
    {
        public int Id { get; set; }
        public string AnimationId { get; set; }
        public long Size { get; set; }
        public string LocalPath { get; set; }
        public string CloudPath { get; set; }
        public AnimationModel Model { get; set; }
        public string md5 { get; set; }
    }
}
