﻿using Mono.Data.SqliteClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UnityEngine;
//using UnityEngine.UI;

namespace Assets.DBHelper
{
    public class Category
    {
        public string Name { get; set; }

        private static IDataReader GetCategoryReader(string query)
        {
            var dbConnection = (IDbConnection)new SqliteConnection("URI=file:" + SynchroniseScript.SynchroniseSingleton.Instance.ConnectionString);
            dbConnection.Open();
            using (IDbCommand cmd = dbConnection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Connection = dbConnection;
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }
        public string[] GetCategories()
        {
            IDataReader rdr = GetCategoryReader("SELECT * FROM Category order by Sort");
            List<Category> lstCategories= new List<Category>();
            while (rdr.Read())
            {
                Category category = new Category();
                DataRecordHelper.CategotyRecordReader(rdr, category);
                lstCategories.Add(category);
            }
            return lstCategories.Select(x => x.Name).ToArray();
        }
    }
}
