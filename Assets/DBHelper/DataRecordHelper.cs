﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Assets.DBHelper
{
    public static class DataRecordHelper
    {
        public static void VideoRecordReader(IDataRecord record, Video video)
        {
            video.Id = record["Id"] == null ? null : record["Id"].ToString();
            video.Name = record["Name"] == null ? null : record["Name"].ToString();
            video.AnimationId = record["AnimationId"] == null ? null : record["AnimationId"].ToString();
            video.Category = record["Category"] == null ? null : record["Category"].ToString();
            video.Location = record["Location"] == null ? null : record["Location"].ToString();
            video.Public = record["Public"] == null ? false : Convert.ToInt32(string.IsNullOrEmpty(record["Public"].ToString())?"0":record["Public"])==1?true:false;
            video.Description = record["Description"] == null ? null : record["Description"].ToString();
            video.Duration = record["Duration"] == null ? 0 : Convert.ToInt32(record["Duration"]);
            video.Private = record["Private"] == null ? false : Convert.ToInt32(string.IsNullOrEmpty(record["Private"].ToString()) ? "0" : record["Private"]) == 1 ? true : false;
            video.Locked = record["Locked"] == null ? false : Convert.ToInt32(string.IsNullOrEmpty(record["Locked"].ToString()) ? "0" : record["Locked"]) == 1 ? true : false;
            if (record["DateCreated"]!=null)
            video.DateCreated =DateTime.ParseExact(record["DateCreated"].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            if (record["DateModified"] != null)
            video.DateModified = DateTime.ParseExact(record["DateModified"].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            video.Status = record["Status"] == null ? Status.None : (Status)Enum.Parse(typeof(Status), record["Status"].ToString());
            video.Owner = record["Owner"] == null ? Owner.None : (Owner)Enum.Parse(typeof(Owner), record["Owner"].ToString());
            video.ImageFrameId = record["ImageFrameId"] == null ? 0 : Convert.ToInt32(record["ImageFrameId"]);
        }
        public static void CategotyRecordReader(IDataRecord record, Category category)
        {
            category.Name = record["Name"] == null ? null : record["Name"].ToString().Trim();
        }
        public static void TemplateRecordReader(IDataRecord record, Templates template)
        {
            template.Id = Convert.ToInt32(record["Id"]);
            template.CmsId = record["CmsId"] == null ? null : record["CmsId"].ToString().Trim();
            template.AnimationId = record["AnimationId"] == null ? null : record["AnimationId"].ToString().Trim();
            template.Name = record["Name"] == null ? null : record["Name"].ToString().Trim();
            template.Content = record["Content"] == null ? null : record["Content"].ToString();
        }
        public static void FileRecordReader(IDataRecord record, FileDetail fileDetail)
        {
            fileDetail.Id = Convert.ToInt32(record["Id"]);
            fileDetail.AnimationId = record["AnimationId"] == null ? null : record["AnimationId"].ToString();
            fileDetail.Size = record["Size"] == null ? 0 : Convert.ToInt32(record["Size"]);
            fileDetail.LocalPath = record["LocalPath"] == null ? null : record["LocalPath"].ToString();
            fileDetail.CloudPath = record["CloudPath"] == null ? null : record["CloudPath"].ToString();
            fileDetail.md5 = record["md5"] == null ? null : record["md5"].ToString();
            fileDetail.Model = record["Model"] == null ? AnimationModel.None : (AnimationModel)Enum.Parse(typeof(AnimationModel), record["Model"].ToString());
        }
    }
}
