﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Mono.Data.SqliteClient;
using UnityEngine;
using System.IO;
namespace Assets.DBHelper
{
    public interface ISQLiteDatabase
    {
        IDbConnection GetDBConnection();
        void Update(String tableName, Dictionary<String, String> data, String where);
        void Delete(String tableName, String where);
        bool Insert(String tableName, Dictionary<String, String> data);
    }
    public class SQLiteDatabase
    {
       static String dbConnection;

        /// <summary>
        /// Default Constructor for SQLiteDatabase Class.
        /// </summary>
        protected SQLiteDatabase()
        {
            if(dbConnection==null)
            {
                dbConnection = "URI=file:" + SynchroniseScript.SynchroniseSingleton.Instance.ConnectionString;
            }
            
        }

        /// <summary>
        ///  Single Param Constructor for specifying the DB file.
        /// </summary>
        /// <param name="inputFile">The File containing the DB</param>
        protected SQLiteDatabase(String inputFile)
        {
            dbConnection = String.Format("URI=file:{0}", inputFile);
        }
        protected IDbConnection GetDBConnection()
        {
            return(IDbConnection)new SqliteConnection(dbConnection);
        }
        /// <summary>
        ///     Single Param Constructor for specifying advanced connection options.
        /// </summary>
        /// <param name="connectionOpts">A dictionary containing all desired options and their values</param>
        protected SQLiteDatabase(Dictionary<String, String> connectionOpts)
        {
            var builder = new StringBuilder();
            foreach (KeyValuePair<String, String> row in connectionOpts)
            {
                builder.Append(String.Format("{0}={1}; ", row.Key, row.Value));
            }
            builder.Length--;

            dbConnection = builder.ToString();
        }

        /// <summary>
        ///     Allows to run a query against the Database.
        /// </summary>
        /// <param name="sql">The SQL to run</param>
        /// <returns>A DataTable containing the result set.</returns>
        protected IDataReader GetDataTable(string sql)
        {
            var cnn = GetDBConnection();
            cnn.Open();
            var command = cnn.CreateCommand();
            command.CommandText = sql;
            var reader = command.ExecuteReader();
            reader.Close();
            cnn.Close();
            return reader;
        }

        /// <summary>
        ///     Allows to interact with the database for purposes other than a query.
        /// </summary>
        /// <param name="sql">The SQL to be run.</param>
        protected void   ExecuteNonQuery(string sql)
        {
            var cnn = GetDBConnection();
            cnn.Open();
            var command = cnn.CreateCommand();
            command.CommandText = sql;
            try
            {
              command.ExecuteNonQuery();
            }
            catch
            {
                Debug.Log("Sql statement : " + sql + " " );
            }
            cnn.Close();
        }
        /// <summary>
        ///     Allows  to easily update rows in the DB.
        /// </summary>
        /// <param name="tableName">The table to update.</param>
        /// <param name="data">A dictionary containing Column names and their new values.</param>
        /// <param name="where">The where clause for the update statement.</param>
        protected void Update(String tableName, Dictionary<String, String> data, String where)
        {
            var builderValues = new StringBuilder(); 
            if (data.Count >= 1)
            {
                foreach (KeyValuePair<String, String> val in data)
                {
                    builderValues.Append(String.Format(" {0} = {1},", val.Key.ToString(), val.Value.ToString()));
                }
                builderValues.Length--;
            }
            try
            {
                this.ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName, builderValues.ToString(), where));
            }
            catch(Exception ex)
            {
                Debug.Log(ex.Message);
            }

        }

        /// <summary>
        ///     Allows to easily delete rows from the DB.
        /// </summary>
        /// <param name="tableName">The table from which to delete.</param>
        /// <param name="where">The where clause for the delete.</param>
        protected void Delete(String tableName, String where)
        {

            this.ExecuteNonQuery(String.Format("delete from {0} where {1};", tableName, where));
        }

        /// <summary>
        ///     Allows to easily insert into the DB
        /// </summary>
        /// <param name="tableName">The table into which we insert the data.</param>
        /// <param name="data">A dictionary containing the column names and data for the insert.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        protected bool Insert(String tableName, Dictionary<String, String> data)
        {
            var columns = new StringBuilder();
            var values = new StringBuilder();
            Boolean returnCode = true;
            foreach (KeyValuePair<String, String> val in data)
            {
                columns.Append( String.Format(" {0},", val.Key.ToString()));
                values.Append( String.Format(" {0},", val.Value));
            }
            columns.Length--;
            values.Length--;
            try
            {
                this.ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns.ToString(), values.ToString()));
            }
            catch (Exception fail)
            {
                Debug.LogException(fail);
                returnCode = false;
            }
            return returnCode;
        }
    }
}
