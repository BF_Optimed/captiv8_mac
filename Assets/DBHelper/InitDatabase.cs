﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace Assets.DBHelper
{
    public class InitDatabase:SQLiteDatabase
    {

        public InitDatabase()
            :base()
        {
        }
        public InitDatabase(string connectionString)
            : base(connectionString)
        {
        }
        public void CreateTables()
        {
            ExecuteNonQuery(AnimationTableScript());
            ExecuteNonQuery(CatregoryTableScript());
            ExecuteNonQuery(TemplateTableScript());
            ExecuteNonQuery(AnimationFileTableScript());
            ExecuteNonQuery(InsertCategoryDefault());
        }

        private bool CheckIfTableExists(string tableName)
        {
           var sqlStatement = "SELECT count(*) as result FROM sqlite_master WHERE type='table' AND name='"+tableName+"'";
           var reader = GetDataTable(sqlStatement);
           while (reader.Read())
           { 
               if( reader["result"] == null)
               {
                   return false;
               }
           }
           return true;
        }
        private string CatregoryTableScript()
        {
            return @"CREATE TABLE IF NOT EXISTS `Category` (
	                `Id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	                `Name`	TEXT NOT NULL,
	                `Sort`	INTEGER
                     )";
        }
        private string TemplateTableScript()
        {
            return @"CREATE TABLE IF NOT EXISTS `Templates` (
	                `Id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	                `CmsId`	TEXT,
	                `Name`	TEXT,
	                `Content`	TEXT,
	                `AnimationId`	TEXT
                     )";
        }
        private string InsertCategoryDefault()
        {
            return  @"INSERT INTO category('Name','Sort') values ('Cornea',1);
                      INSERT INTO category('Name','Sort') values ('Lens',2);
                      INSERT INTO category('Name','Sort') values ('Optic Nerve',5);
                      INSERT INTO category('Name','Sort') values ('Lids',3);
                      INSERT INTO category('Name','Sort') values ('Macula',4);
                      INSERT INTO category('Name','Sort') values ('Retina',6);
                      INSERT INTO category('Name','Sort') values ('Other',7);";
        }
        private string AnimationFileTableScript()
        {
            return @"CREATE TABLE IF NOT EXISTS `AnimationFile`
                         (
	                        `Id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	                        `AnimationId`	INTEGER NOT NULL,
	                        `Size`	INTEGER,
	                        `LocalPath`	TEXT,
	                        `CloudPath`	TEXT,
	                        `Model`	INTEGER,
	                        `md5`	TEXT,
	                        FOREIGN KEY(AnimationId) REFERENCES Animation(id)
                        )";
        }
        private string AnimationTableScript()
        {
            return @"CREATE TABLE IF NOT EXISTS `Animation` (
	                `Id`	TEXT NOT NULL UNIQUE,
	                `AnimationId`	TEXT,
	                `Category`	TEXT,
	                `Name`	TEXT,
	                `Public`	INTEGER,
	                `Description`	TEXT,
	                `Duration`	INTEGER,
	                `DateCreated`	TEXT,
	                `DateModified`	TEXT,
	                `Status`	TEXT,
	                `Private`	INTEGER,
	                `Owner`	TEXT,
	                `Locked`	INTEGER,
	                `Location`	TEXT,
	                `ImageFrameId`	INTEGER,
	                PRIMARY KEY(Id)
                )";
        }
    }
}
